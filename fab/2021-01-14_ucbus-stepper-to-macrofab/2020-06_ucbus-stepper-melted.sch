<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="yes" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="yes" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="yes" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="yes" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="yes" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="yes" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="yes" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="yes" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="yes" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="yes" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="yes" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="yes" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="yes" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="yes" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="yes" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="yes" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="connector">
<packages>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="PWRPAD_M3_STANDOFF">
<pad name="P$1" x="0" y="0" drill="4.4" diameter="7" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3.6"/>
<vertex x="0.6" y="3.6"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3.6"/>
<vertex x="-0.6" y="-3.6"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.6" y="-0.6"/>
<vertex x="-3.6" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3.6" y="0.6"/>
<vertex x="3.6" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.95269375" y="2.13136875"/>
<vertex x="-2.104165625" y="2.979896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.99705625" y="-2.12131875"/>
<vertex x="2.148528125" y="-2.969846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.104165625" y="-2.969846875"/>
<vertex x="-2.95269375" y="-2.12131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.148528125" y="2.979896875"/>
<vertex x="2.99705625" y="2.13136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3.5" width="0.125" layer="51"/>
</package>
<package name="PWRPAD_3-25MM-SKINNY">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.35" thermals="no"/>
</package>
<package name="PWRPAD_M25_STANDOFF">
<pad name="P$1" x="0" y="0" drill="3.7" diameter="6" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="1.9"/>
<vertex x="-0.4" y="1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-1.9"/>
<vertex x="0.4" y="-1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-1.9" y="0.4"/>
<vertex x="-1.9" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.5"/>
<vertex x="3" y="-0.7"/>
<vertex x="1.9" y="-0.5"/>
<vertex x="1.9" y="0.3"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-0.99203125" y="1.584921875"/>
<vertex x="-1.55771875" y="1.0192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.49705625" y="-1.72131875"/>
<vertex x="1.648528125" y="-2.569846875"/>
<vertex x="1.03639375" y="-1.574871875"/>
<vertex x="1.60208125" y="-1.0091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.669846875"/>
<vertex x="-2.55269375" y="-1.82131875"/>
<vertex x="-1.55771875" y="-1.1091875"/>
<vertex x="-0.99203125" y="-1.674871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.60208125" y="1.0192375"/>
<vertex x="1.03639375" y="1.584921875"/>
</polygon>
</package>
<package name="SOLDER_ON">
<smd name="P$1" x="0" y="0" dx="4" dy="7" layer="1" thermals="no"/>
</package>
<package name="DX4R005HJ5_100">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="DX4R005HJ5">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="21"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="21"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="D-" x="-0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="ID" x="0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="GND" x="1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="25" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="DX4R005HJ5_64">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="USB_MICRO_609-4613-1-ND">
<smd name="HD0" x="-3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD4" x="-3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="HD5" x="3.1" y="2.55" dx="2.1" dy="1.6" layer="1"/>
<smd name="D+" x="0" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="2.675" dx="0.4" dy="1.35" layer="1"/>
<text x="4.9275" y="1.2125" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-4.3925" y="1.13" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<smd name="HD1" x="-1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD2" x="1.05" y="0" dx="1.9" dy="1.8" layer="1"/>
<smd name="HD3" x="3.8" y="0" dx="1.9" dy="1.8" layer="1"/>
<wire x1="-4.7" y1="-1.45" x2="4.7" y2="-1.45" width="0.127" layer="51"/>
<text x="0" y="-1.3" size="0.8128" layer="51" font="vector" align="bottom-center">\\ PCB Edge /</text>
<wire x1="-3.9" y1="3" x2="-3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-3.9" y1="-2.5" x2="3.9" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.9" y1="-2.5" x2="3.9" y2="3" width="0.127" layer="51"/>
<wire x1="3.9" y1="3" x2="-3.9" y2="3" width="0.127" layer="51"/>
<wire x1="-3.9" y1="1.1" x2="-3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.9" y1="1.1" x2="3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.8" y1="3" x2="1.7" y2="3" width="0.127" layer="21"/>
<wire x1="-1.7" y1="3" x2="-1.8" y2="3" width="0.127" layer="21"/>
<wire x1="4.4" y1="3" x2="4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-4.4" y1="3" x2="-4.7" y2="3" width="0.127" layer="21"/>
<wire x1="-3.9" y1="3.6" x2="-3.9" y2="3.8" width="0.127" layer="21"/>
<wire x1="3.9" y1="3.6" x2="3.9" y2="3.8" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="USB-1">
<wire x1="6.35" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-1.27" size="2.54" layer="94">USB</text>
<text x="-4.445" y="-1.905" size="1.27" layer="95" font="vector" rot="R90">&gt;Name</text>
<text x="8.255" y="-1.905" size="1.27" layer="96" font="vector" rot="R90">&gt;Value</text>
<pin name="D+" x="5.08" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D-" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="VBUS" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="-2.54" y="5.08" visible="pad" length="short" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-STANDOFF" package="PWRPAD_M3_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-SKINNY" package="PWRPAD_3-25MM-SKINNY">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5_STANDOFF" package="PWRPAD_M25_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOLDER" package="SOLDER_ON">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB" prefix="X">
<description>SMD micro USB connector as found in the fablab inventory. 
Three footprint variants included: 
&lt;ol&gt;
&lt;li&gt;609-4613-1-ND used by Jake
&lt;li&gt; original, as described by manufacturer's datasheet
&lt;li&gt; for milling with the 1/100" bit
&lt;li&gt; for milling with the 1/64" bit
&lt;/ol&gt;
&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="_1/100" package="DX4R005HJ5_100">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ORIG" package="DX4R005HJ5">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1/64" package="DX4R005HJ5_64">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="USB_MICRO_609-4613-1-ND">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X5">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.1524" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<text x="-1.27" y="3.937" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="22"/>
</package>
<package name="2X5-RA">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Male Headers&lt;/h3&gt;
tDocu shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.8" y1="6.3" x2="5.3" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="6.3" x2="5.3" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-6.3" x2="2.8" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-6.3" x2="2.8" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="0" x2="11.3" y2="0" width="0.127" layer="51"/>
<wire x1="5.3" y1="-2.54" x2="11.3" y2="-2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="-5.08" x2="11.3" y2="-5.08" width="0.127" layer="51"/>
<wire x1="5.3" y1="2.54" x2="11.3" y2="2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="5.08" x2="11.3" y2="5.08" width="0.127" layer="51"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<wire x1="13.8" y1="6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="6.3" x2="13.8" y2="6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="-6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
<text x="2.54" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.54" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-RAF">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Female Header&lt;/h3&gt;
Silk outline of pins
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.7" y1="6.3" x2="11.2" y2="6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="6.3" x2="11.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="-6.3" x2="2.7" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-6.3" x2="2.7" y2="6.3" width="0.2032" layer="21"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="3.175" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.175" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.318" y="10.414" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5-SHROUDED_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="-2.2" x2="-4.627" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.627" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-4.445" y1="10.16" x2="-4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="10.16" x2="-3.175" y2="10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="10.16" x2="4.445" y2="10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="10.16" x2="4.445" y2="8.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-8.89" x2="-4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-10.16" x2="-3.175" y2="-10.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="-10.16" x2="4.445" y2="-10.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="-10.16" x2="4.445" y2="-8.89" width="0.127" layer="21"/>
</package>
<package name="2X5-SHROUDED_SMD">
<description>&lt;h3&gt;Surface Mount - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.315" y1="5.715" x2="-5.315" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<smd name="1" x="-2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="2" x="2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="3" x="-2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="4" x="2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="5" x="-2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="6" x="2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="7" x="-2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="8" x="2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="9" x="-2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="10" x="2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-5.353" y1="5.715" x2="-5.353" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="2X5_NOSILK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="51"/>
<text x="-0.889" y="3.81" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-2.159" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5_PTH_SILK_.05">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
Holes are 0.05". 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="0.4318" rot="R90"/>
<pad name="2" x="0" y="1.27" drill="0.4318" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.4318" rot="R90"/>
<pad name="4" x="1.27" y="1.27" drill="0.4318" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="0.4318" rot="R90"/>
<pad name="6" x="2.54" y="1.27" drill="0.4318" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.4318" rot="R90"/>
<pad name="8" x="3.81" y="1.27" drill="0.4318" rot="R90"/>
<pad name="9" x="5.08" y="0" drill="0.4318" rot="R90"/>
<pad name="10" x="5.08" y="1.27" drill="0.4318" rot="R90"/>
<wire x1="-0.635" y1="0.635" x2="-0.762" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="1.778" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.778" x2="-0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="-0.508" y1="2.032" x2="0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.508" y1="2.032" x2="0.635" y2="1.905" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.905" x2="0.762" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.762" y1="2.032" x2="1.778" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.778" y1="2.032" x2="1.905" y2="1.905" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.032" y2="2.032" width="0.127" layer="21"/>
<wire x1="2.032" y1="2.032" x2="3.048" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.048" y1="2.032" x2="3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.905" x2="3.302" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.302" y1="2.032" x2="4.318" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.318" y1="2.032" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.572" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.572" y1="2.032" x2="5.588" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.588" y1="2.032" x2="5.842" y2="1.778" width="0.127" layer="21"/>
<wire x1="5.842" y1="1.778" x2="5.842" y2="0.762" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.762" x2="5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.715" y1="0.635" x2="5.842" y2="0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.508" x2="5.842" y2="-0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="-0.508" x2="5.588" y2="-0.762" width="0.127" layer="21"/>
<wire x1="5.588" y1="-0.762" x2="4.572" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.572" y1="-0.762" x2="4.445" y2="-0.635" width="0.127" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="4.318" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.318" y1="-0.762" x2="3.302" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.048" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="2.032" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.032" y1="-0.762" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="0.762" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="-0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.508" x2="-0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="22"/>
<text x="-0.762" y="2.286" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED-NS">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header No Silk&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
</package>
<package name="1X04">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<text x="2.286" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.286" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="12.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="3.4" x2="12.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="12.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.25" y1="3.15" x2="12.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="3.15" x2="12.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="2.15" x2="12.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="0" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.016" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Locking Footprint&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4_LOCK">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="2.667" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-3.556" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_MALE">
<description>&lt;h3&gt;SMD - 4 Pin Right Angle Male Header&lt;/h3&gt;
tDocu layer shows pin locations.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
<text x="-4.318" y="6.731" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-4-PTH">
<description>&lt;h3&gt;JST Right Angle 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-3" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="0" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="0" drill="0.7" diameter="1.6"/>
<text x="-3.4" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<text x="0.7" y="0.9" size="0.8" layer="51">S</text>
<text x="2.7" y="0.9" size="0.8" layer="51">S</text>
<wire x1="-4.95" y1="-1.6" x2="-4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="6" x2="4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="6" x2="4.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-1.6" x2="-4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-1.6" x2="4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.6" x2="-4.3" y2="0" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.6" x2="4.3" y2="0" width="0.2032" layer="21"/>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="51"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1" y="0" drill="1.2" diameter="2.032" shape="long" rot="R90"/>
<pad name="2" x="3.6" y="0" drill="1.2" diameter="2.032" shape="long" rot="R90"/>
<pad name="3" x="6.9" y="0" drill="1.2" diameter="2.032" shape="long" rot="R90"/>
<pad name="4" x="10.6" y="0" drill="1.2" diameter="2.032" shape="long" rot="R90"/>
<text x="3.81" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.81" y="1.524" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST04_1MM_RA">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.2032" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.2032" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.2032" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1016" width="0.2032" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_VERTICAL_COMBO">
<description>&lt;h3&gt;SMD - 4 Pin Vertical Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;SMD Pad count:8&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-0.508" y="2.921" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_LONG">
<description>&lt;h3&gt;SMD - 4 Pin w/ Long Solder Pads&lt;/h3&gt;
No silk, but tDocu layer shows pin position. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<description>&lt;h3&gt;JST Vertical 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.jst-mfg.com/product/pdf/eng/ePH.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
<text x="-1.143" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 4 Pin Right-Angle Female Header&lt;/h3&gt;
Silk outline shows header location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.205" y1="4.25" x2="-5.205" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="4.25" x2="-5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="-4.25" x2="5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-5.205" y1="-4.25" x2="5.205" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-1.59" y1="6.8" x2="-0.95" y2="7.65" layer="51"/>
<rectangle x1="0.95" y1="6.8" x2="1.59" y2="7.65" layer="51"/>
<rectangle x1="-4.13" y1="6.8" x2="-3.49" y2="7.65" layer="51"/>
<smd name="3" x="1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<rectangle x1="3.49" y1="6.8" x2="4.13" y2="7.65" layer="51"/>
<smd name="4" x="3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SPRING_TERMINAL">
<wire x1="11.6" y1="-5.34" x2="-0.98" y2="-5.34" width="0.2032" layer="21"/>
<wire x1="-0.98" y1="-5.34" x2="-0.98" y2="4.9" width="0.2032" layer="21"/>
<wire x1="-0.98" y1="4.9" x2="11.6" y2="4.9" width="0.2032" layer="21"/>
<wire x1="11.6" y1="4.9" x2="11.6" y2="-5.34" width="0.2032" layer="21"/>
<pad name="1" x="7.62" y="2.54" drill="1.1" diameter="1.8" rot="R270"/>
<pad name="5" x="7.62" y="-2.54" drill="1.1" diameter="1.8" rot="R270"/>
<pad name="2" x="5.08" y="2.54" drill="1.1" diameter="1.8" rot="R270"/>
<pad name="3" x="2.54" y="2.54" drill="1.1" diameter="1.8" rot="R270"/>
<pad name="4" x="0" y="2.54" drill="1.1" diameter="1.8" rot="R270"/>
<rectangle x1="6.84" y1="3.3" x2="8.52" y2="6.3" layer="51" rot="R180"/>
<rectangle x1="4.3" y1="3.3" x2="5.98" y2="6.3" layer="51" rot="R180"/>
<rectangle x1="1.76" y1="3.3" x2="3.44" y2="6.3" layer="51" rot="R180"/>
<rectangle x1="-0.78" y1="3.3" x2="0.9" y2="6.3" layer="51" rot="R180"/>
<pad name="6" x="5.08" y="-2.54" drill="1.1" diameter="1.8" rot="R270"/>
<pad name="7" x="2.54" y="-2.54" drill="1.1" diameter="1.8" rot="R270"/>
<pad name="8" x="0" y="-2.54" drill="1.1" diameter="1.8" rot="R270"/>
<text x="1.27" y="0" size="0.762" layer="25">&gt;Name</text>
<text x="1.27" y="-1.27" size="0.762" layer="27">&gt;Value</text>
<rectangle x1="-0.78" y1="3.3" x2="0.9" y2="4.903" layer="21" rot="R180"/>
<rectangle x1="1.76" y1="3.3" x2="3.44" y2="4.903" layer="21" rot="R180"/>
<rectangle x1="4.3" y1="3.3" x2="5.98" y2="4.903" layer="21" rot="R180"/>
<rectangle x1="6.84" y1="3.3" x2="8.52" y2="4.903" layer="21" rot="R180"/>
</package>
<package name="1X04_1.27MM_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.016" size="0.6096" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.6096" layer="27" font="vector" ratio="12">&gt;VALUE</text>
</package>
<package name="2X5-PTH-1.27MM-NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.762" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.762" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.762" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.762" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.762" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.762" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.762" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.762" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.762" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.762" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="51"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.635" y1="-1.905" x2="0.635" y2="-1.905" width="0.254" layer="21"/>
<wire x1="5.2" y1="1.6" x2="-5.2" y2="1.6" width="0.127" layer="51"/>
<wire x1="-5.2" y1="1.6" x2="-5.2" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-5.2" y1="-1.6" x2="5.2" y2="-1.6" width="0.127" layer="51"/>
<wire x1="5.2" y1="-1.6" x2="5.2" y2="1.6" width="0.127" layer="51"/>
</package>
<package name="2X5-PTH-1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.635" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.635" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.635" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="21"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SMD-1.27MM">
<description>Shrouded SMD connector for JTAG and SWD applications.</description>
<smd name="6" x="0" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="8" x="-1.27" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="10" x="-2.54" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="4" x="1.27" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="2" x="2.54" y="-1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="1" x="2.54" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="3" x="1.27" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="5" x="0" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="7" x="-1.27" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<smd name="9" x="-2.54" y="1.95" dx="0.76" dy="2.4" layer="1" rot="R180"/>
<rectangle x1="-1.0575" y1="-1.9625" x2="1.0575" y2="-1.5525" layer="51" rot="R270"/>
<wire x1="5.55" y1="-1.7" x2="-5.55" y2="-1.7" width="0.1524" layer="51"/>
<wire x1="-5.55" y1="-1.7" x2="-5.55" y2="1.7" width="0.1524" layer="51"/>
<wire x1="-5.55" y1="1.7" x2="5.55" y2="1.7" width="0.1524" layer="51"/>
<wire x1="5.55" y1="1.7" x2="5.55" y2="-1.7" width="0.1524" layer="51"/>
<rectangle x1="-2.3275" y1="-1.9625" x2="-0.2125" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="-3.5975" y1="-1.9625" x2="-1.4825" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="0.2125" y1="-1.9625" x2="2.3275" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="1.4825" y1="-1.9625" x2="3.5975" y2="-1.5525" layer="51" rot="R270"/>
<rectangle x1="1.4825" y1="1.5525" x2="3.5975" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="0.2125" y1="1.5525" x2="2.3275" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="-1.0575" y1="1.5525" x2="1.0575" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="-2.3275" y1="1.5525" x2="-0.2125" y2="1.9625" layer="51" rot="R90"/>
<rectangle x1="-3.5975" y1="1.5525" x2="-1.4825" y2="1.9625" layer="51" rot="R90"/>
<wire x1="-3.2" y1="2.5" x2="-6.3" y2="2.5" width="0.2032" layer="51"/>
<wire x1="-6.3" y1="2.5" x2="-6.3" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-6.3" y1="-2.5" x2="-3.2" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-2.5" x2="6.3" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="6.3" y1="-2.5" x2="6.3" y2="2.5" width="0.2032" layer="51"/>
<wire x1="6.3" y1="2.5" x2="3.2" y2="2.5" width="0.2032" layer="51"/>
<wire x1="0.6" y1="2.9" x2="0.6" y2="3.4" width="0.2032" layer="21"/>
<wire x1="0.6" y1="3.4" x2="-0.6" y2="3.4" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="3.4" x2="-0.6" y2="2.9" width="0.2032" layer="21"/>
<circle x="3.6" y="3.1" radius="0.1" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.127" layer="51"/>
<wire x1="3.175" y1="1.905" x2="3.175" y2="-1.905" width="0.127" layer="51"/>
</package>
<package name="2X5-SHROUDED_LOCK_LATCH">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="16.1" x2="4.5" y2="-16.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-16.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="-2.2" x2="-4.627" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="16.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="16.1" x2="4.4" y2="16.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-16.1" x2="-4.5" y2="-16.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.627" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.627" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-4.445" y1="16.16" x2="-4.445" y2="14.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="16.16" x2="-3.175" y2="16.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="16.16" x2="4.445" y2="16.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="16.16" x2="4.445" y2="14.89" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-14.89" x2="-4.445" y2="-16.16" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-16.16" x2="-3.175" y2="-16.16" width="0.127" layer="21"/>
<wire x1="3.175" y1="-16.16" x2="4.445" y2="-16.16" width="0.127" layer="21"/>
<wire x1="4.445" y1="-16.16" x2="4.445" y2="-14.89" width="0.127" layer="21"/>
</package>
<package name="1X03">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<text x="1.143" y="2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint&lt;/h3&gt;
Pins are staggered 0.005" off center to lock pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pad w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<text x="-1.27" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3_LOCK">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<text x="1.143" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.007" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="2.032" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="1.905" y="-4.699" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-5MM-3">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="3.683" y="2.794" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.429" y="1.905" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint w/out Silk Outline&lt;/h3&gt;
Holes are offset from center 0.005" to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-SMD">
<description>&lt;h3&gt;JST 3 Pin Right Angle SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="3" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="1" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-1.397" y="0.635" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03-1MM-RA">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin SMD&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Female Header&lt;/h3&gt;
Silk outline of pin location
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.524" y="0.889" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE">
<description>&lt;h3&gt;SMD- 3 Pin Right Angle Male Headers&lt;/h3&gt;
No silk outline, but tDocu layer shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<text x="-1.524" y="0.254" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
<text x="-1.397" y="1.524" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH-VERT">
<description>&lt;h3&gt;JST 3 Pin Vertical Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
<text x="-1.397" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLER">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3589"/>
<hole x="1.27" y="0" drill="1.3589"/>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLEST">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3462"/>
<hole x="1.27" y="0" drill="1.3462"/>
</package>
<package name="JST-3-PTH-NS">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_05X2">
<description>&lt;h3&gt;10 Pin Connection&lt;/h3&gt;
5x2 pin layout</description>
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.81" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CONN_04">
<description>&lt;h3&gt;4 Pin Connection&lt;/h3&gt;</description>
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CORTEX_DEBUG">
<description>&lt;h3&gt;Cortex Debug Connector&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="VCC" x="-15.24" y="5.08" length="short"/>
<pin name="GND@3" x="-15.24" y="2.54" length="short"/>
<pin name="GND@5" x="-15.24" y="0" length="short"/>
<pin name="KEY" x="-15.24" y="-2.54" length="short"/>
<pin name="GNDDTCT" x="-15.24" y="-5.08" length="short"/>
<pin name="!RESET" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="NC/TDI" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="SWO/TDO" x="15.24" y="0" length="short" rot="R180"/>
<pin name="SWDCLK/TCK" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="SWDIO/TMS" x="15.24" y="5.08" length="short" rot="R180"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<text x="-12.7" y="7.874" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-12.7" y="-9.906" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="CONN_03">
<description>&lt;h3&gt;3 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_05X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;h3&gt;For AVR SPI programming port, see special device with nice symbol: "AVR_SPI_PROG_5x2.dev"&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with any combo of single row headers, but if you'd like an exact match, check these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/778"&gt; 2x5 AVR ICSP Male Header&lt;/a&gt; (PRT-00778)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8506"&gt; 2x5 Pin Shrouded Header&lt;/a&gt; (PRT-08506)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note: the shrouded connector mates well with our 5x2 ribbon cables:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8535"&gt; 2x5 Pin IDC Ribbon Cable&lt;/a&gt; (PRT-08535)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_05X2" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="2X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08499" constant="no"/>
<attribute name="SF_ID" value="PRT-0778" constant="no"/>
</technology>
</technologies>
</device>
<device name="RA" package="2X5-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RAF" package="2X5-RAF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD" package="2X5-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK" package="2X5-SHROUDED_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_SMD" package="2X5-SHROUDED_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09508" constant="no"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X5_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.05_IN_PTH_SILK" package="2X5_PTH_SILK_.05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD-NS" package="2X5-SHROUDED-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK_LATCH" package="2X5-SHROUDED_LOCK_LATCH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_04" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="J1" symbol="CONN_04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X04">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14485"/>
</technology>
</technologies>
</device>
<device name="1.27MM" package="1X04_1.27MM">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X04_SMD_RA_MALE">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-13531" constant="no"/>
<attribute name="SF_ID" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="JST04_1MM_RA">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10310" constant="no"/>
<attribute name="SF_ID" value="PRT-10208" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_VERTICAL_COMBO">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08511"/>
<attribute name="VALUE" value="1X04_SMD_STRAIGHT_COMBO"/>
</technology>
</technologies>
</device>
<device name="SMD_LONG" package="1X04_SMD_LONG">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13251"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X04_SMD_RA_FEMALE">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12382" constant="no"/>
</technology>
</technologies>
</device>
<device name="SPRING" package="1X04_SPRING_TERMINAL">
<connects>
<connect gate="J1" pin="1" pad="1 5"/>
<connect gate="J1" pin="2" pad="2 6"/>
<connect gate="J1" pin="3" pad="3 7"/>
<connect gate="J1" pin="4" pad="4 8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14363" constant="no"/>
<attribute name="SF_ID" value="PRT-08075" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.27MM_NO_SILK" package="1X04_1.27MM_NO_SILK">
<connects>
<connect gate="J1" pin="1" pad="1"/>
<connect gate="J1" pin="2" pad="2"/>
<connect gate="J1" pin="3" pad="3"/>
<connect gate="J1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CORTEX_JTAG_DEBUG" prefix="J">
<description>&lt;h3&gt;Cortex Debug Connector - 10 pin&lt;/h3&gt;
&lt;p&gt;Supports JTAG debug, Serial Wire debug, and Serial Wire Viewer.
PTH and SMD connector options available.&lt;/p&gt;
&lt;p&gt; &lt;ul&gt;&lt;a href=”http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf”&gt;General Connector Information&lt;/a&gt;
&lt;p&gt;&lt;b&gt; Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”http://www.digikey.com/product-detail/en/cnc-tech/3220-10-0100-00/1175-1627-ND/3883661”&gt;PTH Connector&lt;/a&gt; -via Digi-Key&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13229”&gt;SparkFun PSoc&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13810”&gt;SparkFun T&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="J1" symbol="CORTEX_DEBUG" x="0" y="0"/>
</gates>
<devices>
<device name="_PTH_NS" package="2X5-PTH-1.27MM-NO_SILK">
<connects>
<connect gate="J1" pin="!RESET" pad="10"/>
<connect gate="J1" pin="GND@3" pad="3"/>
<connect gate="J1" pin="GND@5" pad="5"/>
<connect gate="J1" pin="GNDDTCT" pad="9"/>
<connect gate="J1" pin="KEY" pad="7"/>
<connect gate="J1" pin="NC/TDI" pad="8"/>
<connect gate="J1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="J1" pin="SWDIO/TMS" pad="2"/>
<connect gate="J1" pin="SWO/TDO" pad="6"/>
<connect gate="J1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PTH" package="2X5-PTH-1.27MM">
<connects>
<connect gate="J1" pin="!RESET" pad="10"/>
<connect gate="J1" pin="GND@3" pad="3"/>
<connect gate="J1" pin="GND@5" pad="5"/>
<connect gate="J1" pin="GNDDTCT" pad="9"/>
<connect gate="J1" pin="KEY" pad="7"/>
<connect gate="J1" pin="NC/TDI" pad="8"/>
<connect gate="J1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="J1" pin="SWDIO/TMS" pad="2"/>
<connect gate="J1" pin="SWO/TDO" pad="6"/>
<connect gate="J1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD" package="2X5-SMD-1.27MM">
<connects>
<connect gate="J1" pin="!RESET" pad="10"/>
<connect gate="J1" pin="GND@3" pad="3"/>
<connect gate="J1" pin="GND@5" pad="5"/>
<connect gate="J1" pin="GNDDTCT" pad="9"/>
<connect gate="J1" pin="KEY" pad="7"/>
<connect gate="J1" pin="NC/TDI" pad="8"/>
<connect gate="J1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="J1" pin="SWDIO/TMS" pad="2"/>
<connect gate="J1" pin="SWO/TDO" pad="6"/>
<connect gate="J1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-14503" constant="no"/>
<attribute name="VALUE" value="JTAG" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_03" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13875"&gt; Stackable Header - 3 Pin (Female, 0.1")&lt;/a&gt; (PRT-13875)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="J$1" symbol="CONN_03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XXX-00000" constant="no"/>
<attribute name="VALUE" value="455-1750-1-ND" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-10037" constant="no"/>
<attribute name="SF_ID" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10134" constant="no"/>
<attribute name="SF_SKU" value="PRT-08433" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="J$1" pin="1" pad="3"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12591" constant="no"/>
<attribute name="VALUE" value="3-PIN SMD" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13230" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLER" package="1X03_SMD_RA_MALE_POST_SMALLER">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11912" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLEST" package="1X03_SMD_RA_MALE_POST_SMALLEST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-NS" package="JST-3-PTH-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW-NS" package="SCREWTERMINAL-3.5MM-3-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="SOIC8_PAD">
<description>&lt;B&gt;Wide Plastic Gull Wing Small Outline Package&lt;/B&gt;</description>
<circle x="-1.615" y="2.92" radius="0.3" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="1.905" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="2" x="-2.8" y="0.645" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="3" x="-2.8" y="-0.625" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="4" x="-2.8" y="-1.895" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="8" x="2.8" y="1.905" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="7" x="2.8" y="0.635" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="6" x="2.8" y="-0.635" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="5" x="2.8" y="-1.905" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<text x="-2.159" y="2.8575" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<smd name="P$1" x="0" y="0" dx="2.41" dy="3.3" layer="1" thermals="no"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2" y1="-2.5" x2="2" y2="2.5" width="0.127" layer="51"/>
<wire x1="2" y1="2.5" x2="-2" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2" y1="2.5" x2="-2" y2="-2.5" width="0.127" layer="51"/>
</package>
<package name="SOIC8_PAD_THRU">
<description>&lt;B&gt;Wide Plastic Gull Wing Small Outline Package&lt;/B&gt;</description>
<circle x="-1.615" y="2.92" radius="0.3" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="1.905" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="2" x="-2.8" y="0.645" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="3" x="-2.8" y="-0.625" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="4" x="-2.8" y="-1.895" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="8" x="2.8" y="1.905" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="7" x="2.8" y="0.635" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="6" x="2.8" y="-0.635" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<smd name="5" x="2.8" y="-1.905" dx="0.65" dy="1.75" layer="1" rot="R270"/>
<text x="-2.159" y="2.8575" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<smd name="P$1" x="0" y="0" dx="2.41" dy="3.3" layer="1" thermals="no"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2" y1="-2.5" x2="2" y2="2.5" width="0.127" layer="51"/>
<wire x1="2" y1="2.5" x2="-2" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2" y1="2.5" x2="-2" y2="-2.5" width="0.127" layer="51"/>
<pad name="P$2" x="0" y="0.635" drill="0.6" shape="square"/>
<pad name="P$3" x="0" y="-0.635" drill="0.6" shape="square"/>
</package>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;, 5 lead</description>
<wire x1="-1.544" y1="0.713" x2="1.544" y2="0.713" width="0.1524" layer="51"/>
<wire x1="1.544" y1="0.713" x2="1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="1.544" y1="-0.712" x2="-1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="-1.544" y1="-0.712" x2="-1.544" y2="0.713" width="0.1524" layer="51"/>
<smd name="5" x="-0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<text x="-1.778" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.048" y="-1.778" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
<wire x1="-1.5" y1="-1.9" x2="-1.5" y2="-1.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="A4950">
<pin name="GND" x="-15.24" y="7.62" length="middle"/>
<pin name="IN2" x="-15.24" y="2.54" length="middle"/>
<pin name="IN1" x="-15.24" y="-2.54" length="middle"/>
<pin name="VREF" x="-15.24" y="-7.62" length="middle"/>
<pin name="VBB" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="OUT1" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="LSS" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="OUT2" x="15.24" y="7.62" length="middle" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="VREG-AP2112">
<pin name="VIN" x="-12.7" y="2.54" length="middle"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4950" prefix="U">
<gates>
<gate name="G$1" symbol="A4950" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC8_PAD">
<connects>
<connect gate="G$1" pin="GND" pad="1 P$1"/>
<connect gate="G$1" pin="IN1" pad="3"/>
<connect gate="G$1" pin="IN2" pad="2"/>
<connect gate="G$1" pin="LSS" pad="7"/>
<connect gate="G$1" pin="OUT1" pad="6"/>
<connect gate="G$1" pin="OUT2" pad="8"/>
<connect gate="G$1" pin="VBB" pad="5"/>
<connect gate="G$1" pin="VREF" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THRU" package="SOIC8_PAD_THRU">
<connects>
<connect gate="G$1" pin="GND" pad="1 P$1 P$2 P$3"/>
<connect gate="G$1" pin="IN1" pad="3"/>
<connect gate="G$1" pin="IN2" pad="2"/>
<connect gate="G$1" pin="LSS" pad="7"/>
<connect gate="G$1" pin="OUT1" pad="6"/>
<connect gate="G$1" pin="OUT2" pad="8"/>
<connect gate="G$1" pin="VBB" pad="5"/>
<connect gate="G$1" pin="VREF" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VREG-AP2112" prefix="U">
<gates>
<gate name="G$1" symbol="VREG-AP2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.95" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="0805-DIODE">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
<package name="SOD-123HE">
<smd name="P$1" x="0.8" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="P$2" x="-1.55" y="0" dx="0.9" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="-0.9" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.9" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="0.5" y2="-0.9" width="0.127" layer="21"/>
<text x="0" y="1.8" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SMA-403D">
<smd name="P$1" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="P$2" x="2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.2" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.6" x2="-1.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.2" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-1.6" x2="2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="1.6" x2="2" y2="1.6" width="0.127" layer="21"/>
<text x="0" y="2.4" size="1.27" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
<package name="SMD-ELEC-CAP">
<wire x1="-3.25" y1="-3.3" x2="-3.25" y2="2.3" width="0.127" layer="51"/>
<wire x1="-3.25" y1="2.3" x2="-2.25" y2="3.3" width="0.127" layer="51"/>
<wire x1="-2.25" y1="3.3" x2="2.25" y2="3.3" width="0.127" layer="51"/>
<wire x1="2.25" y1="3.3" x2="3.25" y2="2.3" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.3" x2="3.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="3.25" y1="-3.3" x2="-3.25" y2="-3.3" width="0.127" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.08" layer="51"/>
<smd name="A" x="0" y="2.65" dx="3.6" dy="1.8" layer="1" rot="R90" thermals="no"/>
<smd name="C" x="0" y="-2.65" dx="3.6" dy="1.8" layer="1" rot="R90" thermals="no"/>
<rectangle x1="-3.25" y1="-3.3" x2="3.25" y2="-2.3" layer="51"/>
<text x="-3.25" y="5.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.25" y="-6.3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.2" y1="2.35" x2="-2.3" y2="3.25" width="0.127" layer="21"/>
<wire x1="3.2" y1="2.35" x2="2.3" y2="3.25" width="0.127" layer="21"/>
</package>
<package name="0805">
<smd name="1" x="-1" y="0" dx="0.8" dy="1.3" layer="1"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.3" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.6" x2="1" y2="0.6" layer="51"/>
</package>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.032" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="TACT-SWITCH-SIDE">
<smd name="P$1" x="-1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$2" x="1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$3" x="-1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$4" x="1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<wire x1="-0.9" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0.9" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0.9" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.45" x2="1.75" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-1.75" y1="1.6" x2="-1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="0" y2="1.6" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="1" y2="1.6" width="0.127" layer="21"/>
<wire x1="1" y1="1.6" x2="1.75" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="-1" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1" y1="2.3" x2="1" y2="2.3" width="0.127" layer="21"/>
<wire x1="1" y1="2.3" x2="1" y2="1.6" width="0.127" layer="21"/>
</package>
<package name="744777920-INDUCTOR">
<smd name="P$1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="P$2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="-4" y1="0" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="4" y1="3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-4" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-4" y1="-3" x2="-4" y2="0" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-4" x2="4" y2="4" layer="39"/>
<text x="5.08" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="SPM6530-IND">
<smd name="1" x="0" y="2.775" dx="3.4" dy="1.85" layer="1"/>
<smd name="2" x="0" y="-2.775" dx="3.4" dy="1.85" layer="1"/>
<wire x1="-3.25" y1="3.85" x2="-3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.85" x2="3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.85" x2="3.25" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.85" x2="-3.25" y2="3.85" width="0.127" layer="21"/>
<text x="3.81" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="IHLP-5050FD-01-IND">
<smd name="1" x="0" y="5.4102" dx="4.953" dy="2.9464" layer="1"/>
<smd name="2" x="0" y="-5.4102" dx="4.953" dy="2.9464" layer="1"/>
<wire x1="6.4516" y1="6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="6.4516" y1="6.604" x2="3.81" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.604" x2="-6.4516" y2="6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="6.604" x2="-6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="-6.604" x2="-3.81" y2="-6.604" width="0.127" layer="21"/>
<text x="5.08" y="7.62" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="-8.89" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="7443340330-IND">
<smd name="P$1" x="0" y="3.35" dx="3" dy="2.3" layer="1"/>
<smd name="P$2" x="0" y="-3.35" dx="3" dy="2.3" layer="1"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<text x="3" y="5" size="1.016" layer="25">&gt;NAME</text>
<text x="3" y="-6" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.15" y1="2.95" x2="1.15" y2="4.45" layer="51"/>
<rectangle x1="-1.15" y1="-4.45" x2="1.15" y2="-2.95" layer="51"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="8X8-IND">
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="744029100-IND">
<smd name="1" x="0" y="1.1" dx="3.2" dy="1" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="3.2" dy="1" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447709470-IND">
<smd name="1" x="0" y="4.95" dx="5.4" dy="2.9" layer="1"/>
<smd name="2" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<text x="-7" y="8" size="1.27" layer="25">&gt;NAME</text>
<text x="-7" y="-9" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447789002-IND">
<smd name="1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="NRS5020">
<smd name="P$1" x="-1.8" y="0" dx="1.5" dy="4" layer="1"/>
<smd name="P$2" x="1.8" y="0" dx="1.5" dy="4" layer="1"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.127" layer="51"/>
</package>
<package name="DIPSWITCH-8-0100">
<smd name="P$1" x="-8.89" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$2" x="-6.35" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$3" x="-3.81" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$4" x="-1.27" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$5" x="1.27" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$6" x="3.81" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$7" x="6.35" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$8" x="8.89" y="-4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$9" x="8.89" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$10" x="6.35" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$11" x="3.81" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$12" x="1.27" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$13" x="-1.27" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$14" x="-3.81" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$15" x="-6.35" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<smd name="P$16" x="-8.89" y="4.445" dx="2.6" dy="1.3" layer="1" rot="R90"/>
<rectangle x1="-10.895" y1="-3.34" x2="10.895" y2="3.34" layer="51"/>
<text x="-9.525" y="1.27" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="DIPSWITCH-8">
<smd name="P$1" x="-4.445" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$2" x="-3.175" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$3" x="-1.905" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$4" x="-0.635" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$5" x="0.635" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$6" x="1.905" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$7" x="3.175" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$8" x="4.445" y="-2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$9" x="4.445" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$10" x="3.175" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$11" x="1.905" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$12" x="0.635" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$13" x="-0.635" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$14" x="-1.905" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$15" x="-3.175" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<smd name="P$16" x="-4.445" y="2.0447" dx="1.27" dy="0.7366" layer="1" rot="R90"/>
<rectangle x1="-5.6642" y1="-2.8956" x2="5.6642" y2="2.8956" layer="51"/>
<text x="-5.08" y="0" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="4-SMD-3.2X2.5">
<smd name="P$1" x="-1.1" y="-0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$2" x="1.1" y="-0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$3" x="1.1" y="0.8" dx="1.4" dy="1.2" layer="1"/>
<smd name="P$4" x="-1.1" y="0.8" dx="1.4" dy="1.2" layer="1"/>
<rectangle x1="-1.6" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
<circle x="-2" y="1.6" radius="0.14141875" width="0.127" layer="21"/>
</package>
<package name="2-SMD-3.2X1.5MM">
<smd name="P$1" x="-1.25" y="0" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<smd name="P$2" x="1.25" y="0" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<wire x1="-0.6" y1="0.9" x2="0.6" y2="0.9" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.9" x2="0.6" y2="-0.9" width="0.127" layer="51"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CAP-ELECTROLYTIC">
<pin name="C" x="0" y="-2.54" visible="off" length="short" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.032" y1="1.778" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="2.032" y2="1.778" width="0.254" layer="94"/>
<wire x1="-2.032" y1="0.762" x2="2.032" y2="0.762" width="0.254" layer="94" curve="-33.398492"/>
<wire x1="0" y1="1.016" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.048" x2="1.778" y2="3.048" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.556" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="6.35" y="-5.08" size="1.27" layer="97" rot="R90">&gt;PACKAGE</text>
</symbol>
<symbol name="DIPSWITCH-8">
<pin name="1" x="-12.7" y="7.62" length="middle"/>
<pin name="2" x="-12.7" y="5.08" length="middle"/>
<pin name="3" x="-12.7" y="2.54" length="middle"/>
<pin name="4" x="-12.7" y="0" length="middle"/>
<pin name="5" x="-12.7" y="-2.54" length="middle"/>
<pin name="6" x="-12.7" y="-5.08" length="middle"/>
<pin name="7" x="-12.7" y="-7.62" length="middle"/>
<pin name="8" x="-12.7" y="-10.16" length="middle"/>
<pin name="9" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="10" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="11" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="12" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="13" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="14" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="15" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="16" x="12.7" y="7.62" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="CRYSTAL-MHZ">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="GND" x="0" y="-7.62" length="middle" rot="R90"/>
</symbol>
<symbol name="RESONATOR">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIODE" package="0805-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD123HE" package="SOD-123HE">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA403D" package="SMA-403D">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP-POL" prefix="C">
<gates>
<gate name="G$1" symbol="CAP-ELECTROLYTIC" x="0" y="0"/>
</gates>
<devices>
<device name="CAP_POL_SMD6.6X6.6" package="SMD-ELEC-CAP">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="TACT-SWITCH-SIDE">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-744777920" package="744777920-INDUCTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
</technology>
</technologies>
</device>
<device name="-SPM6530" package="SPM6530-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IHLP-5050FD-01" package="IHLP-5050FD-01-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7443340330" package="7443340330-IND">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="7443340330"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
</technology>
</technologies>
</device>
<device name="-744778002" package="8X8-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-744029100" package="744029100-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447709470" package="7447709470-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447789002" package="7447789002-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="NRS5020">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0603" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIPSWITCH-8" prefix="J">
<gates>
<gate name="G$1" symbol="DIPSWITCH-8" x="0" y="0"/>
</gates>
<devices>
<device name="MICRO" package="DIPSWITCH-8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="10" pad="P$10"/>
<connect gate="G$1" pin="11" pad="P$11"/>
<connect gate="G$1" pin="12" pad="P$12"/>
<connect gate="G$1" pin="13" pad="P$13"/>
<connect gate="G$1" pin="14" pad="P$14"/>
<connect gate="G$1" pin="15" pad="P$15"/>
<connect gate="G$1" pin="16" pad="P$16"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
<connect gate="G$1" pin="7" pad="P$7"/>
<connect gate="G$1" pin="8" pad="P$8"/>
<connect gate="G$1" pin="9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0100" package="DIPSWITCH-8-0100">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="10" pad="P$10"/>
<connect gate="G$1" pin="11" pad="P$11"/>
<connect gate="G$1" pin="12" pad="P$12"/>
<connect gate="G$1" pin="13" pad="P$13"/>
<connect gate="G$1" pin="14" pad="P$14"/>
<connect gate="G$1" pin="15" pad="P$15"/>
<connect gate="G$1" pin="16" pad="P$16"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
<connect gate="G$1" pin="7" pad="P$7"/>
<connect gate="G$1" pin="8" pad="P$8"/>
<connect gate="G$1" pin="9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MHZ-CRYSTAL" prefix="Y">
<gates>
<gate name="G$1" symbol="CRYSTAL-MHZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4-SMD-3.2X2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$3"/>
<connect gate="G$1" pin="GND" pad="P$2 P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KHZ-CRYSTAL" prefix="Y">
<gates>
<gate name="G$1" symbol="RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2-SMD-3.2X1.5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+24V" urn="urn:adsk.eagle:symbol:26935/1">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" urn="urn:adsk.eagle:component:26964/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="sensor">
<packages>
<package name="TSSOP14">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 14&lt;/b&gt;</description>
<wire x1="-2.5146" y1="-2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="2.0828" x2="2.5146" y2="-2.0828" width="0.1524" layer="51"/>
<wire x1="2.5146" y1="2.0828" x2="-2.5146" y2="2.0828" width="0.1524" layer="51"/>
<wire x1="-2.5146" y1="-2.0828" x2="-2.5146" y2="2.0828" width="0.1524" layer="51"/>
<circle x="-3.0956" y="-1.6192" radius="0.3048" width="0.1524" layer="21"/>
<smd name="1" x="-1.905" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-1.27" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-0.635" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="0" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="0.635" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="1.27" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="1.905" y="-2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="-1.905" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="-1.27" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="-0.635" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="0" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="0.635" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="1.27" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.905" y="2.7178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-2.8956" y="-2.0828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.8862" y="-2.0828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="1.8034" y1="2.1082" x2="2.0066" y2="2.9464" layer="51"/>
<rectangle x1="1.1684" y1="2.1082" x2="1.3716" y2="2.9464" layer="51"/>
<rectangle x1="0.5334" y1="2.1082" x2="0.7366" y2="2.9464" layer="51"/>
<rectangle x1="-0.1016" y1="2.1082" x2="0.1016" y2="2.9464" layer="51"/>
<rectangle x1="-0.7366" y1="2.1082" x2="-0.5334" y2="2.9464" layer="51"/>
<rectangle x1="-1.3716" y1="2.1082" x2="-1.1684" y2="2.9464" layer="51"/>
<rectangle x1="-2.0066" y1="2.1082" x2="-1.8034" y2="2.9464" layer="51"/>
<rectangle x1="-2.0066" y1="-2.921" x2="-1.8034" y2="-2.0828" layer="51"/>
<rectangle x1="-1.3716" y1="-2.921" x2="-1.1684" y2="-2.0828" layer="51"/>
<rectangle x1="-0.7366" y1="-2.921" x2="-0.5334" y2="-2.0828" layer="51"/>
<rectangle x1="-0.1016" y1="-2.921" x2="0.1016" y2="-2.0828" layer="51"/>
<rectangle x1="0.5334" y1="-2.921" x2="0.7366" y2="-2.0828" layer="51"/>
<rectangle x1="1.1684" y1="-2.921" x2="1.3716" y2="-2.0828" layer="51"/>
<rectangle x1="1.8034" y1="-2.921" x2="2.0066" y2="-2.0828" layer="51"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT 23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="0.762" dy="1.016" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="0.762" dy="1.016" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="0.762" dy="1.016" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="AS5047">
<pin name="MOSI" x="-15.24" y="0" length="middle"/>
<pin name="MISO" x="-15.24" y="2.54" length="middle"/>
<pin name="CLK" x="-15.24" y="5.08" length="middle"/>
<pin name="CSN" x="-15.24" y="7.62" length="middle"/>
<pin name="TEST" x="-15.24" y="-2.54" length="middle"/>
<pin name="A" x="-15.24" y="-5.08" length="middle"/>
<pin name="B" x="-15.24" y="-7.62" length="middle"/>
<pin name="I" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="VDD3V" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="U" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="V" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="W" x="15.24" y="-7.62" length="middle" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="RTD-NI1000SOT">
<pin name="TERM1" x="-12.7" y="5.08" length="middle"/>
<pin name="TERM2" x="-12.7" y="-5.08" length="middle"/>
<pin name="SENSE" x="12.7" y="0" length="middle" rot="R180"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AS5047" prefix="U">
<gates>
<gate name="G$1" symbol="AS5047" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP14">
<connects>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="CLK" pad="2"/>
<connect gate="G$1" pin="CSN" pad="1"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="I" pad="14"/>
<connect gate="G$1" pin="MISO" pad="3"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="TEST" pad="5"/>
<connect gate="G$1" pin="U" pad="10"/>
<connect gate="G$1" pin="V" pad="9"/>
<connect gate="G$1" pin="VDD" pad="11"/>
<connect gate="G$1" pin="VDD3V" pad="12"/>
<connect gate="G$1" pin="W" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RTD-NI1000SOT" prefix="U">
<gates>
<gate name="G$1" symbol="RTD-NI1000SOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="SENSE" pad="3"/>
<connect gate="G$1" pin="TERM1" pad="1"/>
<connect gate="G$1" pin="TERM2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microcontrollers">
<packages>
<package name="QFN-64-9X9MM-SMALLPAD">
<description>&lt;h3&gt;64-pin QFN 9x9mm, 0.5mm pitch&lt;/h3&gt;
&lt;p&gt;Package used by ATmega128RFA1&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.atmel.com/Images/Atmel-8266-MCU_Wireless-ATmega128RFA1_Datasheet.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-4.492" y1="-4.5" x2="4.508" y2="-4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="-4.5" x2="4.508" y2="4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="4.5" x2="-4.492" y2="4.5" width="0.09" layer="51"/>
<wire x1="-4.492" y1="4.5" x2="-4.492" y2="-4.5" width="0.09" layer="51"/>
<wire x1="-4.6" y1="4.6" x2="-4.6" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="4.6" x2="-4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.6" y2="4.1" width="0.2032" layer="21"/>
<circle x="-4.842" y="4.85" radius="0.2" width="0" layer="21"/>
<circle x="-3.442" y="3.45" radius="0.2" width="0.09" layer="51"/>
<smd name="26" x="0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="25" x="0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="24" x="-0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="27" x="1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="28" x="1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="22" x="-1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="21" x="-1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="6" x="-4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="5" x="-4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="4" x="-4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="7" x="-4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="8" x="-4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="3" x="-4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="2" x="-4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="9" x="-4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="10" x="-4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="1" x="-4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="16" x="-4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="15" x="-4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="14" x="-4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="17" x="-3.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="18" x="-3.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="13" x="-4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="12" x="-4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="19" x="-2.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="20" x="-2.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="11" x="-4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="29" x="2.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="30" x="2.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="31" x="3.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="32" x="3.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="33" x="4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="34" x="4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="35" x="4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="36" x="4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="37" x="4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="38" x="4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="39" x="4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="40" x="4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="41" x="4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="42" x="4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="43" x="4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="44" x="4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="45" x="4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="46" x="4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="48" x="4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="49" x="3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="50" x="3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="51" x="2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="52" x="2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="53" x="1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="54" x="1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="55" x="0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="56" x="0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="57" x="-0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="58" x="-0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="59" x="-1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="60" x="-1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="61" x="-2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="62" x="-2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="63" x="-3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="64" x="-3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="4.6" y1="-4.6" x2="4.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-4.6" x2="4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.1" y2="-4.6" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="0" dx="4.8" dy="4.8" layer="1" cream="no"/>
<polygon width="0.127" layer="31">
<vertex x="1.03" y="1.03"/>
<vertex x="1.03" y="2.17"/>
<vertex x="2.17" y="2.17"/>
<vertex x="2.17" y="1.03"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.17" y="1.03"/>
<vertex x="-2.17" y="2.17"/>
<vertex x="-1.03" y="2.17"/>
<vertex x="-1.03" y="1.03"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.17" y="-2.17"/>
<vertex x="-2.17" y="-1.03"/>
<vertex x="-1.03" y="-1.03"/>
<vertex x="-1.03" y="-2.17"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.03" y="-2.17"/>
<vertex x="1.03" y="-1.03"/>
<vertex x="2.17" y="-1.03"/>
<vertex x="2.17" y="-2.17"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.57" y="-0.57"/>
<vertex x="-0.57" y="0.57"/>
<vertex x="0.57" y="0.57"/>
<vertex x="0.57" y="-0.57"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="ATSAMD51J">
<pin name="GND" x="-35.56" y="-132.08" length="middle"/>
<pin name="VDDCORE" x="-35.56" y="-45.72" length="middle"/>
<pin name="VDDANA" x="-35.56" y="-15.24" length="middle"/>
<pin name="VDDIO" x="-35.56" y="0" length="middle"/>
<pin name="PA00/XIN32/SER1-0/TC2-0" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="PA01/XOUT32/SER1-1/TC2-1" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="PA02/ADC0-1/DAC-0" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="PA03/ANAREF-VREFA/ADC0-1" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="PA07/ADC0-7/SER0-3/TC1-1" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" x="43.18" y="-27.94" length="middle" rot="R180"/>
<pin name="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" x="43.18" y="-30.48" length="middle" rot="R180"/>
<pin name="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" x="43.18" y="-33.02" length="middle" rot="R180"/>
<pin name="PA14/XIN0/SER2-2/SER4-2/TC3-0" x="43.18" y="-35.56" length="middle" rot="R180"/>
<pin name="PA15/XOUT0/SER2-3/SER4-3/TC3-1" x="43.18" y="-38.1" length="middle" rot="R180"/>
<pin name="PA16/SER1-0/SER3-1/TC2-0/TCC0-4" x="43.18" y="-40.64" length="middle" rot="R180"/>
<pin name="PA17/SER1-1/SER3-0/TC2-1/TCC0-5" x="43.18" y="-43.18" length="middle" rot="R180"/>
<pin name="PA18/SER1-2/SER3-2/TC3-0" x="43.18" y="-45.72" length="middle" rot="R180"/>
<pin name="PA19/SER1-3/SER3-3/TC3-1" x="43.18" y="-48.26" length="middle" rot="R180"/>
<pin name="PA20/SER5-2/SER3-2/TC7-0" x="43.18" y="-50.8" length="middle" rot="R180"/>
<pin name="PA21/SER5-3/SER3-3/TC7-1" x="43.18" y="-53.34" length="middle" rot="R180"/>
<pin name="PA22/SER3-0/SER5-1/TC4-0" x="43.18" y="-55.88" length="middle" rot="R180"/>
<pin name="PA23/SER3-1/SER5-0/TC4-1" x="43.18" y="-58.42" length="middle" rot="R180"/>
<pin name="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" x="43.18" y="-60.96" length="middle" rot="R180"/>
<pin name="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" x="43.18" y="-63.5" length="middle" rot="R180"/>
<pin name="PA27/GCLK-1" x="43.18" y="-66.04" length="middle" rot="R180"/>
<pin name="PA30/SER7-2/SER1-2/TC6-0/SWCLK" x="43.18" y="-68.58" length="middle" rot="R180"/>
<pin name="PA31/SER7-3/SER1-3/TC6-1/SWDIO" x="43.18" y="-71.12" length="middle" rot="R180"/>
<pin name="PB00/ADC0-12/SER5-2/TC7-0" x="43.18" y="-78.74" length="middle" rot="R180"/>
<pin name="PB01/ADC0-13/SER5-3/TC7-1" x="43.18" y="-81.28" length="middle" rot="R180"/>
<pin name="PB03/ADC0/SER5-1/TC6" x="43.18" y="-86.36" length="middle" rot="R180"/>
<pin name="PB04/ADC1-6" x="43.18" y="-88.9" length="middle" rot="R180"/>
<pin name="PB05/ADC1-7" x="43.18" y="-91.44" length="middle" rot="R180"/>
<pin name="PB06/ADC1-8" x="43.18" y="-93.98" length="middle" rot="R180"/>
<pin name="PB07/ADC1-9" x="43.18" y="-96.52" length="middle" rot="R180"/>
<pin name="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" x="43.18" y="-99.06" length="middle" rot="R180"/>
<pin name="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" x="43.18" y="-101.6" length="middle" rot="R180"/>
<pin name="PB10/SER4-2/TC5-0/TCC0-4" x="43.18" y="-104.14" length="middle" rot="R180"/>
<pin name="PB11/SER4-3/TC5-1/TCC0-5" x="43.18" y="-106.68" length="middle" rot="R180"/>
<pin name="PB12/SER4-0/TC4-0" x="43.18" y="-109.22" length="middle" rot="R180"/>
<pin name="PB13/SER4-1/TC4-1" x="43.18" y="-111.76" length="middle" rot="R180"/>
<pin name="PB14/SER4-2/TC5-0" x="43.18" y="-114.3" length="middle" rot="R180"/>
<pin name="PB15/SER4-3/TC5-1" x="43.18" y="-116.84" length="middle" rot="R180"/>
<pin name="PB16/SER5-0/TC6-0" x="43.18" y="-119.38" length="middle" rot="R180"/>
<pin name="PB17/SER5-1/TC6-1" x="43.18" y="-121.92" length="middle" rot="R180"/>
<pin name="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" x="43.18" y="-124.46" length="middle" rot="R180"/>
<pin name="PB23/XOUT1/SER1-3/SER5-3/TC7-1" x="43.18" y="-127" length="middle" rot="R180"/>
<pin name="PB30/SER7-0/SER5-1/TC0-0/SWO" x="43.18" y="-129.54" length="middle" rot="R180"/>
<pin name="RESETN" x="-35.56" y="-119.38" length="middle"/>
<pin name="PB31/SER7-1/SER5-0/TC0-1" x="43.18" y="-132.08" length="middle" rot="R180"/>
<pin name="PB02/ADC0-14/SER5-0/TC6-0" x="43.18" y="-83.82" length="middle" rot="R180"/>
<wire x1="-30.48" y1="5.08" x2="38.1" y2="5.08" width="0.254" layer="94"/>
<wire x1="38.1" y1="5.08" x2="38.1" y2="-137.16" width="0.254" layer="94"/>
<wire x1="38.1" y1="-137.16" x2="-30.48" y2="-137.16" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-137.16" x2="-30.48" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-142.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VSW" x="-35.56" y="-30.48" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD51J" prefix="U">
<gates>
<gate name="G$1" symbol="ATSAMD51J" x="0" y="0"/>
</gates>
<devices>
<device name="QFN64" package="QFN-64-9X9MM-SMALLPAD">
<connects>
<connect gate="G$1" pin="GND" pad="7 22 33 47 54 P$1"/>
<connect gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0" pad="1"/>
<connect gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1" pad="2"/>
<connect gate="G$1" pin="PA02/ADC0-1/DAC-0" pad="3"/>
<connect gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1" pad="4"/>
<connect gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" pad="13"/>
<connect gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" pad="14"/>
<connect gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" pad="15"/>
<connect gate="G$1" pin="PA07/ADC0-7/SER0-3/TC1-1" pad="16"/>
<connect gate="G$1" pin="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" pad="17"/>
<connect gate="G$1" pin="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" pad="18"/>
<connect gate="G$1" pin="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" pad="19"/>
<connect gate="G$1" pin="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" pad="20"/>
<connect gate="G$1" pin="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" pad="29"/>
<connect gate="G$1" pin="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" pad="30"/>
<connect gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0" pad="31"/>
<connect gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1" pad="32"/>
<connect gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0/TCC0-4" pad="35"/>
<connect gate="G$1" pin="PA17/SER1-1/SER3-0/TC2-1/TCC0-5" pad="36"/>
<connect gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0" pad="37"/>
<connect gate="G$1" pin="PA19/SER1-3/SER3-3/TC3-1" pad="38"/>
<connect gate="G$1" pin="PA20/SER5-2/SER3-2/TC7-0" pad="41"/>
<connect gate="G$1" pin="PA21/SER5-3/SER3-3/TC7-1" pad="42"/>
<connect gate="G$1" pin="PA22/SER3-0/SER5-1/TC4-0" pad="43"/>
<connect gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1" pad="44"/>
<connect gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" pad="45"/>
<connect gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" pad="46"/>
<connect gate="G$1" pin="PA27/GCLK-1" pad="51"/>
<connect gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK" pad="57"/>
<connect gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO" pad="58"/>
<connect gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0" pad="61"/>
<connect gate="G$1" pin="PB01/ADC0-13/SER5-3/TC7-1" pad="62"/>
<connect gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0" pad="63"/>
<connect gate="G$1" pin="PB03/ADC0/SER5-1/TC6" pad="64"/>
<connect gate="G$1" pin="PB04/ADC1-6" pad="5"/>
<connect gate="G$1" pin="PB05/ADC1-7" pad="6"/>
<connect gate="G$1" pin="PB06/ADC1-8" pad="9"/>
<connect gate="G$1" pin="PB07/ADC1-9" pad="10"/>
<connect gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" pad="11"/>
<connect gate="G$1" pin="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" pad="12"/>
<connect gate="G$1" pin="PB10/SER4-2/TC5-0/TCC0-4" pad="23"/>
<connect gate="G$1" pin="PB11/SER4-3/TC5-1/TCC0-5" pad="24"/>
<connect gate="G$1" pin="PB12/SER4-0/TC4-0" pad="25"/>
<connect gate="G$1" pin="PB13/SER4-1/TC4-1" pad="26"/>
<connect gate="G$1" pin="PB14/SER4-2/TC5-0" pad="27"/>
<connect gate="G$1" pin="PB15/SER4-3/TC5-1" pad="28"/>
<connect gate="G$1" pin="PB16/SER5-0/TC6-0" pad="39"/>
<connect gate="G$1" pin="PB17/SER5-1/TC6-1" pad="40"/>
<connect gate="G$1" pin="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" pad="49"/>
<connect gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1" pad="50"/>
<connect gate="G$1" pin="PB30/SER7-0/SER5-1/TC0-0/SWO" pad="59"/>
<connect gate="G$1" pin="PB31/SER7-1/SER5-0/TC0-1" pad="60"/>
<connect gate="G$1" pin="RESETN" pad="52"/>
<connect gate="G$1" pin="VDDANA" pad="8"/>
<connect gate="G$1" pin="VDDCORE" pad="53"/>
<connect gate="G$1" pin="VDDIO" pad="21 34 48 56"/>
<connect gate="G$1" pin="VSW" pad="55"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.0778" y1="0.2818" x2="0.1278" y2="0" width="0.127" layer="21"/>
<wire x1="0.1278" y1="0" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="0.2818" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
</package>
<package name="LED-5630">
<smd name="P$2" x="2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$3" x="2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$4" x="-2.6" y="0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<smd name="P$5" x="-2.6" y="-0.6" dx="0.6" dy="0.8" layer="1" thermals="no"/>
<rectangle x1="-2.5" y1="-1.5" x2="2.5" y2="1.5" layer="51"/>
<wire x1="0" y1="1.4" x2="0" y2="1.9" width="0.1016" layer="21"/>
<wire x1="0" y1="-1.4" x2="0" y2="-1.9" width="0.1016" layer="21"/>
<polygon width="0" layer="1">
<vertex x="-2.4" y="1"/>
<vertex x="-2.2" y="1"/>
<vertex x="-2" y="1.2"/>
<vertex x="-1.4" y="1.2"/>
<vertex x="-1.4" y="-1.2"/>
<vertex x="-2" y="-1.2"/>
<vertex x="-2.2" y="-1"/>
<vertex x="-2.4" y="-1"/>
</polygon>
<polygon width="0" layer="1">
<vertex x="2.4" y="1"/>
<vertex x="2.2" y="1"/>
<vertex x="2" y="1.2"/>
<vertex x="-0.8" y="1.2"/>
<vertex x="-0.8" y="-1.2"/>
<vertex x="2" y="-1.2"/>
<vertex x="2.2" y="-1"/>
<vertex x="2.4" y="-1"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="2.2" y="1.1"/>
<vertex x="2" y="1.3"/>
<vertex x="-0.9" y="1.3"/>
<vertex x="-0.9" y="-1.3"/>
<vertex x="2" y="-1.3"/>
<vertex x="2.2" y="-1.1"/>
<vertex x="3" y="-1.1"/>
<vertex x="3" y="-0.1"/>
<vertex x="2.4" y="-0.1"/>
<vertex x="2.4" y="0.1"/>
<vertex x="3" y="0.1"/>
<vertex x="3" y="1.1"/>
</polygon>
<polygon width="0" layer="29">
<vertex x="-3" y="1.1"/>
<vertex x="-2.2" y="1.1"/>
<vertex x="-2" y="1.3"/>
<vertex x="-1.3" y="1.3"/>
<vertex x="-1.3" y="-1.3"/>
<vertex x="-2" y="-1.3"/>
<vertex x="-2.2" y="-1.1"/>
<vertex x="-3" y="-1.1"/>
<vertex x="-3" y="-0.1"/>
<vertex x="-2.5" y="-0.1"/>
<vertex x="-2.5" y="0.1"/>
<vertex x="-3" y="0.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-2" y="1.1"/>
<vertex x="-2" y="-1.1"/>
<vertex x="-1.5" y="-1.1"/>
<vertex x="-1.5" y="1.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-0.7" y="1.1"/>
<vertex x="2" y="1.1"/>
<vertex x="2" y="-1.1"/>
<vertex x="-0.7" y="-1.1"/>
</polygon>
<wire x1="2.6" y1="-1.2" x2="2.8" y2="-1.4" width="0.1016" layer="21"/>
<wire x1="2.8" y1="-1.4" x2="2.6" y2="-1.6" width="0.1016" layer="21"/>
<wire x1="2.6" y1="-1.6" x2="2.6" y2="-1.2" width="0.1016" layer="21"/>
<wire x1="2.9" y1="-1.2" x2="2.9" y2="-1.6" width="0.1016" layer="21"/>
</package>
<package name="0402-D">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<smd name="1" x="-0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<smd name="2" x="0.525" y="0" dx="0.575" dy="0.7" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1" y1="-0.2" x2="-1" y2="0.2" width="0.127" layer="21"/>
</package>
<package name="LED0603-RIGHTANGLE">
<smd name="1" x="-1" y="0" dx="1.1" dy="1.1" layer="1"/>
<smd name="2" x="1" y="0" dx="1.1" dy="1.1" layer="1"/>
<text x="-0.089" y="1.197" size="0.8128" layer="25" align="center">&gt;NAME</text>
<text x="-0.016" y="-1.313" size="0.8128" layer="27" ratio="10" align="center">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.3818" x2="0.2278" y2="0" width="0.127" layer="21"/>
<wire x1="0.2278" y1="0" x2="-0.1778" y2="-0.3818" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.3818" x2="-0.1778" y2="-0.3818" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.2" x2="1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="1" y1="-0.2" x2="0.4" y2="-0.8" width="0.127" layer="51" curve="-90"/>
<wire x1="0.4" y1="-0.8" x2="-0.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.4" y1="-0.8" x2="-1" y2="-0.2" width="0.127" layer="51" curve="-90"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5630" package="LED-5630">
<connects>
<connect gate="G$1" pin="A" pad="P$4 P$5"/>
<connect gate="G$1" pin="C" pad="P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-D">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603RA" package="LED0603-RIGHTANGLE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="comm">
<packages>
<package name="SOP65P637X110-14N">
<rectangle x1="-0.901" y1="-0.9328" x2="0.901" y2="0.9328" layer="31"/>
<circle x="-4.355" y="2.36" radius="0.1" width="0.2" layer="21"/>
<circle x="-4.355" y="2.36" radius="0.1" width="0.2" layer="51"/>
<wire x1="-2.25" y1="2.55" x2="2.25" y2="2.55" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.55" x2="2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.55" x2="2.25" y2="2.55" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.55" x2="2.25" y2="-2.55" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.55" x2="-2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.55" x2="2.25" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-3.865" y1="2.8" x2="3.865" y2="2.8" width="0.05" layer="39"/>
<wire x1="-3.865" y1="-2.8" x2="3.865" y2="-2.8" width="0.05" layer="39"/>
<wire x1="-3.865" y1="2.8" x2="-3.865" y2="-2.8" width="0.05" layer="39"/>
<wire x1="3.865" y1="2.8" x2="3.865" y2="-2.8" width="0.05" layer="39"/>
<text x="-3.73" y="-2.727" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<text x="-3.73" y="2.727" size="1.27" layer="25">&gt;NAME</text>
<smd name="1" x="-2.875" y="1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="2" x="-2.875" y="1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="3" x="-2.875" y="0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="4" x="-2.875" y="0" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="5" x="-2.875" y="-0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="6" x="-2.875" y="-1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="7" x="-2.875" y="-1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="8" x="2.875" y="-1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="9" x="2.875" y="-1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="10" x="2.875" y="-0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="11" x="2.875" y="0" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="12" x="2.875" y="0.65" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="13" x="2.875" y="1.3" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="14" x="2.875" y="1.95" dx="1.48" dy="0.41" layer="1" roundness="25"/>
<smd name="15" x="0" y="0" dx="2.85" dy="2.95" layer="1" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="MAX13450EAUDT">
<wire x1="-12.7" y1="35.56" x2="12.7" y2="35.56" width="0.41" layer="94"/>
<wire x1="12.7" y1="35.56" x2="12.7" y2="-35.56" width="0.41" layer="94"/>
<wire x1="12.7" y1="-35.56" x2="-12.7" y2="-35.56" width="0.41" layer="94"/>
<wire x1="-12.7" y1="-35.56" x2="-12.7" y2="35.56" width="0.41" layer="94"/>
<text x="-12.7" y="36.56" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-12.7" y="-39.56" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="DI" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="!RE" x="-17.78" y="20.32" length="middle"/>
<pin name="!TERM" x="-17.78" y="15.24" length="middle"/>
<pin name="A" x="-17.78" y="10.16" length="middle"/>
<pin name="B" x="-17.78" y="5.08" length="middle"/>
<pin name="DE" x="-17.78" y="0" length="middle"/>
<pin name="RO" x="-17.78" y="-5.08" length="middle"/>
<pin name="SRL" x="-17.78" y="-10.16" length="middle"/>
<pin name="TERM100" x="-17.78" y="-15.24" length="middle"/>
<pin name="Y" x="-17.78" y="-20.32" length="middle"/>
<pin name="Z" x="-17.78" y="-25.4" length="middle"/>
<pin name="VCC" x="17.78" y="33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VL" x="17.78" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="EPAD" x="17.78" y="-30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-33.02" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MAX13450EAUDT" prefix="U">
<gates>
<gate name="A" symbol="MAX13450EAUDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P637X110-14N">
<connects>
<connect gate="A" pin="!RE" pad="2"/>
<connect gate="A" pin="!TERM" pad="5"/>
<connect gate="A" pin="A" pad="8"/>
<connect gate="A" pin="B" pad="9"/>
<connect gate="A" pin="DE" pad="1"/>
<connect gate="A" pin="DI" pad="3"/>
<connect gate="A" pin="EPAD" pad="15"/>
<connect gate="A" pin="GND" pad="11"/>
<connect gate="A" pin="RO" pad="7"/>
<connect gate="A" pin="SRL" pad="14"/>
<connect gate="A" pin="TERM100" pad="13"/>
<connect gate="A" pin="VCC" pad="4"/>
<connect gate="A" pin="VL" pad="6"/>
<connect gate="A" pin="Y" pad="10"/>
<connect gate="A" pin="Z" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" 1/1 Transceiver Full RS422, RS485 14-TSSOP-EP "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="MAX13450EAUD+T-ND"/>
<attribute name="DIGIKEY-PURCHASE-URL" value="https://snapeda.com/shop?store=DigiKey&amp;id=694117"/>
<attribute name="MF" value="Maxim Integrated"/>
<attribute name="MP" value="MAX13450EAUD+"/>
<attribute name="PACKAGE" value="TSSOP-14 Maxim"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J102" library="SparkFun-Connectors" deviceset="CONN_05X2" device="SHD_LOCK_LATCH"/>
<part name="U7" library="power" deviceset="A4950" device="" value="A4950"/>
<part name="U5" library="power" deviceset="A4950" device="" value="A4950"/>
<part name="J100" library="SparkFun-Connectors" deviceset="CONN_04" device="SCREW_LOCK"/>
<part name="R10" library="passives" deviceset="RESISTOR" device="0402" value="120R"/>
<part name="R14" library="passives" deviceset="RESISTOR" device="0402" value="120R"/>
<part name="J1" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="J2" library="connector" deviceset="PWRPAD" device="M3-SKINNY"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="C23" library="passives" deviceset="CAP" device="0805" value="22uF 35v"/>
<part name="C27" library="passives" deviceset="CAP" device="0805" value="22uF 35v"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="R21" library="passives" deviceset="RESISTOR" device="1206" value="100mOhm 1W 1%"/>
<part name="R20" library="passives" deviceset="RESISTOR" device="1206" value="100mOhm 1W 1%"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="P+1" library="supply1" deviceset="+24V" device=""/>
<part name="P+2" library="supply1" deviceset="+24V" device=""/>
<part name="R16" library="passives" deviceset="RESISTOR" device="0402" value="120R"/>
<part name="R15" library="passives" deviceset="RESISTOR" device="0402" value="120R"/>
<part name="R11" library="passives" deviceset="RESISTOR" device="0402" value="120R"/>
<part name="R12" library="passives" deviceset="RESISTOR" device="0402" value="120R"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="P+5" library="supply1" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" deviceset="+24V" device=""/>
<part name="P+6" library="supply1" deviceset="+24V" device=""/>
<part name="D4" library="passives" deviceset="DIODE" device="SMA403D"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="P+8" library="supply1" deviceset="+24V" device=""/>
<part name="U4" library="sensor" deviceset="AS5047" device=""/>
<part name="+3V8" library="supply1" deviceset="+3V3" device=""/>
<part name="C21" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R17" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="R19" library="passives" deviceset="RESISTOR" device="0402" value="1k"/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="P+10" library="supply1" deviceset="+24V" device=""/>
<part name="R13" library="passives" deviceset="RESISTOR" device="0402" value="470R"/>
<part name="C101" library="passives" deviceset="CAP-POL" device="CAP_POL_SMD6.6X6.6" value="150uF 35v"/>
<part name="U3" library="microcontrollers" deviceset="ATSAMD51J" device="QFN64"/>
<part name="J103" library="SparkFun-Connectors" deviceset="CORTEX_JTAG_DEBUG" device="_PTH_NS"/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="R3" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="S1" library="passives" deviceset="2-8X4-5_SWITCH" device="SIDE"/>
<part name="C4" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="X1" library="connector" deviceset="USB" device=""/>
<part name="P+9" library="supply1" deviceset="+5V" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="L1" library="passives" deviceset="INDUCTOR" device="-0805" value="10uH"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="C13" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C12" library="passives" deviceset="CAP" device="0603-CAP" value="10uF 10v"/>
<part name="C15" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C9" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C16" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="C20" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="U1" library="power" deviceset="VREG-AP2112" device=""/>
<part name="P+11" library="supply1" deviceset="+5V" device=""/>
<part name="C2" library="passives" deviceset="CAP" device="0603-CAP" value="10uF 10v"/>
<part name="C3" library="passives" deviceset="CAP" device="0603-CAP" value="10uF 10v"/>
<part name="C1" library="passives" deviceset="CAP" device="0402" value="1uF"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="J101" library="passives" deviceset="DIPSWITCH-8" device="0100"/>
<part name="+3V9" library="supply1" deviceset="+3V3" device=""/>
<part name="R6" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="R5" library="passives" deviceset="RESISTOR" device="0402" value="470R"/>
<part name="R4" library="passives" deviceset="RESISTOR" device="0402" value="470R"/>
<part name="R2" library="passives" deviceset="RESISTOR" device="0402" value="470R"/>
<part name="+3V10" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V11" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V12" library="supply1" deviceset="+3V3" device=""/>
<part name="Y1" library="passives" deviceset="MHZ-CRYSTAL" device=""/>
<part name="C14" library="passives" deviceset="CAP" device="0402" value="10pF"/>
<part name="C11" library="passives" deviceset="CAP" device="0402" value="10pF"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="C17" library="passives" deviceset="CAP" device="0402" value="10pF"/>
<part name="Y2" library="passives" deviceset="KHZ-CRYSTAL" device=""/>
<part name="C18" library="passives" deviceset="CAP" device="0402" value="10pF"/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="D1" library="lights" deviceset="LED" device="0603RA"/>
<part name="D2" library="lights" deviceset="LED" device="0603RA"/>
<part name="D3" library="lights" deviceset="LED" device="0603RA"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="U2" library="comm" deviceset="MAX13450EAUDT" device=""/>
<part name="C6" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="C8" library="passives" deviceset="CAP" device="0402" value="0.1uF 10v"/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="C5" library="passives" deviceset="CAP" device="0402" value="1uf 10v"/>
<part name="C10" library="passives" deviceset="CAP" device="0402" value="1uF 10v"/>
<part name="+3V13" library="supply1" deviceset="+3V3" device=""/>
<part name="P+12" library="supply1" deviceset="+5V" device=""/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="R7" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="R8" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="+3V14" library="supply1" deviceset="+3V3" device=""/>
<part name="C19" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="U6" library="sensor" deviceset="RTD-NI1000SOT" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="R18" library="passives" deviceset="RESISTOR" device="0402" value="1k"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="C26" library="passives" deviceset="CAP" device="0805" value="22uF 35v"/>
<part name="C22" library="passives" deviceset="CAP" device="0805" value="22uF 35v"/>
<part name="C25" library="passives" deviceset="CAP" device="0402" value="1uF 35v"/>
<part name="C24" library="passives" deviceset="CAP" device="0402" value="1uF 35v"/>
<part name="C7" library="passives" deviceset="CAP" device="0402" value="0.1uF"/>
<part name="R9" library="passives" deviceset="RESISTOR" device="0402" value="10k"/>
<part name="C100" library="passives" deviceset="CAP-POL" device="CAP_POL_SMD6.6X6.6" value="150uF 35v"/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_03" device="SMD_RA_MALE"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="22.86" y="198.12" size="1.778" layer="91">SIDE LEDS</text>
</plain>
<instances>
<instance part="J102" gate="G$1" x="340.36" y="185.42" smashed="yes">
<attribute name="VALUE" x="336.55" y="175.514" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="336.55" y="193.548" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="U7" gate="G$1" x="292.1" y="45.72" smashed="yes"/>
<instance part="U5" gate="G$1" x="292.1" y="88.9" smashed="yes"/>
<instance part="J100" gate="J1" x="421.64" y="96.52" smashed="yes" rot="R180">
<attribute name="VALUE" x="426.72" y="103.886" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="426.72" y="88.392" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="R10" gate="G$1" x="269.24" y="81.28" smashed="yes">
<attribute name="NAME" x="265.43" y="82.7786" size="1.778" layer="95"/>
<attribute name="VALUE" x="265.43" y="77.978" size="1.778" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="269.24" y="38.1" smashed="yes">
<attribute name="NAME" x="265.43" y="39.5986" size="1.778" layer="95"/>
<attribute name="VALUE" x="265.43" y="34.798" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="426.72" y="73.66" smashed="yes"/>
<instance part="J2" gate="G$1" x="426.72" y="78.74" smashed="yes"/>
<instance part="GND4" gate="1" x="246.38" y="96.52" smashed="yes" rot="R270">
<attribute name="VALUE" x="243.84" y="99.06" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND5" gate="1" x="246.38" y="53.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="243.84" y="55.88" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C23" gate="G$1" x="312.42" y="33.02" smashed="yes">
<attribute name="NAME" x="313.944" y="35.941" size="1.778" layer="95"/>
<attribute name="VALUE" x="313.944" y="30.861" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="313.944" y="28.956" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="313.944" y="27.178" size="1.27" layer="97"/>
<attribute name="TYPE" x="313.944" y="25.4" size="1.27" layer="97"/>
</instance>
<instance part="C27" gate="G$1" x="312.42" y="76.2" smashed="yes">
<attribute name="NAME" x="313.944" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="313.944" y="74.041" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="313.944" y="72.136" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="313.944" y="70.358" size="1.27" layer="97"/>
<attribute name="TYPE" x="313.944" y="68.58" size="1.27" layer="97"/>
</instance>
<instance part="GND6" gate="1" x="312.42" y="66.04" smashed="yes">
<attribute name="VALUE" x="309.88" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="312.42" y="22.86" smashed="yes">
<attribute name="VALUE" x="309.88" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="R21" gate="G$1" x="363.22" y="48.26" smashed="yes">
<attribute name="NAME" x="359.41" y="49.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="359.41" y="44.958" size="1.778" layer="96"/>
<attribute name="PRECISION" x="359.41" y="41.402" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="359.41" y="43.18" size="1.27" layer="97"/>
</instance>
<instance part="R20" gate="G$1" x="363.22" y="91.44" smashed="yes">
<attribute name="NAME" x="359.41" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="359.41" y="88.138" size="1.778" layer="96"/>
<attribute name="PRECISION" x="359.41" y="84.582" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="359.41" y="86.36" size="1.27" layer="97"/>
</instance>
<instance part="GND8" gate="1" x="373.38" y="81.28" smashed="yes">
<attribute name="VALUE" x="370.84" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="373.38" y="38.1" smashed="yes">
<attribute name="VALUE" x="370.84" y="35.56" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="358.14" y="81.28" smashed="yes" rot="R270">
<attribute name="VALUE" x="355.6" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="358.14" y="38.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="355.6" y="33.02" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="269.24" y="43.18" smashed="yes">
<attribute name="NAME" x="265.43" y="44.6786" size="1.778" layer="95"/>
<attribute name="VALUE" x="265.43" y="39.878" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="269.24" y="48.26" smashed="yes">
<attribute name="NAME" x="265.43" y="49.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="265.43" y="44.958" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="269.24" y="86.36" smashed="yes">
<attribute name="NAME" x="265.43" y="87.8586" size="1.778" layer="95"/>
<attribute name="VALUE" x="265.43" y="83.058" size="1.778" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="269.24" y="91.44" smashed="yes">
<attribute name="NAME" x="265.43" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="265.43" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="358.14" y="185.42" smashed="yes" rot="R90">
<attribute name="VALUE" x="360.68" y="182.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND11" gate="1" x="322.58" y="185.42" smashed="yes" rot="R270">
<attribute name="VALUE" x="320.04" y="187.96" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+4" gate="1" x="358.14" y="190.5" smashed="yes" rot="R270">
<attribute name="VALUE" x="360.68" y="190.5" size="1.778" layer="96"/>
</instance>
<instance part="P+5" gate="1" x="322.58" y="180.34" smashed="yes" rot="R90">
<attribute name="VALUE" x="320.04" y="180.34" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+3" gate="1" x="322.58" y="190.5" smashed="yes" rot="R90">
<attribute name="VALUE" x="320.04" y="190.5" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+6" gate="1" x="358.14" y="180.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="360.68" y="180.34" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="264.16" y="124.46" smashed="yes">
<attribute name="NAME" x="266.7" y="124.9426" size="1.778" layer="95"/>
<attribute name="VALUE" x="266.7" y="122.1486" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="246.38" y="124.46" smashed="yes" rot="R270">
<attribute name="VALUE" x="243.84" y="127" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+8" gate="1" x="281.94" y="124.46" smashed="yes" rot="R270">
<attribute name="VALUE" x="284.48" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="U4" gate="G$1" x="292.1" y="147.32" smashed="yes">
<attribute name="NAME" x="289.56" y="160.02" size="1.27" layer="95"/>
<attribute name="VALUE" x="289.56" y="134.62" size="1.27" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="335.28" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="330.2" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="C21" gate="G$1" x="317.5" y="147.32" smashed="yes">
<attribute name="NAME" x="319.024" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="319.024" y="145.161" size="1.778" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="335.28" y="152.4" smashed="yes" rot="R90">
<attribute name="VALUE" x="337.82" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R17" gate="G$1" x="375.92" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="374.4214" y="133.35" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="379.222" y="133.35" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R19" gate="G$1" x="375.92" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="374.4214" y="118.11" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="379.222" y="118.11" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="375.92" y="111.76" smashed="yes">
<attribute name="VALUE" x="373.38" y="109.22" size="1.778" layer="96"/>
</instance>
<instance part="P+10" gate="1" x="375.92" y="149.86" smashed="yes">
<attribute name="VALUE" x="373.38" y="144.78" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R13" gate="G$1" x="363.22" y="129.54" smashed="yes">
<attribute name="NAME" x="359.41" y="131.0386" size="1.778" layer="95"/>
<attribute name="VALUE" x="359.41" y="126.238" size="1.778" layer="96"/>
</instance>
<instance part="C101" gate="G$1" x="264.16" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="269.24" y="114.3" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="261.62" y="114.3" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U3" gate="G$1" x="154.94" y="152.4" smashed="yes">
<attribute name="NAME" x="149.86" y="160.02" size="1.778" layer="95"/>
<attribute name="VALUE" x="149.86" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="J103" gate="J1" x="50.8" y="38.1" smashed="yes">
<attribute name="NAME" x="38.1" y="45.974" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="38.1" y="28.194" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="+3V3" gate="G$1" x="25.4" y="50.8" smashed="yes">
<attribute name="VALUE" x="22.86" y="45.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND14" gate="1" x="25.4" y="25.4" smashed="yes">
<attribute name="VALUE" x="22.86" y="22.86" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="86.36" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="84.8614" y="46.99" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="89.662" y="46.99" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V4" gate="G$1" x="86.36" y="63.5" smashed="yes">
<attribute name="VALUE" x="83.82" y="58.42" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S1" gate="G$1" x="101.6" y="25.4" smashed="yes">
<attribute name="NAME" x="95.25" y="22.86" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="97.79" y="28.575" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="109.22" y="22.86" smashed="yes">
<attribute name="NAME" x="110.744" y="25.781" size="1.778" layer="95"/>
<attribute name="VALUE" x="110.744" y="20.701" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="40.64" y="76.2" smashed="yes" rot="R270">
<attribute name="NAME" x="38.735" y="80.645" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="38.735" y="67.945" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="P+9" gate="1" x="71.12" y="76.2" smashed="yes" rot="R270">
<attribute name="VALUE" x="73.66" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="63.5" y="78.74" smashed="yes" rot="R90">
<attribute name="VALUE" x="66.04" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="109.22" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="107.7214" y="46.99" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="112.522" y="46.99" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V5" gate="G$1" x="109.22" y="63.5" smashed="yes">
<attribute name="VALUE" x="106.68" y="58.42" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L1" gate="G$1" x="104.14" y="121.92" smashed="yes" rot="R270">
<attribute name="NAME" x="99.06" y="123.19" size="1.778" layer="95"/>
<attribute name="VALUE" x="99.06" y="118.11" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="99.06" y="115.57" size="1.27" layer="97"/>
</instance>
<instance part="GND16" gate="1" x="93.98" y="93.98" smashed="yes">
<attribute name="VALUE" x="91.44" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="109.22" y="10.16" smashed="yes">
<attribute name="VALUE" x="106.68" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="C13" gate="G$1" x="109.22" y="101.6" smashed="yes">
<attribute name="NAME" x="110.744" y="104.521" size="1.778" layer="95"/>
<attribute name="VALUE" x="110.744" y="99.441" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="93.98" y="101.6" smashed="yes">
<attribute name="NAME" x="95.504" y="104.521" size="1.778" layer="95"/>
<attribute name="VALUE" x="95.504" y="99.441" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="95.504" y="97.536" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="95.504" y="95.758" size="1.27" layer="97"/>
<attribute name="TYPE" x="95.504" y="93.98" size="1.27" layer="97"/>
</instance>
<instance part="C15" gate="G$1" x="88.9" y="147.32" smashed="yes">
<attribute name="NAME" x="90.424" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="90.424" y="145.161" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="78.74" y="147.32" smashed="yes">
<attribute name="NAME" x="80.264" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.264" y="145.161" size="1.778" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="68.58" y="147.32" smashed="yes">
<attribute name="NAME" x="70.104" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="70.104" y="145.161" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="58.42" y="147.32" smashed="yes">
<attribute name="NAME" x="59.944" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="59.944" y="145.161" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="58.42" y="134.62" smashed="yes">
<attribute name="VALUE" x="55.88" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="58.42" y="160.02" smashed="yes">
<attribute name="VALUE" x="55.88" y="154.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U1" gate="G$1" x="35.56" y="119.38" smashed="yes">
<attribute name="NAME" x="33.02" y="127" size="1.27" layer="95"/>
<attribute name="VALUE" x="38.1" y="111.76" size="1.27" layer="96"/>
</instance>
<instance part="P+11" gate="1" x="5.08" y="121.92" smashed="yes" rot="R90">
<attribute name="VALUE" x="2.54" y="121.92" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2" gate="G$1" x="12.7" y="116.84" smashed="yes">
<attribute name="NAME" x="14.224" y="119.761" size="1.778" layer="95"/>
<attribute name="VALUE" x="14.224" y="114.681" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="14.224" y="112.776" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="14.224" y="110.998" size="1.27" layer="97"/>
<attribute name="TYPE" x="14.224" y="109.22" size="1.27" layer="97"/>
</instance>
<instance part="C3" gate="G$1" x="53.34" y="116.84" smashed="yes">
<attribute name="NAME" x="54.864" y="119.761" size="1.778" layer="95"/>
<attribute name="VALUE" x="54.864" y="114.681" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="54.864" y="112.776" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="54.864" y="110.998" size="1.27" layer="97"/>
<attribute name="TYPE" x="54.864" y="109.22" size="1.27" layer="97"/>
</instance>
<instance part="C1" gate="G$1" x="68.58" y="116.84" smashed="yes">
<attribute name="NAME" x="70.104" y="119.761" size="1.778" layer="95"/>
<attribute name="VALUE" x="70.104" y="114.681" size="1.778" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="35.56" y="101.6" smashed="yes">
<attribute name="VALUE" x="33.02" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="+3V7" gate="G$1" x="81.28" y="121.92" smashed="yes" rot="R270">
<attribute name="VALUE" x="76.2" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="J101" gate="G$1" x="20.32" y="147.32" smashed="yes"/>
<instance part="+3V9" gate="G$1" x="50.8" y="160.02" smashed="yes">
<attribute name="VALUE" x="48.26" y="154.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="43.18" y="154.94" smashed="yes">
<attribute name="NAME" x="39.37" y="156.4386" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.37" y="151.638" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="68.58" y="182.88" smashed="yes">
<attribute name="NAME" x="64.77" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="64.77" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="55.88" y="187.96" smashed="yes">
<attribute name="NAME" x="52.07" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="52.07" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="43.18" y="193.04" smashed="yes">
<attribute name="NAME" x="39.37" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="39.37" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="+3V10" gate="G$1" x="81.28" y="182.88" smashed="yes" rot="R270">
<attribute name="VALUE" x="83.82" y="182.88" size="1.778" layer="96"/>
</instance>
<instance part="+3V11" gate="G$1" x="81.28" y="187.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="83.82" y="187.96" size="1.778" layer="96"/>
</instance>
<instance part="+3V12" gate="G$1" x="81.28" y="193.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="83.82" y="193.04" size="1.778" layer="96"/>
</instance>
<instance part="Y1" gate="G$1" x="60.96" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="59.944" y="213.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="63.5" y="213.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C14" gate="G$1" x="76.2" y="208.28" smashed="yes" rot="R270">
<attribute name="NAME" x="79.121" y="206.756" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="74.041" y="206.756" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C11" gate="G$1" x="78.74" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="75.819" y="214.884" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="80.899" y="214.884" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND22" gate="1" x="88.9" y="210.82" smashed="yes" rot="R90">
<attribute name="VALUE" x="91.44" y="208.28" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C17" gate="G$1" x="76.2" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="73.279" y="240.284" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.359" y="240.284" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="Y2" gate="G$1" x="60.96" y="236.22" smashed="yes" rot="R270">
<attribute name="NAME" x="61.976" y="233.68" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="58.42" y="233.68" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C18" gate="G$1" x="73.66" y="233.68" smashed="yes" rot="R270">
<attribute name="NAME" x="76.581" y="232.156" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="71.501" y="232.156" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND23" gate="1" x="88.9" y="236.22" smashed="yes" rot="R90">
<attribute name="VALUE" x="91.44" y="233.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D1" gate="G$1" x="27.94" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="25.908" y="189.484" size="1.778" layer="95"/>
<attribute name="VALUE" x="25.908" y="187.325" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="17.78" y="187.96" smashed="yes" rot="R270">
<attribute name="NAME" x="15.748" y="184.404" size="1.778" layer="95"/>
<attribute name="VALUE" x="15.748" y="182.245" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="35.56" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="33.528" y="179.324" size="1.778" layer="95"/>
<attribute name="VALUE" x="33.528" y="177.165" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="0" y="182.88" smashed="yes" rot="R270">
<attribute name="VALUE" x="-2.54" y="185.42" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U2" gate="A" x="180.34" y="208.28" smashed="yes">
<attribute name="NAME" x="167.64" y="244.84" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="167.64" y="168.72" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="C6" gate="G$1" x="203.2" y="233.68" smashed="yes">
<attribute name="NAME" x="204.724" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="204.724" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="241.3" y="236.22" smashed="yes">
<attribute name="NAME" x="242.824" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="242.824" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="203.2" y="167.64" smashed="yes">
<attribute name="VALUE" x="200.66" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="218.44" y="233.68" smashed="yes">
<attribute name="NAME" x="219.964" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="219.964" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="256.54" y="236.22" smashed="yes">
<attribute name="NAME" x="258.064" y="239.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="258.064" y="234.061" size="1.778" layer="96"/>
</instance>
<instance part="+3V13" gate="G$1" x="228.6" y="238.76" smashed="yes" rot="R270">
<attribute name="VALUE" x="228.6" y="236.22" size="1.778" layer="96"/>
</instance>
<instance part="P+12" gate="1" x="266.7" y="241.3" smashed="yes" rot="R270">
<attribute name="VALUE" x="269.24" y="238.76" size="1.778" layer="96"/>
</instance>
<instance part="GND28" gate="1" x="203.2" y="220.98" smashed="yes">
<attribute name="VALUE" x="200.66" y="218.44" size="1.778" layer="96"/>
</instance>
<instance part="GND29" gate="1" x="241.3" y="223.52" smashed="yes">
<attribute name="VALUE" x="238.76" y="220.98" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="139.7" y="193.04" smashed="yes">
<attribute name="NAME" x="135.89" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="135.89" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="GND30" gate="1" x="129.54" y="193.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="127" y="195.58" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R8" gate="G$1" x="149.86" y="198.12" smashed="yes">
<attribute name="NAME" x="146.05" y="199.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="146.05" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="GND31" gate="1" x="124.46" y="198.12" smashed="yes" rot="R270">
<attribute name="VALUE" x="121.92" y="200.66" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND32" gate="1" x="129.54" y="228.6" smashed="yes" rot="R270">
<attribute name="VALUE" x="127" y="231.14" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V14" gate="G$1" x="226.06" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="220.98" y="147.32" size="1.778" layer="96"/>
</instance>
<instance part="C19" gate="G$1" x="215.9" y="139.7" smashed="yes">
<attribute name="NAME" x="217.424" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="217.424" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="GND33" gate="1" x="215.9" y="132.08" smashed="yes">
<attribute name="VALUE" x="213.36" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="U6" gate="G$1" x="419.1" y="134.62" smashed="yes">
<attribute name="NAME" x="414.02" y="144.78" size="1.778" layer="95"/>
<attribute name="VALUE" x="414.02" y="121.92" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="439.42" y="134.62" smashed="yes" rot="R90">
<attribute name="VALUE" x="441.96" y="132.08" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R18" gate="G$1" x="398.78" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="397.2814" y="120.65" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="402.082" y="120.65" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="398.78" y="111.76" smashed="yes">
<attribute name="VALUE" x="396.24" y="109.22" size="1.778" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="396.24" y="139.7" smashed="yes" rot="R90">
<attribute name="VALUE" x="401.32" y="137.16" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C26" gate="G$1" x="327.66" y="33.02" smashed="yes">
<attribute name="NAME" x="329.184" y="35.941" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="30.861" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="28.956" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="27.178" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="25.4" size="1.27" layer="97"/>
</instance>
<instance part="C22" gate="G$1" x="327.66" y="76.2" smashed="yes">
<attribute name="NAME" x="329.184" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="329.184" y="74.041" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="329.184" y="72.136" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="329.184" y="70.358" size="1.27" layer="97"/>
<attribute name="TYPE" x="329.184" y="68.58" size="1.27" layer="97"/>
</instance>
<instance part="C25" gate="G$1" x="342.9" y="76.2" smashed="yes">
<attribute name="NAME" x="344.424" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.424" y="74.041" size="1.778" layer="96"/>
</instance>
<instance part="C24" gate="G$1" x="342.9" y="33.02" smashed="yes">
<attribute name="NAME" x="344.424" y="35.941" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.424" y="30.861" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="99.06" y="147.32" smashed="yes">
<attribute name="NAME" x="100.584" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.584" y="145.161" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="142.24" y="228.6" smashed="yes">
<attribute name="NAME" x="138.43" y="230.0986" size="1.778" layer="95"/>
<attribute name="VALUE" x="138.43" y="225.298" size="1.778" layer="96"/>
</instance>
<instance part="C100" gate="G$1" x="264.16" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="269.24" y="104.14" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="261.62" y="104.14" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J3" gate="J$1" x="289.56" y="12.7" smashed="yes" rot="R180">
<attribute name="VALUE" x="292.1" y="20.066" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="292.1" y="7.112" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND3" gate="1" x="246.38" y="15.24" smashed="yes" rot="R270">
<attribute name="VALUE" x="243.84" y="17.78" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V2" gate="G$1" x="251.46" y="12.7" smashed="yes" rot="R90">
<attribute name="VALUE" x="256.54" y="10.16" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="DAC-0" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="264.16" y1="81.28" x2="251.46" y2="81.28" width="0.1524" layer="91"/>
<label x="251.46" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA02/ADC0-1/DAC-0"/>
<wire x1="198.12" y1="147.32" x2="210.82" y2="147.32" width="0.1524" layer="91"/>
<label x="203.2" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="DAC-1" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="264.16" y1="38.1" x2="251.46" y2="38.1" width="0.1524" layer="91"/>
<label x="251.46" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1"/>
<wire x1="198.12" y1="139.7" x2="210.82" y2="139.7" width="0.1524" layer="91"/>
<label x="203.2" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="U7" gate="G$1" pin="VREF"/>
<wire x1="274.32" y1="38.1" x2="276.86" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="VREF"/>
<wire x1="274.32" y1="81.28" x2="276.86" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="248.92" y1="53.34" x2="276.86" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="248.92" y1="96.52" x2="276.86" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="368.3" y1="48.26" x2="373.38" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="373.38" y1="48.26" x2="373.38" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="368.3" y1="91.44" x2="373.38" y2="91.44" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="373.38" y1="91.44" x2="373.38" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J102" gate="G$1" pin="6"/>
<wire x1="347.98" y1="185.42" x2="355.6" y2="185.42" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J102" gate="G$1" pin="5"/>
<wire x1="332.74" y1="185.42" x2="325.12" y2="185.42" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="261.62" y1="124.46" x2="259.08" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="C"/>
<wire x1="259.08" y1="124.46" x2="248.92" y2="124.46" width="0.1524" layer="91"/>
<wire x1="261.62" y1="116.84" x2="259.08" y2="116.84" width="0.1524" layer="91"/>
<wire x1="259.08" y1="116.84" x2="259.08" y2="124.46" width="0.1524" layer="91"/>
<junction x="259.08" y="124.46"/>
<wire x1="259.08" y1="116.84" x2="259.08" y2="106.68" width="0.1524" layer="91"/>
<junction x="259.08" y="116.84"/>
<pinref part="C100" gate="G$1" pin="C"/>
<wire x1="259.08" y1="106.68" x2="261.62" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="307.34" y1="152.4" x2="317.5" y2="152.4" width="0.1524" layer="91"/>
<wire x1="317.5" y1="152.4" x2="332.74" y2="152.4" width="0.1524" layer="91"/>
<junction x="317.5" y="152.4"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="375.92" y1="114.3" x2="375.92" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="312.42" y1="25.4" x2="312.42" y2="27.94" width="0.1524" layer="91"/>
<wire x1="312.42" y1="27.94" x2="312.42" y2="30.48" width="0.1524" layer="91"/>
<wire x1="312.42" y1="27.94" x2="327.66" y2="27.94" width="0.1524" layer="91"/>
<junction x="312.42" y="27.94"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="327.66" y1="27.94" x2="327.66" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="342.9" y1="30.48" x2="342.9" y2="27.94" width="0.1524" layer="91"/>
<wire x1="342.9" y1="27.94" x2="327.66" y2="27.94" width="0.1524" layer="91"/>
<junction x="327.66" y="27.94"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="312.42" y1="73.66" x2="312.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="312.42" y1="71.12" x2="312.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="312.42" y1="71.12" x2="327.66" y2="71.12" width="0.1524" layer="91"/>
<junction x="312.42" y="71.12"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="327.66" y1="71.12" x2="327.66" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="342.9" y1="73.66" x2="342.9" y2="71.12" width="0.1524" layer="91"/>
<wire x1="342.9" y1="71.12" x2="327.66" y2="71.12" width="0.1524" layer="91"/>
<junction x="327.66" y="71.12"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="25.4" y1="27.94" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<pinref part="J103" gate="J1" pin="GNDDTCT"/>
<wire x1="25.4" y1="33.02" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
<pinref part="J103" gate="J1" pin="GND@5"/>
<wire x1="35.56" y1="38.1" x2="25.4" y2="38.1" width="0.1524" layer="91"/>
<wire x1="25.4" y1="38.1" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<junction x="25.4" y="33.02"/>
<pinref part="J103" gate="J1" pin="GND@3"/>
<wire x1="35.56" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="40.64" x2="25.4" y2="38.1" width="0.1524" layer="91"/>
<junction x="25.4" y="38.1"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="78.74" x2="45.72" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="119.38" y1="20.32" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="P1"/>
<wire x1="109.22" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<junction x="109.22" y="20.32"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="109.22" y1="12.7" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="93.98" y1="96.52" x2="93.98" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="109.22" y1="99.06" x2="93.98" y2="99.06" width="0.1524" layer="91"/>
<junction x="93.98" y="99.06"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="88.9" y1="144.78" x2="78.74" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="78.74" y1="144.78" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
<junction x="78.74" y="144.78"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="68.58" y1="144.78" x2="58.42" y2="144.78" width="0.1524" layer="91"/>
<junction x="68.58" y="144.78"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="58.42" y1="144.78" x2="58.42" y2="137.16" width="0.1524" layer="91"/>
<junction x="58.42" y="144.78"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="99.06" y1="144.78" x2="88.9" y2="144.78" width="0.1524" layer="91"/>
<junction x="88.9" y="144.78"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="12.7" y1="114.3" x2="12.7" y2="106.68" width="0.1524" layer="91"/>
<wire x1="12.7" y1="106.68" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="35.56" y1="106.68" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="53.34" y1="114.3" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<wire x1="53.34" y1="106.68" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<junction x="35.56" y="106.68"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="68.58" y1="114.3" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="106.68" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<junction x="53.34" y="106.68"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="35.56" y1="104.14" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="81.28" y1="213.36" x2="86.36" y2="213.36" width="0.1524" layer="91"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="86.36" y1="213.36" x2="86.36" y2="210.82" width="0.1524" layer="91"/>
<wire x1="86.36" y1="210.82" x2="86.36" y2="208.28" width="0.1524" layer="91"/>
<junction x="86.36" y="210.82"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="86.36" y1="208.28" x2="81.28" y2="208.28" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="GND"/>
<wire x1="68.58" y1="210.82" x2="86.36" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="78.74" y1="233.68" x2="86.36" y2="233.68" width="0.1524" layer="91"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="86.36" y1="233.68" x2="86.36" y2="236.22" width="0.1524" layer="91"/>
<wire x1="86.36" y1="236.22" x2="86.36" y2="238.76" width="0.1524" layer="91"/>
<junction x="86.36" y="236.22"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="86.36" y1="238.76" x2="78.74" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="33.02" y1="182.88" x2="2.54" y2="182.88" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="EPAD"/>
<wire x1="203.2" y1="177.8" x2="198.12" y2="177.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="GND"/>
<wire x1="198.12" y1="175.26" x2="203.2" y2="175.26" width="0.1524" layer="91"/>
<wire x1="203.2" y1="175.26" x2="203.2" y2="177.8" width="0.1524" layer="91"/>
<wire x1="203.2" y1="175.26" x2="203.2" y2="170.18" width="0.1524" layer="91"/>
<junction x="203.2" y="175.26"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="203.2" y1="231.14" x2="218.44" y2="231.14" width="0.1524" layer="91"/>
<wire x1="203.2" y1="231.14" x2="203.2" y2="223.52" width="0.1524" layer="91"/>
<junction x="203.2" y="231.14"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="241.3" y1="233.68" x2="256.54" y2="233.68" width="0.1524" layer="91"/>
<wire x1="241.3" y1="233.68" x2="241.3" y2="226.06" width="0.1524" layer="91"/>
<junction x="241.3" y="233.68"/>
<pinref part="GND29" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="132.08" y1="193.04" x2="134.62" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="127" y1="198.12" x2="144.78" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="215.9" y1="134.62" x2="215.9" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="U6" gate="G$1" pin="SENSE"/>
<wire x1="436.88" y1="134.62" x2="431.8" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="398.78" y1="114.3" x2="398.78" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="137.16" y1="228.6" x2="132.08" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="J3" gate="J$1" pin="1"/>
<wire x1="248.92" y1="15.24" x2="281.94" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="VDD"/>
<wire x1="307.34" y1="147.32" x2="309.88" y2="147.32" width="0.1524" layer="91"/>
<wire x1="309.88" y1="147.32" x2="309.88" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="309.88" y1="144.78" x2="317.5" y2="144.78" width="0.1524" layer="91"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="317.5" y1="144.78" x2="332.74" y2="144.78" width="0.1524" layer="91"/>
<junction x="317.5" y="144.78"/>
<pinref part="U4" gate="G$1" pin="VDD3V"/>
<wire x1="307.34" y1="149.86" x2="309.88" y2="149.86" width="0.1524" layer="91"/>
<wire x1="309.88" y1="149.86" x2="309.88" y2="147.32" width="0.1524" layer="91"/>
<junction x="309.88" y="147.32"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="48.26" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<pinref part="J103" gate="J1" pin="VCC"/>
<wire x1="35.56" y1="43.18" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="86.36" y1="60.96" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="109.22" y1="60.96" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="VDDIO"/>
<wire x1="88.9" y1="152.4" x2="99.06" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="99.06" y1="152.4" x2="114.3" y2="152.4" width="0.1524" layer="91"/>
<wire x1="114.3" y1="152.4" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<wire x1="88.9" y1="152.4" x2="78.74" y2="152.4" width="0.1524" layer="91"/>
<junction x="88.9" y="152.4"/>
<pinref part="U3" gate="G$1" pin="VDDANA"/>
<wire x1="119.38" y1="137.16" x2="114.3" y2="137.16" width="0.1524" layer="91"/>
<wire x1="114.3" y1="137.16" x2="114.3" y2="152.4" width="0.1524" layer="91"/>
<junction x="114.3" y="152.4"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="78.74" y1="152.4" x2="68.58" y2="152.4" width="0.1524" layer="91"/>
<junction x="78.74" y="152.4"/>
<wire x1="68.58" y1="152.4" x2="58.42" y2="152.4" width="0.1524" layer="91"/>
<junction x="68.58" y="152.4"/>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="58.42" y1="152.4" x2="58.42" y2="157.48" width="0.1524" layer="91"/>
<junction x="58.42" y="152.4"/>
<pinref part="C7" gate="G$1" pin="1"/>
<junction x="99.06" y="152.4"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VOUT"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="48.26" y1="121.92" x2="53.34" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="68.58" y1="121.92" x2="53.34" y2="121.92" width="0.1524" layer="91"/>
<junction x="53.34" y="121.92"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="78.74" y1="121.92" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
<junction x="68.58" y="121.92"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="48.26" y1="154.94" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="50.8" y1="154.94" x2="50.8" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="78.74" y1="182.88" x2="73.66" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="78.74" y1="187.96" x2="60.96" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="78.74" y1="193.04" x2="48.26" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VL"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="198.12" y1="238.76" x2="203.2" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="203.2" y1="238.76" x2="218.44" y2="238.76" width="0.1524" layer="91"/>
<junction x="203.2" y="238.76"/>
<wire x1="218.44" y1="238.76" x2="226.06" y2="238.76" width="0.1524" layer="91"/>
<junction x="218.44" y="238.76"/>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1"/>
<wire x1="198.12" y1="144.78" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="215.9" y1="144.78" x2="223.52" y2="144.78" width="0.1524" layer="91"/>
<junction x="215.9" y="144.78"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<pinref part="U6" gate="G$1" pin="TERM1"/>
<wire x1="398.78" y1="139.7" x2="406.4" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="J$1" pin="2"/>
<wire x1="254" y1="12.7" x2="281.94" y2="12.7" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="OB1" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="OUT1"/>
<wire x1="307.34" y1="43.18" x2="322.58" y2="43.18" width="0.1524" layer="91"/>
<label x="312.42" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J100" gate="J1" pin="2"/>
<wire x1="416.56" y1="96.52" x2="406.4" y2="96.52" width="0.1524" layer="91"/>
<label x="408.94" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="OB2" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="OUT2"/>
<wire x1="307.34" y1="53.34" x2="322.58" y2="53.34" width="0.1524" layer="91"/>
<label x="312.42" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J100" gate="J1" pin="1"/>
<wire x1="416.56" y1="99.06" x2="406.4" y2="99.06" width="0.1524" layer="91"/>
<label x="408.94" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="OA2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT2"/>
<wire x1="307.34" y1="96.52" x2="322.58" y2="96.52" width="0.1524" layer="91"/>
<label x="312.42" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J100" gate="J1" pin="3"/>
<wire x1="416.56" y1="93.98" x2="406.4" y2="93.98" width="0.1524" layer="91"/>
<label x="408.94" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="OA1" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="OUT1"/>
<wire x1="307.34" y1="86.36" x2="322.58" y2="86.36" width="0.1524" layer="91"/>
<label x="312.42" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J100" gate="J1" pin="4"/>
<wire x1="416.56" y1="91.44" x2="406.4" y2="91.44" width="0.1524" layer="91"/>
<label x="408.94" y="91.44" size="1.778" layer="95"/>
<label x="408.94" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="LSSA" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="LSS"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="307.34" y1="91.44" x2="358.14" y2="91.44" width="0.1524" layer="91"/>
<label x="312.42" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="LSSB" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="U7" gate="G$1" pin="LSS"/>
<wire x1="358.14" y1="48.26" x2="307.34" y2="48.26" width="0.1524" layer="91"/>
<label x="312.42" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="VBB"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="307.34" y1="38.1" x2="312.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="312.42" y1="38.1" x2="327.66" y2="38.1" width="0.1524" layer="91"/>
<junction x="312.42" y="38.1"/>
<pinref part="P+2" gate="1" pin="+24V"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="327.66" y1="38.1" x2="342.9" y2="38.1" width="0.1524" layer="91"/>
<junction x="327.66" y="38.1"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="342.9" y1="38.1" x2="355.6" y2="38.1" width="0.1524" layer="91"/>
<junction x="342.9" y="38.1"/>
</segment>
<segment>
<pinref part="J102" gate="G$1" pin="1"/>
<wire x1="332.74" y1="190.5" x2="325.12" y2="190.5" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="J102" gate="G$1" pin="10"/>
<wire x1="347.98" y1="180.34" x2="355.6" y2="180.34" width="0.1524" layer="91"/>
<pinref part="P+6" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="P+8" gate="1" pin="+24V"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="279.4" y1="124.46" x2="271.78" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="A"/>
<wire x1="271.78" y1="124.46" x2="266.7" y2="124.46" width="0.1524" layer="91"/>
<wire x1="269.24" y1="116.84" x2="271.78" y2="116.84" width="0.1524" layer="91"/>
<wire x1="271.78" y1="116.84" x2="271.78" y2="124.46" width="0.1524" layer="91"/>
<junction x="271.78" y="124.46"/>
<pinref part="C100" gate="G$1" pin="A"/>
<wire x1="269.24" y1="106.68" x2="271.78" y2="106.68" width="0.1524" layer="91"/>
<wire x1="271.78" y1="106.68" x2="271.78" y2="116.84" width="0.1524" layer="91"/>
<junction x="271.78" y="116.84"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="P+10" gate="1" pin="+24V"/>
<wire x1="375.92" y1="142.24" x2="375.92" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="VBB"/>
<wire x1="307.34" y1="81.28" x2="312.42" y2="81.28" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+24V"/>
<wire x1="312.42" y1="81.28" x2="327.66" y2="81.28" width="0.1524" layer="91"/>
<junction x="312.42" y="81.28"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="327.66" y1="81.28" x2="342.9" y2="81.28" width="0.1524" layer="91"/>
<junction x="327.66" y="81.28"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="342.9" y1="81.28" x2="355.6" y2="81.28" width="0.1524" layer="91"/>
<junction x="342.9" y="81.28"/>
</segment>
</net>
<net name="AIN1" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="264.16" y1="86.36" x2="251.46" y2="86.36" width="0.1524" layer="91"/>
<label x="251.46" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB06/ADC1-8"/>
<wire x1="198.12" y1="58.42" x2="213.36" y2="58.42" width="0.1524" layer="91"/>
<label x="203.2" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN2" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="264.16" y1="91.44" x2="251.46" y2="91.44" width="0.1524" layer="91"/>
<label x="251.46" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0"/>
<wire x1="198.12" y1="142.24" x2="210.82" y2="142.24" width="0.1524" layer="91"/>
<label x="203.2" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="IN1"/>
<wire x1="274.32" y1="86.36" x2="276.86" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="IN2"/>
<wire x1="274.32" y1="91.44" x2="276.86" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="U7" gate="G$1" pin="IN2"/>
<wire x1="274.32" y1="48.26" x2="276.86" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="U7" gate="G$1" pin="IN1"/>
<wire x1="274.32" y1="43.18" x2="276.86" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BIN1" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="264.16" y1="43.18" x2="251.46" y2="43.18" width="0.1524" layer="91"/>
<label x="251.46" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA07/ADC0-7/SER0-3/TC1-1"/>
<wire x1="198.12" y1="134.62" x2="210.82" y2="134.62" width="0.1524" layer="91"/>
<label x="203.2" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="BIN2" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="264.16" y1="48.26" x2="251.46" y2="48.26" width="0.1524" layer="91"/>
<label x="251.46" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0"/>
<wire x1="198.12" y1="137.16" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
<label x="203.2" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="J102" gate="G$1" pin="2"/>
<wire x1="347.98" y1="190.5" x2="355.6" y2="190.5" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="J102" gate="G$1" pin="9"/>
<wire x1="332.74" y1="180.34" x2="325.12" y2="180.34" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<pinref part="P+9" gate="1" pin="+5V"/>
<wire x1="45.72" y1="76.2" x2="68.58" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+11" gate="1" pin="+5V"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="7.62" y1="121.92" x2="12.7" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="12.7" y1="121.92" x2="20.32" y2="121.92" width="0.1524" layer="91"/>
<junction x="12.7" y="121.92"/>
<pinref part="U1" gate="G$1" pin="EN"/>
<wire x1="20.32" y1="121.92" x2="22.86" y2="121.92" width="0.1524" layer="91"/>
<wire x1="22.86" y1="116.84" x2="20.32" y2="116.84" width="0.1524" layer="91"/>
<wire x1="20.32" y1="116.84" x2="20.32" y2="121.92" width="0.1524" layer="91"/>
<junction x="20.32" y="121.92"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VCC"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="198.12" y1="241.3" x2="241.3" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="241.3" y1="241.3" x2="256.54" y2="241.3" width="0.1524" layer="91"/>
<junction x="241.3" y="241.3"/>
<wire x1="256.54" y1="241.3" x2="264.16" y2="241.3" width="0.1524" layer="91"/>
<junction x="256.54" y="241.3"/>
<pinref part="P+12" gate="1" pin="+5V"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="J102" gate="G$1" pin="4"/>
<wire x1="347.98" y1="187.96" x2="355.6" y2="187.96" width="0.1524" layer="91"/>
<label x="350.52" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="A"/>
<wire x1="162.56" y1="218.44" x2="147.32" y2="218.44" width="0.1524" layer="91"/>
<label x="149.86" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="J102" gate="G$1" pin="3"/>
<wire x1="332.74" y1="187.96" x2="325.12" y2="187.96" width="0.1524" layer="91"/>
<label x="327.66" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="B"/>
<wire x1="162.56" y1="213.36" x2="147.32" y2="213.36" width="0.1524" layer="91"/>
<label x="149.86" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="Z" class="0">
<segment>
<pinref part="J102" gate="G$1" pin="8"/>
<wire x1="347.98" y1="182.88" x2="355.6" y2="182.88" width="0.1524" layer="91"/>
<label x="350.52" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="Z"/>
<wire x1="162.56" y1="182.88" x2="147.32" y2="182.88" width="0.1524" layer="91"/>
<label x="149.86" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="Y" class="0">
<segment>
<pinref part="J102" gate="G$1" pin="7"/>
<wire x1="332.74" y1="182.88" x2="325.12" y2="182.88" width="0.1524" layer="91"/>
<label x="327.66" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="Y"/>
<wire x1="162.56" y1="187.96" x2="147.32" y2="187.96" width="0.1524" layer="91"/>
<label x="149.86" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSENSE" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="358.14" y1="129.54" x2="342.9" y2="129.54" width="0.1524" layer="91"/>
<label x="342.9" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB07/ADC1-9"/>
<wire x1="198.12" y1="55.88" x2="213.36" y2="55.88" width="0.1524" layer="91"/>
<label x="203.2" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="375.92" y1="132.08" x2="375.92" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="375.92" y1="129.54" x2="375.92" y2="127" width="0.1524" layer="91"/>
<wire x1="368.3" y1="129.54" x2="375.92" y2="129.54" width="0.1524" layer="91"/>
<junction x="375.92" y="129.54"/>
</segment>
</net>
<net name="4-1-CLK" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="CLK"/>
<wire x1="276.86" y1="152.4" x2="261.62" y2="152.4" width="0.1524" layer="91"/>
<label x="264.16" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB13/SER4-1/TC4-1"/>
<wire x1="198.12" y1="40.64" x2="213.36" y2="40.64" width="0.1524" layer="91"/>
<label x="203.2" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="4-0-MOSI" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="MOSI"/>
<wire x1="276.86" y1="147.32" x2="261.62" y2="147.32" width="0.1524" layer="91"/>
<label x="264.16" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB12/SER4-0/TC4-0"/>
<wire x1="198.12" y1="43.18" x2="213.36" y2="43.18" width="0.1524" layer="91"/>
<label x="203.2" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="4-2-CS" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="CSN"/>
<wire x1="276.86" y1="154.94" x2="261.62" y2="154.94" width="0.1524" layer="91"/>
<label x="264.16" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB14/SER4-2/TC5-0"/>
<wire x1="198.12" y1="38.1" x2="213.36" y2="38.1" width="0.1524" layer="91"/>
<label x="203.2" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="4-3-MISO" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="MISO"/>
<wire x1="276.86" y1="149.86" x2="261.62" y2="149.86" width="0.1524" layer="91"/>
<label x="264.16" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB15/SER4-3/TC5-1"/>
<wire x1="198.12" y1="35.56" x2="213.36" y2="35.56" width="0.1524" layer="91"/>
<label x="203.2" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="J103" gate="J1" pin="SWDIO/TMS"/>
<wire x1="66.04" y1="43.18" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
<label x="68.58" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO"/>
<wire x1="198.12" y1="81.28" x2="213.36" y2="81.28" width="0.1524" layer="91"/>
<label x="200.66" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDCLK" class="0">
<segment>
<pinref part="J103" gate="J1" pin="SWDCLK/TCK"/>
<wire x1="66.04" y1="40.64" x2="86.36" y2="40.64" width="0.1524" layer="91"/>
<label x="68.58" y="40.64" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="86.36" y1="40.64" x2="86.36" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK"/>
<wire x1="198.12" y1="83.82" x2="213.36" y2="83.82" width="0.1524" layer="91"/>
<label x="200.66" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="S1"/>
<pinref part="U3" gate="G$1" pin="RESETN"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="109.22" y1="27.94" x2="109.22" y2="33.02" width="0.1524" layer="91"/>
<wire x1="109.22" y1="33.02" x2="119.38" y2="33.02" width="0.1524" layer="91"/>
<junction x="109.22" y="33.02"/>
<wire x1="109.22" y1="33.02" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<wire x1="104.14" y1="33.02" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<pinref part="J103" gate="J1" pin="!RESET"/>
<wire x1="104.14" y1="33.02" x2="66.04" y2="33.02" width="0.1524" layer="91"/>
<junction x="104.14" y="33.02"/>
<label x="68.58" y="33.02" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="109.22" y1="45.72" x2="109.22" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USBDM" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="45.72" y1="73.66" x2="63.5" y2="73.66" width="0.1524" layer="91"/>
<label x="53.34" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM"/>
<wire x1="198.12" y1="91.44" x2="213.36" y2="91.44" width="0.1524" layer="91"/>
<label x="200.66" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="USBDP" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="45.72" y1="71.12" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<label x="53.34" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP"/>
<wire x1="198.12" y1="88.9" x2="213.36" y2="88.9" width="0.1524" layer="91"/>
<label x="200.66" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSW" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="VSW"/>
<wire x1="111.76" y1="121.92" x2="119.38" y2="121.92" width="0.1524" layer="91"/>
<label x="111.76" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDDCORE" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="93.98" y1="106.68" x2="109.22" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDDCORE"/>
<wire x1="109.22" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<junction x="109.22" y="106.68"/>
<wire x1="93.98" y1="106.68" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<junction x="93.98" y="106.68"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="93.98" y1="121.92" x2="96.52" y2="121.92" width="0.1524" layer="91"/>
<label x="99.06" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="ERRLIGHT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0"/>
<wire x1="198.12" y1="53.34" x2="213.36" y2="53.34" width="0.1524" layer="91"/>
<label x="200.66" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="25.4" y1="193.04" x2="2.54" y2="193.04" width="0.1524" layer="91"/>
<label x="2.54" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLKLIGHT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PA27/GCLK-1"/>
<wire x1="198.12" y1="86.36" x2="213.36" y2="86.36" width="0.1524" layer="91"/>
<label x="200.66" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="15.24" y1="187.96" x2="2.54" y2="187.96" width="0.1524" layer="91"/>
<label x="2.54" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0"/>
<wire x1="198.12" y1="73.66" x2="213.36" y2="73.66" width="0.1524" layer="91"/>
<label x="200.66" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="6"/>
<wire x1="7.62" y1="142.24" x2="-2.54" y2="142.24" width="0.1524" layer="91"/>
<label x="-2.54" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB01/ADC0-13/SER5-3/TC7-1"/>
<wire x1="198.12" y1="71.12" x2="213.36" y2="71.12" width="0.1524" layer="91"/>
<label x="200.66" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="5"/>
<wire x1="7.62" y1="144.78" x2="-2.54" y2="144.78" width="0.1524" layer="91"/>
<label x="-2.54" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0"/>
<wire x1="198.12" y1="68.58" x2="213.36" y2="68.58" width="0.1524" layer="91"/>
<label x="200.66" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="4"/>
<wire x1="7.62" y1="147.32" x2="-2.54" y2="147.32" width="0.1524" layer="91"/>
<label x="-2.54" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB03/ADC0/SER5-1/TC6"/>
<wire x1="198.12" y1="66.04" x2="213.36" y2="66.04" width="0.1524" layer="91"/>
<label x="200.66" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="3"/>
<wire x1="7.62" y1="149.86" x2="-2.54" y2="149.86" width="0.1524" layer="91"/>
<label x="-2.54" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB04/ADC1-6"/>
<wire x1="198.12" y1="63.5" x2="213.36" y2="63.5" width="0.1524" layer="91"/>
<label x="200.66" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="2"/>
<wire x1="7.62" y1="152.4" x2="-2.54" y2="152.4" width="0.1524" layer="91"/>
<label x="-2.54" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB05/ADC1-7"/>
<wire x1="198.12" y1="60.96" x2="213.36" y2="60.96" width="0.1524" layer="91"/>
<label x="200.66" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="1"/>
<wire x1="7.62" y1="154.94" x2="-2.54" y2="154.94" width="0.1524" layer="91"/>
<label x="-2.54" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP8" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB30/SER7-0/SER5-1/TC0-0/SWO"/>
<wire x1="198.12" y1="22.86" x2="213.36" y2="22.86" width="0.1524" layer="91"/>
<label x="200.66" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="8"/>
<wire x1="7.62" y1="137.16" x2="-2.54" y2="137.16" width="0.1524" layer="91"/>
<label x="-2.54" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIP7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB31/SER7-1/SER5-0/TC0-1"/>
<wire x1="198.12" y1="20.32" x2="213.36" y2="20.32" width="0.1524" layer="91"/>
<label x="200.66" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J101" gate="G$1" pin="7"/>
<wire x1="7.62" y1="139.7" x2="-2.54" y2="139.7" width="0.1524" layer="91"/>
<label x="-2.54" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="J101" gate="G$1" pin="16"/>
<wire x1="38.1" y1="154.94" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<wire x1="35.56" y1="154.94" x2="33.02" y2="154.94" width="0.1524" layer="91"/>
<wire x1="35.56" y1="154.94" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
<junction x="35.56" y="154.94"/>
<pinref part="J101" gate="G$1" pin="9"/>
<wire x1="35.56" y1="152.4" x2="35.56" y2="149.86" width="0.1524" layer="91"/>
<wire x1="35.56" y1="149.86" x2="35.56" y2="147.32" width="0.1524" layer="91"/>
<wire x1="35.56" y1="147.32" x2="35.56" y2="144.78" width="0.1524" layer="91"/>
<wire x1="35.56" y1="144.78" x2="35.56" y2="142.24" width="0.1524" layer="91"/>
<wire x1="35.56" y1="142.24" x2="35.56" y2="139.7" width="0.1524" layer="91"/>
<wire x1="35.56" y1="139.7" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
<wire x1="35.56" y1="137.16" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<pinref part="J101" gate="G$1" pin="10"/>
<wire x1="33.02" y1="139.7" x2="35.56" y2="139.7" width="0.1524" layer="91"/>
<junction x="35.56" y="139.7"/>
<pinref part="J101" gate="G$1" pin="11"/>
<wire x1="33.02" y1="142.24" x2="35.56" y2="142.24" width="0.1524" layer="91"/>
<junction x="35.56" y="142.24"/>
<pinref part="J101" gate="G$1" pin="12"/>
<wire x1="33.02" y1="144.78" x2="35.56" y2="144.78" width="0.1524" layer="91"/>
<junction x="35.56" y="144.78"/>
<pinref part="J101" gate="G$1" pin="13"/>
<wire x1="33.02" y1="147.32" x2="35.56" y2="147.32" width="0.1524" layer="91"/>
<junction x="35.56" y="147.32"/>
<pinref part="J101" gate="G$1" pin="14"/>
<wire x1="33.02" y1="149.86" x2="35.56" y2="149.86" width="0.1524" layer="91"/>
<junction x="35.56" y="149.86"/>
<pinref part="J101" gate="G$1" pin="15"/>
<wire x1="33.02" y1="152.4" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
<junction x="35.56" y="152.4"/>
</segment>
</net>
<net name="XIN0" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="1"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="73.66" y1="208.28" x2="60.96" y2="208.28" width="0.1524" layer="91"/>
<wire x1="60.96" y1="208.28" x2="48.26" y2="208.28" width="0.1524" layer="91"/>
<junction x="60.96" y="208.28"/>
<label x="48.26" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0"/>
<wire x1="198.12" y1="116.84" x2="210.82" y2="116.84" width="0.1524" layer="91"/>
<label x="200.66" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="XOUT0" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="60.96" y1="213.36" x2="73.66" y2="213.36" width="0.1524" layer="91"/>
<wire x1="60.96" y1="213.36" x2="48.26" y2="213.36" width="0.1524" layer="91"/>
<junction x="60.96" y="213.36"/>
<label x="48.26" y="213.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1"/>
<wire x1="198.12" y1="114.3" x2="210.82" y2="114.3" width="0.1524" layer="91"/>
<label x="200.66" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="XIN32" class="0">
<segment>
<pinref part="Y2" gate="G$1" pin="1"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="71.12" y1="238.76" x2="60.96" y2="238.76" width="0.1524" layer="91"/>
<wire x1="60.96" y1="238.76" x2="48.26" y2="238.76" width="0.1524" layer="91"/>
<junction x="60.96" y="238.76"/>
<label x="48.26" y="238.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0"/>
<wire x1="198.12" y1="152.4" x2="210.82" y2="152.4" width="0.1524" layer="91"/>
<label x="200.66" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="XOUT32" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="Y2" gate="G$1" pin="2"/>
<wire x1="60.96" y1="233.68" x2="71.12" y2="233.68" width="0.1524" layer="91"/>
<wire x1="60.96" y1="233.68" x2="48.26" y2="233.68" width="0.1524" layer="91"/>
<junction x="60.96" y="233.68"/>
<label x="48.26" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1"/>
<wire x1="198.12" y1="149.86" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
<label x="200.66" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="40.64" y1="182.88" x2="63.5" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="50.8" y1="187.96" x2="22.86" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="33.02" y1="193.04" x2="38.1" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1-2-PA18" class="0">
<segment>
<pinref part="U2" gate="A" pin="RO"/>
<wire x1="162.56" y1="203.2" x2="147.32" y2="203.2" width="0.1524" layer="91"/>
<label x="149.86" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0"/>
<wire x1="198.12" y1="106.68" x2="210.82" y2="106.68" width="0.1524" layer="91"/>
<label x="200.66" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="1-0-PA16" class="0">
<segment>
<pinref part="U2" gate="A" pin="DI"/>
<wire x1="162.56" y1="233.68" x2="147.32" y2="233.68" width="0.1524" layer="91"/>
<label x="149.86" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0/TCC0-4"/>
<wire x1="198.12" y1="111.76" x2="210.82" y2="111.76" width="0.1524" layer="91"/>
<label x="200.66" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="DE" class="0">
<segment>
<pinref part="U2" gate="A" pin="DE"/>
<wire x1="162.56" y1="208.28" x2="147.32" y2="208.28" width="0.1524" layer="91"/>
<label x="149.86" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA19/SER1-3/SER3-3/TC3-1"/>
<wire x1="198.12" y1="104.14" x2="210.82" y2="104.14" width="0.1524" layer="91"/>
<label x="200.66" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="TERM" class="0">
<segment>
<pinref part="U2" gate="A" pin="!TERM"/>
<wire x1="162.56" y1="223.52" x2="147.32" y2="223.52" width="0.1524" layer="91"/>
<label x="149.86" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA17/SER1-1/SER3-0/TC2-1/TCC0-5"/>
<wire x1="198.12" y1="109.22" x2="210.82" y2="109.22" width="0.1524" layer="91"/>
<label x="200.66" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="U2" gate="A" pin="TERM100"/>
<wire x1="144.78" y1="193.04" x2="162.56" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U2" gate="A" pin="SRL"/>
<wire x1="154.94" y1="198.12" x2="162.56" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RTD" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<pinref part="U6" gate="G$1" pin="TERM2"/>
<wire x1="398.78" y1="129.54" x2="406.4" y2="129.54" width="0.1524" layer="91"/>
<wire x1="398.78" y1="129.54" x2="386.08" y2="129.54" width="0.1524" layer="91"/>
<junction x="398.78" y="129.54"/>
<label x="386.08" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1"/>
<wire x1="198.12" y1="50.8" x2="213.36" y2="50.8" width="0.1524" layer="91"/>
<label x="203.2" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="U2" gate="A" pin="!RE"/>
<wire x1="147.32" y1="228.6" x2="162.56" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STOP" class="0">
<segment>
<pinref part="J3" gate="J$1" pin="3"/>
<wire x1="281.94" y1="10.16" x2="261.62" y2="10.16" width="0.1524" layer="91"/>
<label x="264.16" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1"/>
<wire x1="198.12" y1="93.98" x2="210.82" y2="93.98" width="0.1524" layer="91"/>
<label x="200.66" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
