## Stepper Module

[DEV LOG](log/ucbus-stepper-log.md)

These are stepper drivers! They run:

- ATSAMD51 Microcontrollers  
- 2x A4950 H-Bridges on 2x DACs to VREF
- 1x AS5147 if you want to get closed-loop-fancy  
- RS485 input and 40W 24V into one 10-pin IDC  

To build one w/ a motor, for closed loop stepping, I do:

- N17 Motor 17HS19-2004S1
- IDC, Plug, 2x15 609-5106-ND 
- IDC, Plug, 2x5 609-1746-ND
- Magnet 469-1076-ND
- Spacer 6mm Tall 94669A101
- M3 SHCS 50mm 91292A026
- DIP Switch 219-8LPSTRF
- 2x5 Latching 732-2679-ND
- Two Part Epoxy

The hardware is set up to run on the 'UCBus', a synchronous embedded protocol that establishes a bus clock *and* data lines on one RS485/UART Phy. Details are not documented anywhere ATM, but it w o r k s. 

## Closed Loop

Circuits have a land for an AS5147P encoder, with which they can run closed-loop stepper control:

![cl](log/2021-02-25_cl-step-perf.mp4)

## Melted Version V02

I revisioned this recently to skip the module and just build the whole circuit on one board. Schematic is identical to V01, but now is much smaller (can fit on a NEMA14 size motor) and all components are SMT on one side, i.e. the AS5047P is on the same side as the H-Bridges, Micro etc. 

The IDC UCBus connector & DIP Switch are on the flipped surface. 

![schem](log/2020-08-25_schematic.png)
![routed](log/2020-08-25_routed.png)

140 of these are out fab / assembly.

## Module Version V01

Beneath [the module](https://gitlab.cba.mit.edu/jakeread/ucbus-module).

Networked step driver (or) general purpose 2-H-Bridge ute. Uses module for micro / network drivers, all other parts are 0805 or similar to hand-solder easily. 

![fab](log/2020-07-28_ucbus-stepper-fab.jpg)
![routed](log/2020-06-29_routed.png)
![schematic](log/2020-06-29_schematic.png)

## Voltages / VREFs etc 

The A4950 VREFs are hooked up to DAC0 and DAC1. 

Current limiting will be set by:

`Imax = Vref / (10 * Rs) ``

Where Rs is the sense resistor value, in this circuit its 0.2 ohms. So maximum current is 1.65A at 3.3v full DAC, all else scales. I think I set it up this way in case I was unable to set the DACs up properly, and had to turn them on full all the time. 
