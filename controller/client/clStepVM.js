/*
cclStepVM.js

vm for closed-loop stepper motors 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2021

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import { TS, PK, DK, AK, EP } from '../osapjs/core/ts.js'

export default function ClStepVM(osap, route) {

  // ------------------------------------------------------ 0: DIAGNOSTICS QUERY
  let diagnosticsQuery = osap.query(route, TS.endpoint(0, 0), 512)
  this.getMagDiagnostics = () => {
    return new Promise((resolve, reject) => {
      diagnosticsQuery.pull().then((data) => {
        let result = {
          magHi: TS.read('boolean', data, 0, true),
          magLo: TS.read('boolean', data, 1, true),
          cordicOverflow: TS.read('boolean', data, 2, true),
          compensationComplete: TS.read('boolean', data, 3, true),
          angularGainCorrection: TS.read('uint8', data, 4, true),
          cordicMagnitude: TS.read('uint16', data, 5, true)
        }
        resolve(result)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  // ------------------------------------------------------ 1: RUN CALIB

  let calibEP = osap.endpoint()
  calibEP.addRoute(route, TS.endpoint(0, 1), 512)
  calibEP.setTimeoutLength(10000)
  this.runCalib = () => {
    return new Promise((resolve, reject) => {
      calibEP.write(new Uint8Array(0)).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }

  // ------------------------------------------------------ 2: SET MODE SWITCH

  let modeEP = osap.endpoint()
  modeEP.addRoute(route, TS.endpoint(0, 2), 512)
  this.setMode = (mode) => {
    return new Promise((resolve, reject) => {
      let val = 0
      switch (mode) {
        case "noop":
          val = 0
          break;
        case "pid":
          val = 1
          break;
        default:
          reject("bad mode key");
          return
      }
      modeEP.write(Uint8Array.from([val])).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }

  // ------------------------------------------------------ 3: SET POSITION TARGET  

  let angleTargetEP = osap.endpoint()
  angleTargetEP.addRoute(route, TS.endpoint(0, 3), 512)
  this.setTarget = (targ) => {
    return new Promise((resolve, reject) => {
      let datagram = new Uint8Array(4)
      TS.write('float32', targ, datagram, 0, true)
      angleTargetEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }

  // ------------------------------------------------------ 4: POSITION QUERY

  let positionQuery = osap.query(route, TS.endpoint(0, 4), 512)
  this.getPosition = () => {
    return new Promise((resolve, reject) => {
      positionQuery.pull().then((data) => {
        let pos = TS.read('float32', data, 0, true)
        resolve(pos)
      }).catch((err) => { reject(err) })
    })
  }

  // ------------------------------------------------------ 5: LOOP SPEC

  let specQuery = osap.query(route, TS.endpoint(0, 5), 512)
  this.getSpec = () => {
    return new Promise((resolve, reject) => {
      specQuery.pull().then((data) => {
        let result = {
          encoder: TS.read('uint16', data, 0, true),
          angle: TS.read('float32', data, 2, true),
          posEstimate: TS.read('float32', data, 6, true),
          posDot: TS.read('float32', data, 10, true),
          effort: TS.read('float32', data, 14, true)
        }
        resolve(result)
      }).catch((err) => { reject(err) })
    })
  }

  // ------------------------------------------------------ 6: SET PID TERMS

  let pidEP = osap.endpoint()
  pidEP.addRoute(route, TS.endpoint(0, 6), 512)
  this.setPIDTerms = (vals) => {
    return new Promise((resolve, reject) => {
      let datagram = new Uint8Array(16)
      TS.write('float32', vals[0], datagram, 0, true)
      TS.write('float32', vals[1], datagram, 4, true)
      TS.write('float32', vals[2], datagram, 8, true)
      TS.write('float32', vals[3], datagram, 12, true)
      pidEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }

  // ------------------------------------------------------ 7: SET ALPHA TERMS

  let alphaEP = osap.endpoint()
  alphaEP.addRoute(route, TS.endpoint(0, 7), 512)
  this.setAlphas = (vals) => {
    return new Promise((resolve, reject) => {
      let datagram = new Uint8Array(8)
      TS.write('float32', vals[0], datagram, 0, true)
      TS.write('float32', vals[1], datagram, 4, true)
      alphaEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }

  // ------------------------------------------------------ 8: SET UPR

  let uprEP = osap.endpoint()
  uprEP.addRoute(route, TS.endpoint(0, 8), 512)
  this.setUPR = (upr) => {
    return new Promise((resolve, reject) => {
      let datagram = new Uint8Array(4)
      TS.write('float32', upr, datagram, 0, true)
      uprEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => { resolve(err) })
    })
  }

  /*
  // ------------------------------------------------------ POS
  // ok: we make an 'endpoint' that will transmit moves,
  let moveEP = osap.endpoint()
  // add the machine head's route to it, 
  moveEP.addRoute(TS.route().portf(0).portf(1).end(), TS.endpoint(0, 1), 512)
  // and set a long timeout,
  // moveEP.setTimeoutLength(60000)
  this.addMoveToQueue = (move) => {
    // write the gram, 
    let wptr = 0
    let datagram = new Uint8Array(20)
    // write rate 
    wptr += TS.write('float32', move.rate, datagram, wptr, true)
    // write posns 
    wptr += TS.write('float32', move.position.X, datagram, wptr, true)
    wptr += TS.write('float32', move.position.Y, datagram, wptr, true)
    wptr += TS.write('float32', move.position.Z, datagram, wptr, true)
    if (move.position.E) {
      //console.log(move.position.E)
      wptr += TS.write('float32', move.position.E, datagram, wptr, true)
    } else {
      wptr += TS.write('float32', 0, datagram, wptr, true)
    }
    // do the networking, 
    return new Promise((resolve, reject) => {
      moveEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => {
        reject(err)
      })
    })
  }

  // to set the current position, 
  let setPosEP = osap.endpoint()
  setPosEP.addRoute(TS.route().portf(0).portf(1).end(), TS.endpoint(0, 2), 512)
  setPosEP.setTimeoutLength(10000)
  this.setPos = (pos) => {
    let wptr = 0
    let datagram = new Uint8Array(16)
    wptr += TS.write('float32', pos.X, datagram, wptr, true)
    wptr += TS.write('float32', pos.Y, datagram, wptr, true)
    wptr += TS.write('float32', pos.Z, datagram, wptr, true)
    if (pos.E) {
      wptr += TS.write('float32', pos.E, datagram, wptr, true)
    } else {
      wptr += TS.write('float32', 0, datagram, wptr, true)
    }
    // ship it 
    return new Promise((resolve, reject) => {
      setPosEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }

  // an a 'query' to check current position 
  let posQuery = osap.query(TS.route().portf(0).portf(1).end(), TS.endpoint(0, 2), 512)
  this.getPos = () => {
    return new Promise((resolve, reject) => {
      posQuery.pull().then((data) => {
        let pos = {
          X: TS.read('float32', data, 0, true),
          Y: TS.read('float32', data, 4, true),
          Z: TS.read('float32', data, 8, true),
          E: TS.read('float32', data, 12, true)
        }
        resolve(pos)
      }).catch((err) => { reject(err) })
    })
  }

  // another query to see if it's currently moving, 
  // update that endpoint so we can 'write halt' / 'write go' with a set 
  let motionQuery = osap.query(TS.route().portf(0).portf(1).end(), TS.endpoint(0, 3), 512)
  this.awaitMotionEnd = () => {
    return new Promise((resolve, reject) => {
      let check = () => {
        motionQuery.pull().then((data) => {
          if (data[0] > 0) {
            setTimeout(check, 50)
          } else {
            resolve()
          }
        }).catch((err) => {
          reject(err)
        })
      }
      setTimeout(check, 50)
    })
  }

  // endpoint to set per-axis accelerations,
  let accelEP = osap.endpoint()
  accelEP.addRoute(TS.route().portf(0).portf(1).end(), TS.endpoint(0, 5), 512)
  this.setAccels = (accels) => { // float array, len 4 XYZE 
    let wptr = 0
    let datagram = new Uint8Array(16)
    wptr += TS.write('float32', accels.X, datagram, wptr, true)
    wptr += TS.write('float32', accels.Y, datagram, wptr, true)
    wptr += TS.write('float32', accels.Z, datagram, wptr, true)
    if (accels.E) {
      wptr += TS.write('float32', accels.E, datagram, wptr, true)
    } else {
      wptr += TS.write('float32', 0, datagram, wptr, true)
    }
    return new Promise((resolve, reject) => {
      accelEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }

  let rateEP = osap.endpoint()
  rateEP.addRoute(TS.route().portf(0).portf(1).end(), TS.endpoint(0, 6), 512)
  this.setRates = (rates) => {
    // in firmware we think of mm/sec, 
    // in gcode and up here we think in mm/minute 
    // so conversion happens here 
    let wptr = 0
    let datagram = new Uint8Array(16)
    wptr += TS.write('float32', rates.X / 60, datagram, wptr, true)
    wptr += TS.write('float32', rates.Y / 60, datagram, wptr, true)
    wptr += TS.write('float32', rates.Z / 60, datagram, wptr, true)
    if (rates.E) {
      wptr += TS.write('float32', rates.E / 60, datagram, wptr, true)
    } else {
      wptr += TS.write('float32', 100, datagram, wptr, true)
    }
    return new Promise((resolve, reject) => {
      rateEP.write(datagram).then(() => {
        resolve()
      }).catch((err) => { reject(err) })
    })
  }
  */
}