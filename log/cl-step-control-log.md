## Closed Loop Step Control

With 2x A4950s on the DAC, AS5047P on the encoder, etc.

## Do Want 

- magnet alignment print / tool for the glue-in 
- https://www.youtube.com/watch?v=5x73LjZQ21o 

## Likely Bugs

- haven't tested with '+ve' signed calibrations, i.e. if motor += magnetic step does encoder += tick step. 

## Likely Improvements

- encoder value filtering is a PITA, see note from 2021-02-17 about filtering and time constant. change loop structure so that we (1) sample the encoder as fast as possible, always, maybe 50kHz, and then (2) filter that with a heavier first exponential filter (keeping some small time constant as a target), and (3) run the actual control loop with this filtered value 
- use a rejection term instead of this filter... if delX > 200 encoder ticks, just use last... 
- bring phase advance back 
- even better, run an [alpha beta filter](https://en.wikipedia.org/wiki/Alpha_beta_filter) underneath the control loop, as fast as possible, always. 

## Paper 

Have started a writing doc for this, in cba/papers/2021-03_stepper-paper

## Evaluation

- static holding, watch encoder wobble when torque applied (can see ticks moving around even w/o motor torque being overcome), measure again with closed loop: can we hold *better* than static pointing? 

## 2020 12 21

You've mucked about in OSAP a lot since last here. The last commit message from an OSAP version that probably works with this embedded code is ` vport bus-head forwards` - good luck, maybe re-write. Apologies. 

## 2020 10 06 

OK, today am just waking this thing up. Have magnet glued (curing) on the back of the motor now, and one encoder soldered up on an old module-based stepper board - code should be the same.

OK, I have the SPI set up to read from the encoder as in I can see things on the scope are working but I still need to get the complete value back in. My write op for the 16 bit word is ~ 4us, and I need two of those for a complete read (one to issue the read cmd, the next to get the data back), so I am looking at an 8us read, ~ 10, about 100kHz, which is what I was running the step ticker at. I can try pushing the clock speed - running around 124ns clock period is just north of the AS5047's limit, but it seems to behave here and the edges look pretty clean: that nets a 6us read operation for 160-ish KHz alone.

```cpp
void ENC_AS5047::init(void){
    // do pin setup 
    // chip select (not on PC, op manuel)
    ENC_CS_PORT.DIRSET.reg = ENC_CS_BM;
    ENC_CS_DESELECT;
    // clk 
    ENC_CLK_PORT.DIRSET.reg = ENC_CLK_BM;
    ENC_CLK_PORT.PINCFG[ENC_CLK_PIN].bit.PMUXEN = 1;
    if(ENC_CLK_PIN % 2){
        ENC_CLK_PORT.PMUX[ENC_CLK_PIN >> 1].reg |= PORT_PMUX_PMUXO(ENC_SER_PERIPHERAL);
    } else {
        ENC_CLK_PORT.PMUX[ENC_CLK_PIN >> 1].reg |= PORT_PMUX_PMUXE(ENC_SER_PERIPHERAL);
    }
    // mosi
    ENC_MOSI_PORT.DIRSET.reg = ENC_MOSI_BM;
    ENC_MOSI_PORT.PINCFG[ENC_MOSI_PIN].bit.PMUXEN = 1;
    if(ENC_MOSI_PIN % 2){
        ENC_MOSI_PORT.PMUX[ENC_MOSI_PIN >> 1].reg |= PORT_PMUX_PMUXO(ENC_SER_PERIPHERAL);
    } else {
        ENC_MOSI_PORT.PMUX[ENC_MOSI_PIN >> 1].reg |= PORT_PMUX_PMUXE(ENC_SER_PERIPHERAL);
    }
    // miso 
    ENC_MISO_PORT.DIRCLR.reg = ENC_MISO_BM;
    ENC_MISO_PORT.PINCFG[ENC_MISO_PIN].bit.PMUXEN = 1;
    if(ENC_MISO_PIN % 2){
        ENC_MISO_PORT.PMUX[ENC_MISO_PIN >> 1].reg |= PORT_PMUX_PMUXO(ENC_SER_PERIPHERAL);
    } else {
        ENC_MISO_PORT.PMUX[ENC_MISO_PIN >> 1].reg |= PORT_PMUX_PMUXE(ENC_SER_PERIPHERAL);
    }

    // do SPI clock setup
    MCLK->APBDMASK.bit.SERCOM4_ = 1;
    GCLK->GENCTRL[ENC_SER_GCLKNUM].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
    while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(ENC_SER_GCLKNUM));
    GCLK->PCHCTRL[ENC_SER_GCLK_ID_CORE].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(ENC_SER_GCLKNUM);
    
    // reset / disable SPI 
    while(ENC_SER_SPI.SYNCBUSY.bit.ENABLE);
    ENC_SER_SPI.CTRLA.bit.ENABLE = 0; // disable 
    while(ENC_SER_SPI.SYNCBUSY.bit.SWRST);
    ENC_SER_SPI.CTRLA.bit.SWRST = 1; // reset 
    while(ENC_SER_SPI.SYNCBUSY.bit.SWRST || ENC_SER_SPI.SYNCBUSY.bit.ENABLE);
    
    // configure the SPI 
    // AS5047 datasheet says CPOL = 1, CPHA = 0, msb first, and parity checks 
    // bit: func 
    // 15: parity, 14: 0/read, 1/write, 13:0 address to read or write 
    ENC_SER_SPI.CTRLA.reg = SERCOM_SPI_CTRLA_CPOL | // CPOL = 1
                            SERCOM_SPI_CTRLA_DIPO(3) | // pad 3 is data input 
                            SERCOM_SPI_CTRLA_DOPO(0) | // pad 0 is data output, 1 is clk  
                            SERCOM_SPI_CTRLA_MODE(3);  // mode 3: head operation 
    ENC_SER_SPI.CTRLB.reg = SERCOM_SPI_CTRLB_RXEN; // enable rx, char size is 8, etc 
    ENC_SER_SPI.BAUD.reg = SERCOM_SPI_BAUD_BAUD(2); // f_baud = f_ref / ((2*BAUD) + 1) 
                                                    // BAUD = 2 ~= 8MHz / 124ns clock period 
                                                    // BAUD = 3 ~= 6MHz / 164ns clock period: AS5047 min period is 100ns
    
    // enable interrupts 
    NVIC_EnableIRQ(SERCOM4_0_IRQn);
    NVIC_EnableIRQ(SERCOM4_1_IRQn);
    NVIC_EnableIRQ(SERCOM4_2_IRQn);
    NVIC_EnableIRQ(SERCOM4_3_IRQn);

    // turn it back on 
    while(ENC_SER_SPI.SYNCBUSY.bit.ENABLE);
    ENC_SER_SPI.CTRLA.bit.ENABLE = 1;
}

uint16_t ENC_AS5047::spi_interaction(uint16_t outWord){
    if(ENC_SER_SPI.INTFLAG.bit.DRE == 1){
        ENC_CS_SELECT;
        // write first half (back 8 bits) then enable tx interrupt to write second 
        // when written & cleared, write next half 
        outWord01 = (outWord >> 8);
        outWord02 = outWord & 255;
        firstWord = true;
        ENC_SER_SPI.DATA.reg = outWord01;
        ENC_SER_SPI.INTENSET.bit.TXC = 1;
    } else {
        return 0;
    }
}

void ENC_AS5047::txcISR(void){
    // always clear this flag 
    ENC_SER_SPI.INTFLAG.bit.TXC = 1;
    if(firstWord){
        ENC_SER_SPI.DATA.reg = outWord02;
        firstWord = false;
    } else {
        ENC_CS_DESELECT;
        ENC_SER_SPI.INTENCLR.bit.TXC = 1;
    }
}

// 1 handles TXC 
void SERCOM4_1_Handler(void){
    enc_as5047->txcISR();
}

uint16_t ENC_AS5047::read(void){
    spi_interaction(AS5047_SPI_READ_POS);
}
```

I could do this with a read-start then collect-results-later block. 100kHz *seems* like a lot of speed, so I can finish this as a blocking structure and then adapt it later if it seems necessary: but if I run this at 100kHz control loop and do that, I'm occupying ~ 60% of the processor clock just running the encoder read. The intelligent thing would be to trigger the read before the loop, and use it's 'tail' to do the maths. I'll be running this on a timer interrupt anyways, so might as well have that timer start the read, then come back to finish the control loop when the new value is available. 

OK, I think I'm just getting half of the bits... 

Trying this with receive complete interrupts, seems like I'm only getting two events. Weird, have to reset the rxc interrupt to be on at the beginning of every transaction.

Now it just smells like I'm not getting the MSB in the data... probably polarity? OK, per D51 this is actually CPOL = 0, CPHA = 1, flipped bit relative AS5047 datasheet. Here's the full implementation:

```cpp
#include "enc_as5047.h"
#include "../utils/clocks_d51_module.h"

ENC_AS5047* ENC_AS5047::instance = 0;

ENC_AS5047* ENC_AS5047::getInstance(void){
    if(instance == 0){
        instance = new ENC_AS5047();
    }
    return instance;
}

ENC_AS5047* enc_as5047 = ENC_AS5047::getInstance();

ENC_AS5047::ENC_AS5047(){};

void ENC_AS5047::init(void){
    // do pin setup 
    // chip select (not on PC, op manuel)
    ENC_CS_PORT.DIRSET.reg = ENC_CS_BM;
    ENC_CS_DESELECT;
    // clk 
    ENC_CLK_PORT.DIRSET.reg = ENC_CLK_BM;
    ENC_CLK_PORT.PINCFG[ENC_CLK_PIN].bit.PMUXEN = 1;
    if(ENC_CLK_PIN % 2){
        ENC_CLK_PORT.PMUX[ENC_CLK_PIN >> 1].reg |= PORT_PMUX_PMUXO(ENC_SER_PERIPHERAL);
    } else {
        ENC_CLK_PORT.PMUX[ENC_CLK_PIN >> 1].reg |= PORT_PMUX_PMUXE(ENC_SER_PERIPHERAL);
    }
    // mosi
    ENC_MOSI_PORT.DIRSET.reg = ENC_MOSI_BM;
    ENC_MOSI_PORT.PINCFG[ENC_MOSI_PIN].bit.PMUXEN = 1;
    if(ENC_MOSI_PIN % 2){
        ENC_MOSI_PORT.PMUX[ENC_MOSI_PIN >> 1].reg |= PORT_PMUX_PMUXO(ENC_SER_PERIPHERAL);
    } else {
        ENC_MOSI_PORT.PMUX[ENC_MOSI_PIN >> 1].reg |= PORT_PMUX_PMUXE(ENC_SER_PERIPHERAL);
    }
    // miso 
    ENC_MISO_PORT.DIRCLR.reg = ENC_MISO_BM;
    ENC_MISO_PORT.PINCFG[ENC_MISO_PIN].bit.PMUXEN = 1;
    if(ENC_MISO_PIN % 2){
        ENC_MISO_PORT.PMUX[ENC_MISO_PIN >> 1].reg |= PORT_PMUX_PMUXO(ENC_SER_PERIPHERAL);
    } else {
        ENC_MISO_PORT.PMUX[ENC_MISO_PIN >> 1].reg |= PORT_PMUX_PMUXE(ENC_SER_PERIPHERAL);
    }

    // do SPI clock setup
    MCLK->APBDMASK.bit.SERCOM4_ = 1;
    GCLK->GENCTRL[ENC_SER_GCLKNUM].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
    while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(ENC_SER_GCLKNUM));
    GCLK->PCHCTRL[ENC_SER_GCLK_ID_CORE].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(ENC_SER_GCLKNUM);
    
    // reset / disable SPI 
    while(ENC_SER_SPI.SYNCBUSY.bit.ENABLE);
    ENC_SER_SPI.CTRLA.bit.ENABLE = 0; // disable 
    while(ENC_SER_SPI.SYNCBUSY.bit.SWRST);
    ENC_SER_SPI.CTRLA.bit.SWRST = 1; // reset 
    while(ENC_SER_SPI.SYNCBUSY.bit.SWRST || ENC_SER_SPI.SYNCBUSY.bit.ENABLE);
    
    // configure the SPI 
    // AS5047 datasheet says CPOL = 1, CPHA = 0, msb first, and parity checks 
    // bit: func 
    // 15: parity, 14: 0/read, 1/write, 13:0 address to read or write 
    ENC_SER_SPI.CTRLA.reg = //SERCOM_SPI_CTRLA_CPOL | // CPOL = 1
                            SERCOM_SPI_CTRLA_CPHA | // ?
                            SERCOM_SPI_CTRLA_DIPO(3) | // pad 3 is data input 
                            SERCOM_SPI_CTRLA_DOPO(0) | // pad 0 is data output, 1 is clk  
                            SERCOM_SPI_CTRLA_MODE(3);  // mode 3: head operation 
    ENC_SER_SPI.CTRLB.reg = SERCOM_SPI_CTRLB_RXEN; // enable rx, char size is 8, etc 
    ENC_SER_SPI.BAUD.reg = SERCOM_SPI_BAUD_BAUD(2); // f_baud = f_ref / ((2*BAUD) + 1) 
                                                    // BAUD = 2 ~= 8MHz / 124ns clock period 
                                                    // BAUD = 3 ~= 6MHz / 164ns clock period: AS5047 min period is 100ns
    
    // enable interrupts 
    NVIC_EnableIRQ(SERCOM4_0_IRQn);
    NVIC_EnableIRQ(SERCOM4_1_IRQn);
    NVIC_EnableIRQ(SERCOM4_2_IRQn);
    NVIC_EnableIRQ(SERCOM4_3_IRQn);

    // turn it back on 
    while(ENC_SER_SPI.SYNCBUSY.bit.ENABLE);
    ENC_SER_SPI.CTRLA.bit.ENABLE = 1;
    // just... always listen 
    ENC_SER_SPI.INTENSET.bit.RXC = 1;
}

void ENC_AS5047::start_spi_interaction(uint16_t outWord){
    // for some reason, have to reset this to fire? 
    ENC_SER_SPI.INTENSET.bit.RXC = 1;
    if(ENC_SER_SPI.INTFLAG.bit.DRE == 1){
        ENC_CS_SELECT;
        // write first half (back 8 bits) then enable tx interrupt to write second 
        // when written & cleared, write next half 
        outWord01 = (outWord >> 8);
        outWord02 = outWord & 255;
        firstWord = true;
        ENC_SER_SPI.DATA.reg = outWord01;
        ENC_SER_SPI.INTENSET.bit.TXC = 1;
    }
}

void ENC_AS5047::txcISR(void){
    // always clear this flag 
    ENC_SER_SPI.INTFLAG.bit.TXC = 1;
    if(firstWord){
        ENC_SER_SPI.DATA.reg = outWord02;
        firstWord = false;
    } else {
        ENC_CS_DESELECT;
        ENC_SER_SPI.INTENCLR.bit.TXC = 1;
        if(firstAction){
            firstAction = false;
            start_spi_interaction(AS5047_SPI_READ_POS);
        }
    }
}

void ENC_AS5047::rxcISR(void){
    // always clear the bit, 
    uint8_t data = ENC_SER_SPI.DATA.reg;
    readComplete = true;
    if(!firstAction){
        if(firstWord){
            inWord01 = data;
        } else {
            inWord02 = data;
            result = (inWord01 << 8) | inWord02;
            on_read_complete(result);
            readComplete = true;
        }
    }
}

void SERCOM4_2_Handler(void){
    DEBUG1PIN_TOGGLE;
    enc_as5047->rxcISR();
}

// 1 handles TXC 
void SERCOM4_1_Handler(void){
    enc_as5047->txcISR();
}

void ENC_AS5047::trigger_read(void){
    firstAction = true;
    readComplete = false;
    start_spi_interaction(AS5047_SPI_READ_POS);
}

boolean ENC_AS5047::is_read_complete(void){
    return readComplete;
}

uint16_t ENC_AS5047::get_reading(void){
    return result;
}
```

```cpp
void loop() {
  osap->loop();
  stepper_hw->dacRefresh();

  tick++;
  if(tick > 1000){
    tick = 0;
    enc_as5047->trigger_read();
    while(!enc_as5047->is_read_complete());
    uint16_t reading = 0b0011111111111111 & enc_as5047->get_reading();
    sysError(String(reading));
  }
}
```

So, next (tomorrow?) I want to outfit this to build calib. tables... starting at phase 0 / step x, building a table through the sweep. Then I should just be able to apply torque by writing 90 deg phase offsets based on that table, i.e. just pointing through table offsets, no maths. Nice. Then a speed test is to see how fast I can op the stepper just by doing that. 

10pm now, do I want to do this tonight? 

Looking forward, I want to write the table into EEPROM (equivalent) on the motor itself, so writing a routine to do it via VM doesn't make a lot of sense... I should get my spreadsheet out and decide what a reasonable size table is / scheme for this thing. 

## 2020 10 07 

OK, so I need to consider how this is actually going to work.

My control loop will have some set command (torque), and I'll start by reading the encoder. I want a table that relates my encoder reading to (1) the 'actual angle' and (2) the magnetic angle of the rotor w/r/t the stator. Since the whole premise is that we use the stepper's high internal accuracy w/r/t its magnetic angles (its... steps), these should be the same LUT... and since there is a strong relationship between angular position & magnetic position, I am really just going to do some maths later to get the phase advance right, and for the time being can just generate the table from encoder posns to 'real angular' posns. 

[Mechaduino](https://github.com/jcchurch13/Mechaduino-Firmware/blob/master/Mechaduino/Mechaduino/Utils.cpp) does this by generating a LUT where each index is the encoder reading, containing a floating point angular value. Does this by single-stepping through one full revolution 0-360 and recording encoder values (averaged) at each location, then reversing through 0-2^16 to linterp one angular value for each possible encoder uint16_t result, then it can just do:

`float pos = LUT[reading]`

And I need to store these in flash, or / reply to the browser w/ them... will do ahn 'runcalib' and 'readcalib' codes from the browser. 

This table is ~ 65kbytes by the way! 

OK, also I need to make my dac-writing / step-hardware code machine a bit better. First, I think that my LUT crosses 4 'steps' in its full width table, so while I thought I've been microstepping at 256 I've just been doing 64, or something similar. I need this to be a vector machine ish thing, where I can write a phase angle into it, or ->step(micro_counts), which would similarely advance the phase angle from its last position. I would eventually assume I would want something like ->point(phase_angle). 

OK, damn, before I get to that I need to wake up flash and it's a bit more complicated than this library initially makes it seem. 

I also can't seem to even store this big ol' LUT in RAM in the step_cl class, idk what is up. Going to eat dinner. 

`const float __attribute__((__aligned__(256))) lookup[16384] = {`

OK, IDK, odd things here - I can globally define the above LUT and the program does not crash, but when I make it a class member it breaks. Also doesn't seem to make any change to the program allocation size in either of those cases, which is spooky. Ghost memory. 

This seems like a whole thing I'll want to dig into later. I think for now I'm just going to see if I can generate & output the table, I can dig through flash memory later. 

## 2020 10 08 

I'm just going to get into the 'magnetic angle' pointing system for the DACs now, and just wanted to confirm for a minute that these phases *are* 90 deg out of phase with eachother. 

![phase](2020-10-08_90degs.png)

This makes sense: so when I'm at '0 degs' my A phase is on 100%, B phase is zero. Or, the way I've my LUT written, I'll have A at 0 and B at full-width positive. If I don't like this for some reason (It'll calibrate away) I can change the LUT.

FWIW **at 2021 02 03** I ended up drawing this out later:

![phase](2021-02-03_phase-steps-angle.jpg)

In the stepper, we have one 'complete electrical phase' inside of four of what we normally call 'steps' - this spans 7.2 physical degrees when we have a 200 'step' (pole) motor. 

I'm up to pointing, now I just need to deliver some power to the motor. 

OK, this is making sense and I can point my magnetic vector around:

```cpp
// do _aStep and _bStep integers, op electronics 
void STEP_A4950::writePhases(void){
    // a phase, 
    if(LUT_8190[_aStep] > 4095){
        A_UP;
    } else if (LUT_8190[_aStep] < 4095){
        A_DOWN;
    } else {
        A_OFF;
    }
    // a DAC 
    dacs->writeDac0(dacLUT[_aStep] * _cscale);

    // b phase, 
    if(LUT_8190[_bStep] > 4095){
        B_UP;
    } else if (LUT_8190[_bStep] < 4095){
        B_DOWN;
    } else {
        B_OFF;
    }
    // b DAC
    dacs->writeDac1(dacLUT[_bStep] * _cscale);
}

// magnetic angle 0-1 maps 0-2PI phase  
// magnitude 0-1 of <range> board's max amp output 
// one complete magnetic period is four 'steps' 
void STEP_A4950::point(float magangle, float magnitude){
    // guard out of range angles & magnitudes 
    clamp(&magangle, 0.0F, 1.0F);
    clamp(&magnitude, 0.0F, 1.0F);
    uint16_t magint = (uint16_t)(magangle * (float)LUT_LENGTH);
    _aStep = magint; // a phase just right on this thing, 
    // lut_length / 4 = lut_length >> 2 (rotates phase 90 degs)
    // x modulo y = (x & (y − 1))       (wraps phase around end of lut)
    _bStep = (magint + ((uint16_t)LUT_LENGTH >> 2)) & (LUT_LENGTH - 1);
    // upd8 the cscale 
    _cscale = magnitude;
    // now we should be able to... 
    writePhases(); 
}
```

One magnetic period is four full 'steps', so my LUT is 1024 positions long and I am effectively 'microstepping' 256 times (or have this amount of resolution). 

Now I need to write the table, so I'll take 200 samples at each full step, maybe averaging encoder readings 10x. 

OK, I've those samples, so I guess the next move is just to write my table. I think I want to check that all of the readings are continuous (pointing in the same direction) but this also seems a bit tricky: one will be reversed as well, for sure. 

Then I'll get a table that has one jump around the encoder origin. I think I want to start by rewriting the readings table to base zero around the origin, well, obviously since this is a LUT. Finding the actual 'zero' is a bit of a trick though, and requires the interpolation routine to set the first 'mag angle' at that point. 

Maybe this is less complicated... for each value 0-16k, I find the interval it's between in 0-199 indices from the scan. One of these is a bit tricky, otherwise it's whatever. I do the linterp and write down a float. 

Here's the calib so far,

```cpp
boolean Step_CL::calibrate(void){
    // (1) first, build a table for 200 full steps w/ encoder averaged values at each step 
    float phase_angle = 0.0F;
    for(uint8_t i = 0; i < 200; i ++){
        // pt to new angle 
        stepper_hw->point(phase_angle, CALIB_CSCALE);
        // wait to settle / go slowly 
        delay(CALIB_STEP_DELAY);
        // do readings 
        float reading = 0.0F;
        for(uint8_t s = 0; s < CALIB_SAMPLE_PER_TICK; s ++){
            enc_as5047->trigger_read();
            while(!enc_as5047->is_read_complete()); // do this synchronously 
            reading += (float)(enc_as5047->get_reading());
            // this is odd, I know, but it allows a new measurement to settle
            // so we get a real average 
            delay(1); 
        }
        // push reading 
        calib_readings[i] = reading / (float)CALIB_SAMPLE_PER_TICK;
        // rotate 
        phase_angle += 0.25F;
        if(phase_angle >= 1.0F) phase_angle = 0.0F;
    }
    // report readings
    if(false){
        for(uint8_t r = 0; r < 200; r ++){
            sysError(String(calib_readings[r]));
            delay(10);
        }
    }
    // check sign of readings 
    // the sign will help identify the wrapping interval
    // might get unlucky and find the wrap, so take majority vote of three 
    boolean s1 = (calib_readings[1] - calib_readings[0]) > 0 ? true : false;
    boolean s2 = (calib_readings[2] - calib_readings[1]) > 0 ? true : false;
    boolean s3 = (calib_readings[3] - calib_readings[2]) > 0 ? true : false;
    boolean sign = false;
    if((s1 && s2) || (s2 && s3) || (s1 && s3)){
        sign = true;
    } else {
        sign = false;
    }
    sysError("calib sign: " + String(sign));

    // now to build the actual table... 
    // want to start with the 0 indice, 
    for(uint16_t e = 0; e < ENCODER_COUNTS; e ++){
        // find the interval that spans this sample 
        int16_t interval = -1;
        for(uint8_t i = 0; i < 199; i ++){
            if(sign){ // +ve slope readings, left < right 
                if(calib_readings[i] < e && e <= calib_readings[i + 1]){
                    interval = i;
                    break;
                }
            } else { // -ve slope readings, left > right 
                if(calib_readings[i] > e && e >= calib_readings[i + 1]){
                    interval = i;
                    break;
                }
            }
        }
        // log intervals 
        if(interval >= 0){
            // sysError(String(e) + " inter: " + String(interval) 
            //                 + " " + String(calib_readings[interval]) 
            //                 + " " + String(calib_readings[interval + 1]));
        } else {
            sysError("bad interval at: " + String(e));
            return false;
        }
        // find anchors 
        float ra0 = 360.0F * ((float)interval / 200);          // real angle at left of interval 
        float ra1 = 360.0F * ((float)(interval + 1) / 200);    // real angle at right of interval 
        // check we are not abt to div / 0: this could happen if motor did not turn during measurement 
        float intSpan = calib_readings[interval - 1] - calib_readings[interval];
        if(intSpan < 0.1F && intSpan > -0.1F){
            sysError("short interval, exiting");
            return false;
        }
        // find pos. inside of interval 
        float offset =  ((float)e - calib_readings[interval]) / intSpan;
        // find real angle offset at e 
        float ra = ra0 + (ra1 - ra0) * offset;
        // log those 
        sysError("ra: " + String(ra, 4));
        delay(1);
    } // end sweep thru 2^14 pts 
    return true; // went OK 
}
```

This works inside of 'normal' intervals, but fails around the origin due to that wrap issue. I should detect this case... 

OK, new bug: when I average readings that are around the origin, I have samples like i.e. 

```
16383
1
16384
2
16382
1
```

etc, so the average of these needs to wrap as well, ya duh, tough to do with the wrap tho yeah. I can just add the total counts to each measurement and then sub that value from the total average... or from the sum. 

I have this problem: https://en.wikipedia.org/wiki/Mean_of_circular_quantities 

The maths-ey way to do this is to convert to cartesian coordinates, given the theta, average in that space and then return to r coords. 

I guess doing this like that isn't too aweful, tho it's expensive. It would be awesome to have a faster method later on, but there probably better filters (like a kalman) make more sense than just a big ol' average. 

I can imagine taking the measurement spreads: if any were larger than 20 ticks, I could sweep through that set and add the enc_count to the lo vals, then average. Or I could just do this for any reading < 31 ticks, as these are the small ones in *this particular window* but then I have that crawling window issue for measurements really ~ around 31 ticks. Might just be prettier to take a real circular average. 

Here's the circular average:

```cpp
float x = 0.0F;
float y = 0.0F;
for(uint8_t s = 0; s < CALIB_SAMPLE_PER_TICK; s ++){
    enc_as5047->trigger_read();
    while(!enc_as5047->is_read_complete()); // do this synchronously 
    float reading = enc_as5047->get_reading();
    x += cos((reading / (float)(ENCODER_COUNTS)) * 2 * PI);
    y += sin((reading / (float)(ENCODER_COUNTS)) * 2 * PI);
    // this is odd, I know, but it allows a new measurement to settle
    // so we get a real average 
    delay(1); 
}
// push reading, average removes the wraps added to readings. 
calib_readings[i] = atan2(y, x);//(reading / (float)CALIB_SAMPLE_PER_TICK) - ENCODER_COUNTS;
if(calib_readings[i] < 0) calib_readings[i] = 2 * PI + calib_readings[i]; // wrap the circle 
calib_readings[i] = (calib_readings[i] * ENCODER_COUNTS) / (2 * PI);
```

I'm getting some strange intervals back, seems like the 'bad interval' is showing up in three chunks, though it should be one span of angles. I think the best way to debug this would be to plot the whole thing out... or I *could* try straight printing it all back on the serport... maybe I'll try that before building a whole packet-transmission-of-hella-floats thing. 

Yeah this looks all kinds of wrong. 

Borked, indeed. Will check tomorrow. Errors just before the BI (nan, so our of index somewhere?) and the BI looks close but not quite there. 

## 2020 10 09

Ok, updating the interpolation, seems messy but I think (?) maybe this shot will work - at least for -ve signed samples. 

I think.. also, if this doesn't work, I might tackle this differently - by knocking the tail down or up, then wrapping angles. 

Ugh, still doesn't work. Here was the attempt:

```cpp
        // (4) for the bad interval, some more work to do to modify interp. points 
        if(bi){
            // find the zero crossing within the wrapping interval 
            float zc, l0, l1;
            if(sign){
                float l0 = ENCODER_COUNTS - er0;
                float l1 = er1;
            } else {
                float l0 = er0;
                float l1 = ENCODER_COUNTS - er1;
            }
            zc = l0 / (l0 + l1);
            float raMid = (ra0 - (ra1 - ra0)) * zc;
            // modify the interval tails, depending on side of zero crossing 
            if( (sign && er0 < e) || (!sign && er0 > e) ){ 
                // this is left of zc, modify right tail 
                ra1 = raMid; // find the about-zero pt, 
                if(sign){
                    er1 = ENCODER_COUNTS;
                } else {
                    er1 = 0;
                }
            } else { 
                // this is right of zc, modify left tail 
                ra0 = raMid;
                if(sign){
                    er0 = 0;
                } else {
                    er0 = ENCODER_COUNTS;
                }
            }
        }
```

I'm going to try to get this right for the normal intervals first... 

Getting much closer, have implemented a simpler wrap system, looks like the first half works but the second is confused. 

I am still getting these *three* instances of the BI, tho I had always expected to just get two - one at the head of the thing (0 -> first good interval) and the second at the end (last gi -> 2^14). The middle instance is where this seems broken... I'll inspect around there. 

Yeah, I've these readings in the middle of things that are not appearing to live in an interval. 

Right, of course, this is the one that lives in the wrap on readings. I could stick another reading on there, or wrap the indices... should check if the mechanical wrap ~= the code wrap...    

OK - just tacked the last interval on there, so I have 201 readings (201st being == 1st), and 200 intervals. Here's the full calibration routine, w/o saving the giant array:

```cpp
boolean Step_CL::calibrate(void){
    // (1) first, build a table for 200 full steps w/ encoder averaged values at each step 
    float phase_angle = 0.0F;
    for(uint8_t i = 0; i < 200; i ++){ 
        // pt to new angle 
        stepper_hw->point(phase_angle, CALIB_CSCALE);
        // wait to settle / go slowly 
        delay(CALIB_STEP_DELAY);
        // do readings 
        float x = 0.0F;
        float y = 0.0F;
        for(uint8_t s = 0; s < CALIB_SAMPLE_PER_TICK; s ++){
            enc_as5047->trigger_read();
            while(!enc_as5047->is_read_complete()); // do this synchronously 
            float reading = enc_as5047->get_reading();
            x += cos((reading / (float)(ENCODER_COUNTS)) * 2 * PI);
            y += sin((reading / (float)(ENCODER_COUNTS)) * 2 * PI);
            // this is odd, I know, but it allows a new measurement to settle
            // so we get a real average 
            delay(CALIB_SETTLE_DELAY); 
        }
        // push reading, average removes the wraps added to readings. 
        calib_readings[i] = atan2(y, x);//(reading / (float)CALIB_SAMPLE_PER_TICK) - ENCODER_COUNTS;
        if(calib_readings[i] < 0) calib_readings[i] = 2 * PI + calib_readings[i]; // wrap the circle 
        calib_readings[i] = (calib_readings[i] * ENCODER_COUNTS) / (2 * PI);
        // rotate 
        phase_angle += 0.25F;
        if(phase_angle >= 1.0F) phase_angle = 0.0F;
    } // end measurement taking 
    // tack end-wrap together, to easily find the wrap-at-indice interval 
    calib_readings[200] = calib_readings[0];
    if(false){ // debug print intervals 
        for(uint8_t i = 0; i < 200; i ++){
            sysError("int: " + String(i) 
                        + " " + String(calib_readings[i], 4)
                        + " " + String(calib_readings[i + 1], 4));
            delay(2);
        }
    }
    // check sign of readings 
    // the sign will help identify the wrapping interval
    // might get unlucky and find the wrap, so take majority vote of three 
    boolean s1 = (calib_readings[1] - calib_readings[0]) > 0 ? true : false;
    boolean s2 = (calib_readings[2] - calib_readings[1]) > 0 ? true : false;
    boolean s3 = (calib_readings[3] - calib_readings[2]) > 0 ? true : false;
    boolean sign = false;
    if((s1 && s2) || (s2 && s3) || (s1 && s3)){
        sign = true;
    } else {
        sign = false;
    }
    sysError("calib sign: " + String(sign));

    // (2) build the table, walk all encoder counts... 
    // now to build the actual table... 
    // want to start with the 0 indice, 
    for(uint16_t e = 0; e < ENCODER_COUNTS; e ++){
        // find the interval that spans this sample
        boolean bi = false; 
        int16_t interval = -1;
        for(uint8_t i = 0; i < 200; i ++){
            if(sign){ // +ve slope readings, left < right 
                if(calib_readings[i] < e && e <= calib_readings[i + 1]){
                    interval = i;
                    break;
                }
            } else { // -ve slope readings, left > right 
                if(calib_readings[i] > e && e >= calib_readings[i + 1]){
                    interval = i;
                    break;
                }
            }
        }
        // log intervals 
        if(interval >= 0){
            // sysError(String(e) + " inter: " + String(interval) 
            //                 + " " + String(calib_readings[interval]) 
            //                 + " " + String(calib_readings[interval + 1]));
        } else {
            // no proper interval found, must be the bi 
            // find the opposite-sign interval 
            for(uint8_t i = 0; i < 200; i ++){
                boolean intSign = (calib_readings[i + 1] - calib_readings[i]) > 0 ? true : false;
                if(intSign != sign){
                    interval = i;
                    bi = true; // mark the bad interval
                    break;
                }
            }
            if(!bi){
                // truly strange 
                sysError("missing interval, exiting");
                return false;
            }
            /*
            sysError("bad interval at: " + String(e) 
                    + " " + String(interval)
                    + " " + String(calib_readings[interval]) 
                    + " " + String(calib_readings[interval + 1]));
            */
        }

        // (3) have the interval (one is bad), 
        // find real angles (ra0, ra1)
        float ra0 = 360.0F * ((float)interval / 200);          // real angle at left of interval 
        float ra1 = 360.0F * ((float)(interval + 1) / 200);    // real angle at right of interval 
        // interval spans these readings (er0, er1)
        float er0 = calib_readings[interval];
        float er1 = calib_readings[interval + 1];

        // (4) for the bad interval, some more work to do to modify interp. points 
        float spot = e;
        if(bi){
            if(sign){ // wrap the tail *up*, do same for pts past zero crossing 
                er1 += (float)ENCODER_COUNTS;
                if(spot < er0) spot += (float)ENCODER_COUNTS;
            } else { // wrap the tail *down*, do same for pts past zero crossing 
                er1 -= (float)ENCODER_COUNTS;
                if(spot > er0) spot -= (float)ENCODER_COUNTS;
            }
        }

        // (5) continue w/ (ra0, ra1) and (er0, er1) to interpolate for spot 
        // check we are not abt to div / 0: this could happen if motor did not turn during measurement 
        float intSpan = er1 - er0;
        if(intSpan < 0.01F && intSpan > -0.01F){
            sysError("near zero interval, exiting");
            return false;
        }
        // find pos. inside of interval 
        float offset = (spot - er0) / intSpan;
        // find real angle offset at e, modulo for the bad interval 
        float ra = (ra0 + (ra1 - ra0) * offset);
        // wrap to 360 degs, 
        /*
        if(ra < 0.0F){
            ra += 360.0F;
        } else if (ra > 360.0F){
            ra -= 360.0F;
        }
        */
        // log those 
        if(bi){
            sysError("e: " + String(e) + " ra: " + String(ra, 4) + " BI");
            //     + " span: " + String(intSpan) + " offset: " + String(offset));
            // sysError("i0: " + String(interval) + " " + String(calib_readings[interval])
            //     + " i1: " + String(calib_readings[interval + 1])
            //     + " BI");
        } else {
            sysError("e: " + String(e) + " ra: " + String(ra, 4));
        }
        delay(10);
    } // end sweep thru 2^14 pts 
    sysError("calib complete");
    return true; // went OK 
}
```

So next up is writing that to flash so that I can use it. 

Alright! Flash memory. 

Seems like I've most of it written - just copying from mechaduino, but after I write these out, I freeze up the micro. 

Yeah, welp, I think I might have to get into the datasheet and do this au manuel. I've learned that when I declare a const variable, it goes into the flash memory. I think my 'alignment' of that will set it up in pages, so I 'align' it to the page size, which I can read to be 512 bytes... I'll take a minute, see how this goes. Bummer it's Friday, I was pumped to get this up and running this week. 

## 2020 10 13 

Setting up the NVM Controller today to try to write pages into flash. Not sure why this was crashing before, probably something simple. 

I don't think there's a clock / init I have to do to wake up the flash controller, seems like that would be kind of crazy. Yeah, 25.6.1.1 clarifies this. 

I'm also not sure where the bootloader is in here, and seems likely that I might write it over. 

'addressable on the AHB bus' 

I think I almost get this, need to figure this note about disabling cache.. some erratta... am at 25.6.6

Trying with just doing automatic quad word writing, which I think auto increments the page-write pointer, not sure about what happens for the page buffer though. I should check via the mechaduino code and flash storage code... 

Ok this is hella opaque, 25.6.6.2 ... `procedure for manual page writes` 
    - the block to be written must be erased before the write command is given 
    - write to the page buffer by addressing the nvm main address space directly (crazy) 
    - cmd = wp to write the full contents of the page buffer into the nvm at the page pointed to by addr 

I fully don't understand this, but it seems like it should be straightforward. Likely next step is to burn it all down and try again from scratch. I should see if I can write *anything* and move away from this calib-then-write situation, which is probably complicating things. 

#### Aboot Flash

Have to erase before write, to set back to 1's: flash memory writes by pulling zeroes low, and has to operate in blocks. 

I can also check the UF2 bootloader, which uses flash:

https://github.com/microsoft/uf2-samdx1/blob/master/src/flash_samd51.c

This code seems to have a much better handle on things in any case. I should try something straightforward to start. 

Well, I think I might be writing into memory, but I've no way of reading it out - or I crash when I try to dereference the address. Will try making that const... Same. 

I suspect I am writing into some bad spaces. This code (which *is* better) is expecting a pointer to a uint32, not the actual 'raw' uint32 addr... and I was previously trying to write to 0xF0000000 which is well out of range, I meant to do 0x10000000... 

To clarify, I'll write the base fns with real void* rs... or in uint32_t addresses, as uint32_t s... 

Have isolated down to this `((uint32_t*)dst)[i] = ((uint32_t*)src)[i];` call... I suspect I'm pointing at something poorly. And I still get the sense I'm writing into the bootloader block. 

OK, I can read out of the start_addr, but after I've written to it, cannot do this anymore - processor hangs. Altho:

`// bossac writes at 0x20005000`

so that should be OK, I'm writing at 0x1... tho this program uses 42kb, but indeed once I've done the flash write, the bootloader fails. Tried writing at the lut address, fails also. 

I don't know what is real anymore, the libraries failed me, the datasheet is opaque AF, I am writing somewhere I shouldn't, etc. Might be time to bring out the atmel studio guns. 

Or the small guns: I'm going to try to get a minimum viable something up with this library. 

Works like this:

```cpp
FlashStorage(flash_store, float);

void flash_write_init(void){

}

void flash_write_value(float val){
    flash_store.write(val);
}

void Step_CL::print_table(void){
    for(uint16_t e = 0; e < 4; e ++){
        float ra = flash_store.read();
        sysError("e: " + String(e) + " ra: " + String(ra, 4));
        delay(5);
    }
}
```

So I can try doing batches of these... Seems like a full 'page' works, I wonder if anything > 512 bytes does as well, or crashes... breaks at 256 floats, but not at 129... 

So it seems that it should work, if I can get into the lower levels of this thing and increment pointers around. I'll try to do that first with just the 128 bytes that I know can work, and I'm pretty sure that's inside of one page. 

OK, trips at the erase. Takes *forever* to reload code once I cripple it like this. 

Oy, seems like I wrote it... then it dissappears after a reset, wth? That was to do with a volatile flag. 

I think this has to do with erase block / instead of erase page. I should learn more about that in the data sheet. 

Blocks are 16 pages: erase granularity is per block, write granularity is per page. One guess is that if the block / page are not aligned to a block, the EB command fails. Altho from 25.6.6.6 (fitting), it notes that any address within the block is valid for an erase block command. 

OK, trying to unlock the block beforehand. 

SO! Definitely hung on trying to erase a block. However, have had some evidence that this worked previously. 

It's 1230 - I'll think about this tomorrow. Maybe try taking the above single-float working example apart, see if you can code it up yourself, to understand what's up. 

## 2020 10 14 

OK, today will bring this library into the local build and see about figuring what the heck it does, build the minimum example from scratch, and then see if I can scale that up to multiple blocks / tables. 

- pages are 512 bytes 
- blocks are 16 pages 

This means a block is 8192, makes sense this is where flashstorage aligns itself. I need 8 blocks total for the full LUT. 

Interesting, now that I fixed this error which appeared when it was compiling locally, I can get past 512 bytes with my example. Could try the big one... 

OK, but doesn't work past 8192 bytes - I'll try just over the boundary to check again. At the full spec, it indeed doesn't even start up. Can get to 4096 floats, but 8192 breaks things. 

Last check for easy answers, I'll see if I can just tag 4x4096 FlashStorage instances together. OK, looks like I can get away with this. Damn, that was promising but it locks up on block #3. It doesn't matter which one I try to write first: i.e. it's not that prior writes fill up space for the latter, even if I try to write to the 3rd brick at the start, it hangs. OK very strange: can write all bricks except for the 3rd... 

Eh, IDK anymore, I can't write more than twice regardless of which brick I'm writing to. Big shrug, going to try with bricks of 2048 size. Need 8 of those for the same total-bounds errors... OK, having the same problem here. These should be blocks. I can try once more, with 1024 floats each, just to be sure. 

Nope, errors around the same spot. So I should check in the code to see if I can figure out where this is hanging up... At this point I suspect I am having some low level error from an NVMCTRL command, or, somehow the ptrs are pointing into bad spaces. 

Trying:
- no write (fails block 3)
- no erase (fails block 3)
- no invalidate cache (same)
- *no* write fn (same)
- *no* erase fn 

Suspect ROW_SIZE something is up, 
- no while(>size) in erase 
- no ptr op in erase fn 

OK this is crazy, I have every fn in the class commented out, and the thing still locks up. 

So, tried my way around that, I have honestly no idea what is going on here. I'm going to try writing from the base class again. 

![bugfix](2020-10-14_bugfix.mp4)

OK, did that and seems like I'm writing into the thing. Crazy. Here's the code:

```cpp
#define BYTES_PER_BLOCK 8192
#define FLOATS_PER_BLOCK 2048

const float __attribute__((__aligned__(8192))) lut[16834] = {};
FlashClass flashClass((const uint8_t*)lut);

// write mechanics
const void* block_ptr;

// write buffer 
static float buffer[FLOATS_PER_BLOCK];

uint32_t bfi = 0; // buffer indice 
uint32_t bli = 0; // block indice 

//FlashStorage(flash_storage, flts);

void flash_write_init(void){
    block_ptr = (const uint8_t*) lut;
    bfi = 0;
    bli = 0;
}

void flash_write_page(void){
    sysError("erasing");
    flashClass.erase(block_ptr, BYTES_PER_BLOCK);
    sysError("writing");
    flashClass.write(block_ptr, (const uint8_t*)buffer, BYTES_PER_BLOCK);
    delay(100);
}

void flash_write_value(float val){
    buffer[bfi ++] = val;
    if(bfi >= FLOATS_PER_BLOCK){
        flash_write_page();
        bfi = 0;
        bli ++;
        block_ptr = ((const uint8_t *)(&(lut[bli * FLOATS_PER_BLOCK])));
    }
}

void Step_CL::print_table(void){
    sysError("reading from lut");
    for(uint32_t i = 0; i < ENCODER_COUNTS; i ++){
        float ra = lut[i];
        sysError(String(ra));
        delay(5);
    }
}

// the calib routine 
boolean Step_CL::calibrate(void){
    flash_write_init();
    for(uint32_t i = 0; i < ENCODER_COUNTS; i ++){
        flash_write_value(i * 1.1F);
    }
    return true;
}
```

So I think now I'll carry on actually running the calibration, then see if I can send a 'torque' command down / operate a control loop. 

Great, this calibrates, so now I can roll a tiny control loop, yeah? 

### Rolling a Loop

First up, I'm hanging this thing up when I try to run it in an interrupt: might need to make some volatile vars. 

Ah - no, this is probably because I am running the control loop in an interrupt, but the var is never returning because that happens in *another* interrupt. 

OK, have 50khz of this going on, now I need to apply some twerk vector and see if I can poke it around. 

To track performance: 
- read takes 7.7us
- w/ all the whistles, down to the set, it's about 10us even 
- 7.9us w/o the write to the dacs, so that's 2us alone 

OK, now I just have to prevent this from running when things are non-calib'd. Or maybe there should be a dummy table in there, to start? Have to turn the timer IRQ off during calib, let's see... 

OK, yep, but now the loop ranges from 40us to 10us because of this line:

```cpp
    float ra = lut[result];
    // to find the phase, want to find modulus this ra / 1.8 
    while(ra > 1.8F){
        ra -= 1.8F;
    } 
```

Not cool. Also seems pretty terrence regardless: easy to stall. 

Ah, I was mapping 1.8 - 1.0, should have been 7.2 - 1.0: the 0-1 'phase angle' sweeps thru 4 full steps, not one. 

OK well this is a decent minimum viable thing: it commutates. There's (I think) an obvious relationship between control loop and maximum RPM, so it's probably all maths and optimization from here out. The encoder read is the the obvious bottleneck, so is that modulus operation. I'm not totally sure I'm applying phase angle ideally... ah yeah, obviously better results when I do 180* phase rotation rather than 90, why did I think it was 90? Thing's rippen at 5krpm w/ no load, that should do pretty well, stoked to put it on an axis tomorrow and issue some commands from the browser to whip things around. Exciting futures for motion control. 

![motor](2020-10-14_cl-step-control.mp4)

## 2020 10 16

Next real steps for this are formulating it into a paper / research question, then getting into the work. 

OK - the work. Am going to do a torque command from the browser first, full width. 

That works, now I'm noticing some oddball behaviour - the control loop seems to lag depending on where the encoder reading is: I thought that was because the maths was taking longer (father to modulo down) but I might just have some pins mixed up? Ah - this was a mistaken end-of-transmission from the SPI level... fixed, I was correct orginally, just had the pin flipping confused. 

So yeah, this is the long modulo. I guess it won't hurt to try writing a second LUT for phase angle, see if I can bring that execution time down... ready to brick in 3..2..1.. bricked haha, IDK how to align such a big table in RAM I guess. 

So to store that in flash, I could perhaps build the LUT to be like [ra0, pa0, ra1, pa1, ..., ra1024, pa1024] and access with 2x or 2x + 1 indices. Let's see if I can write a flash table this long without any trouble. 

OK, so my loop is now more like:

```cpp
    _ra = lut[result * 2];          // the real angle (position 0-360)
    _pa = lut[result * 2 + 1];      // the phase angle (0 - 1 in a sweep of 4 steps)
    // this is the phase angle we want to apply, 90 degs off & wrap't to 1 
    if(step_cl->get_torque() < 0){
        _pa -= 0.25; // 90* phase swop 
        if(_pa < 0){
            _pa += 1.0F;
        }
    } else {
        _pa += 0.25;
        if(_pa > 1){
            _pa -= 1.0F;
        }
    }
    // now we ask our voltage modulation machine to put this on the coils 
    // with the *amount* commanded by our _tc torque ask 
    step_a4950->point(_pa, abs(step_cl->get_torque()));
``` 

and runs 2.4us. The whole loop is now nicely just around 10us, so I could choke the whole micro and run 100kHz and probably trip up, or I'll try running ~ 75kHz, or I could be satisfied with 50 and leave overhead for the processor to do ... well ... other stuff, haha. I.E I will probably want this headroom for positioning PID, etc. Maybe I should just get it mounted on an axis and proceed with the next steps instead of fiddling with speed before I know it's necessary or warranted. 

### What Now 

So, this is great the LL hardware / control strategy / calibration works. Now starts the real business.

There are two things I want to do, first is use these in machines: that means reliably positioning, rolling a PID (maybe) or some other kind of control loop around the thing, to recieve a 'goto pos' command and execute it. In this case, I just want an entry point into the controller to be that 'pos' field, and have a loop running to control around that. 

The second thing is to build this 'real torque / virtual effort' calibration: I suspect I want a 2D Table here so that f(effort, speed) = torque. To do this, I would want to build an interface that let me command various efforts, and read back resulting speeds... to build accelerations, calculate forces, and write that table doing some LERPing. 

The second thing is actually, probably, easier than the first: since it doesn't require me to do any control. Later, when I *am* doing control, it will be interesting to try to re-align the two studies, to see if, indeed, the accelerations that happen are the ones I expected. 

Since that seems easy, I'll start there. Going to get another motor solder'd up, throw it on that axis, and then tool around in JS and CPP writing down motor-efforts & reading-back speed values. Then I should be able to plot some things, and see about writing a routine that sweeps thru a handful of speeds & efforts to try to build that 2D LERP table... 

Shiiiit this is tite. I wonder if I should move the logs to the project repo...

![speeed](2020-10-16_cl-step-speed-01.mp4)
![speeed](2020-10-16_cl-step-speed-02.mp4)
![speeed](2020-10-16_cl-step-speed-03.mp4)

So I am a bit distracted with just tooling around with this thing, but it feels pretty slick and I'm stoked. Videos above. I need to get into the actual control. The first note, though, is that there is certainly some missing control parameter that is like 'phase advance * velocity' - when velocity ticks up, the commanded current should lead a little longer than 90 degs, but probably no more than 180 degs. 

So the first move is to build in to the lower level a velocity measurement. For units, since I have real angles, I should do degs/sec as a unit, and this means I'll have to deal with wrapping angles... hopefully I can find a fast way to do that. 

## 2020 10 17 

What's a reasonable 'end spiral' for this?

I do want to get to phase advance today & the beginnings of the JS 'looping' interface: that will determine if I need to build better underlaying architecture before continuing... py serial, or rpi node->uart. 

### Phase Advance 

So let's see... I want to know about angular velocity's relation to RPM / relation to angular phases / second. 

```
1 rpm = 6 deg/s
7.2 deg/s = 1 magnetic phase / sec
```

So there is a nice kind of parity here, 

```
1 rpm = 0.83 magnetic phase / sec 
``` 

Supposing that the stator 'points' immediately at the end of each loop, I'll have one speed limit related to the angular travel in between each loop: if I set my phase 90deg ahead at the end of one loop, but the rotor is moving more than 90deg (magnetic: 1.8 physical) per loop period, I'm in trouble. This limit actually seems quite high: it's about 15krpm. 

| rpm | deg/s | mag phases / sec | us / mag phase | us / quarter mag phase |
| --- | --- | --- | --- | --- | 
| 15000 | 90000 | 12500 | 80 | 20 |
| 10000 | 60000 | 8333 | 120 | 30 |

This doesn't mean that's *absolute max* speed, it's just a crossing point where I would be applying some negative torque during part of the rotation if I went any faster.

Since optimal torque is applied at 90* out of phase with the current rotor position, to do this ideally I would set the magnetic vector of the stator ahead of 90* by 1/2 of the phase angle covered by the rotor during the loop time, so that, during the rotation taking place during this period of the loop, the effective angle would average to 90*.  

Of course, I'm not pointing my magnetic vector *immediately* when I write the DACs - indeed, even the chopper drive in the A4950s has a 25us fixed off time during current decay, about half of my loop cycle time. 

![chopper](2020-10-17_a4950-decay-time.png) 

Besides this, there's the actual rise time of the coils: the real magnetic lag. This means that in reality, I'll want to set my phase advance well ahead of the *ideal* 90 deg, to compensate for slower parts of the system. 

Were I a bit more intrepid, I could do things like measure phase inductance, etc, and could probably figure pretty well exactly how far ahead it were reasonable to set the phase advance. However! In reality, this is probably a tuned variable: and one that is probably not hard to search for. 

### Roughing It 

That said! I want to set it and forget about it for now: I can come back here if I ever want to chase some more gains. I showed up here just trying to ballpark a first guess. Basically, I think I should set my phase advance at 90deg when rpm = 0, and then scale it to a maximum of 180deg when things are going quite fast: based on my control-loop-speed guessing above (where the abs max before crossover was 15krpm) and based on my intuitive knowledge that I am unlikely to spin this stepper faster than 10krpm, I'll set the top there. 

```
where 0.5 == 180 degs of phase advance 
phase advance = 0.25 + min(0.25, deg/s * 0.000004)
```

This is a kind of crazy small floating point, and I besides need to calculate my speed - which, really, I should do some filtering there because I know there's a few ticks of error in the encoder. 

### Speed Maths 

Discrete deeeeeerivatiiiives 

I think this is probably not hard, however, doing it fast is maybe more challenging. I also need to set up some infrastructure I think to do it well: this js looping situation. Maybe that first. 

OK, have this basically up and - yes - there's lots of noise in the measurement, and the wrap is a struggle. I am tempted to plot a little chart so that I can visualize this more-better as opposed to just watching values flash across the page. 

Yeah, starting that up, now have this very noisy signal:

![noise](2020-10-17_noisy-speed.png)

I think first order is eliminating those spikes that (?) I would guess are during wraps, and then doing better by smoothing over readings. Although I am getting some readings north of 16 million degs / sec: those would be the wraps (360 * 50k), most of the noise is around 60k degs / sec, where it appears as if things have rotated ~ 1 full degree each tick. Those seem odd: one encoder tick is only 0.022 degrees, so that would indicate over 50 ticks of noise, which seems like a tonne more than I was observing. 

Tried the cos/sin/atan2 wrap method: way too slow. 

I suspect it will eventually be very useful to bring the whole calibration table back and plot it, and I wonder if I should just get to filtering things to see if I can eliminate these 1 degree noisy events. 

A simple / dummy filter *sort of works*
```cpp
    // want to calculate speeds, need to wrap the pair 
    _ra_l0 = 0;
    _ra_l1 = 0;
    // to filter... 
    for(uint8_t i = 0; i < 6; i ++){
        _ra_l0 += _ra_last[i];
        _ra_l1 += _ra_last[i + 6];
    }
    _diff = (_ra_l1 - _ra_l0) / 6;
    _deg_s = _diff * TICKS_PER_SEC;
    // upd8 
    for(uint8_t i = 0; i < 11; i ++){
        _ra_last[i] = _ra_last[i + 1];
    }
    _ra_last[11] = _ra;
```

But not really. I think the real answer is a kalman filter, so I should give this a go. 

http://robotsforroboticists.com/kalman-filtering/

https://github.com/hmartiro/kalman-cpp 

This would be a real piece of maths homework for me. A more likely approach would be to fit a linear segment to a series of measurements, and use the endpoint of that segment as the 'predicted state' ... 

I also have this wrapping-around problem no matter how I kick it. For a last attempt at a hacked filter, I might try writing down deltas and wrapping deltas, then filtering on those (instead of absolute angles). 

... state estimate is x and xdot 
... state estimator is x = x + xdot 
... new estimate for xdot is the 
    ... (old estimate for xdot * alpha) + (measurement * (1 - alpha))
... alpha is an expecation of how smooth 
... new xdot uses old estimate of x, and new measurement 

OK, sam gave me some beta on how to think about this in simpler terms. A 1D Kalman filter is e-z, seems like, it's kind of a smoothing filter. 

Oh jeez, I don't think I've done this right: or the wrapping situation is really messing things up. 

I've implemented [this simple 1d kalman](https://github.com/bachagas/Kalman/blob/master/Kalman.h) though I don't think I understand it... it does do stuff. But does it do more than a simple smoothing filter? 

In any case, I gave up tuning and just piped position back, here's what noise there looks like:

![noise](2020-10-17_noisy-pos.png)

Alltogether too much, I think I should read in the whole calibration table and plot it, to see what that looks like... I suspect there are some stray jumps there: maybe at interval boundaries or something like that. 

So next up, I'll write a little routine that sequentially queries for LUT entries, in blocks, and plots those. Once I squash that problem (ominous foreshadowing) I think I'll have a much better time filtering. X first, then Xdot, yeah. 

So - going to put this down for a minute. 

**when we return**
- find the jumps in position readings: pull the LUT up and plot it: check RA and PA. 
- the current track is just trying to get a decent, non-noisy position and velocity estimate(s) from the thing. then you can use the vel estimate to write phase advance term per your notes above, and also to measure accel (xddot) = f, for your 2D torque-estimate lut. 
- *then* you want to do both
    - measuring torque guess success: use calibrated loadcell or somesuch?
    - (or) simply go forwards, towards using this in a machine: build a PID position controller, and tune it, maybe using a JS setPID() command, and remotely evaluate some set of moves, to tune. an auto-cal / auto-tune routine, etc. likely via the bus & with smoothie dishing moves, maybe just over usb, who knows 

## 2020 11 16

OK, looks like it's been a month. Not sure where the days are dissappearing to lately. I'm going to start by pulling the LUT back home. 

First, easy test, can just send the encoder reading back to see what the direct noise is like. 

Glad I did this, there's obviously some erroneous (eliminate-able) noise on top of a reasonable floor:

![noise](2020-11-16_noisy-encoder.png)

These are just raw encoder values. My honest guess is that I'm missing some bits in the signal back. I might still have some polarity wrong on the SPI, or it's going too fast and missing edges (or something like this). 

I turned the clock polarity to 1 in the D51, looks like I eliminate most of these spikes;

![cpol](2020-11-16_cpol-zero-typical.png)

That's just about 2 bits of noise, totally understandable from a 14 bit encoder. However, I still see the occasional spike:

![cpol](2020-11-16_cpol-zero-stray.png)

I can try tuning the speed down, it's near a limit at the moment. 

Well, yeah, the only thing that reliably de-noises these readings is to take the speed from around 8MHz with a nice small 5us read time to around 1.6MHz, where it takes about 25us. 

I think I should tack on an encoder-magnetic-strength reading to the calibration phase. That's set up, but the value looks to be high enough... though it fluctuates between 9k and 16k (16384 beign tops: it's a 14 bit value). 

There's also an error-flags register I can read, that'll directly tell me if things are too high or too low. 

Cool, I see some flags there - magnetic field is too high. So I can re-outfit my diagnostic to return a string with this whole message, then boot that back to the browser. And I can try bigger standoffs (nice), to compensate. 

Well, that's all instrumented. I have some strangeness here though: maghi / maglo seem to sweep between the full range: so does the cordic magnitude. This is a bit confusing because the magnet stays separated by the same amount, so I am wondering if because the magnet is a bit smaller - 6x2mm rather than the 8x3 spec'd in the datasheet - that the field is doing strange things due to misalignment. I should pick up the 8x3 magnets to check, that's my last best guess.

In any case, slowing the read time down has significantly improved the noise, and I think I can get back to a speed estimate. 

### Alpha Filter

I want to try this, but always want to take the underlying encoder reading back as well, so I can see if errors are arising somewhere in the filtering code or in the real reading. 

... ??? wtf 

OK, my new theory is that something is *up* with the volatile vals in here. 

Confused, IDK what is up. Another day. 

## 2021 02 14

Long haitus on this work but I am back because I want to bring it home for good position control. 

I have changed a lot of the OSAP internals since working on this, so I'll bring it up to date with OSAP submodules to start. 

I'm adding an on-query call in embedded osap... 

I think my diagnostics register reading might be off... things seem to vary way too much. Again, I think maybe the CPOL is off or something? 

I should check against position readings. Sorted those, seems like it *should* be set up as CPHA = 1, CPOL = 0, but I still have some mysterious diagnostics readings. Whatever, I am going to proceed as planned. 

OK, I'm pulling back a full spec / object - and yeah, big / compound typing is really really critical for these things - and I just need now to bring calibration back online, then I can get to noise-bughunting. 

So I'm pulling back data now: encoder reading, an angle measurement (from the LUT) and then a filtered value ... I absolutely pinched the filter: alpha is `0.01`, but this actually works really well. Here's the encoder reading up top (where I have this terrible noise... I figure a bit is going missing at some place, idk - it's an odd pattern), the measured angle beneath, and filtered finally below. This is picking out really nice variations between 320.22 deg to 320.170 - and it feels fast enough (even with the big filter) to control with. So I'll try to start with this as a reference signal. 

![heavy-filt](2021-02-15_heavy-alpha-0-01.png)

I should eventually try to delete these spurious signals, but... I can approach that later. 

### Position PID

This is pretty straightforward, but I find noise here too - filtering the position estimate isn't really enough, I think I need to filter the derivative estimate as well after calculating - which shouldn't really be true. Eventually, surely, I need to get rid of these periodic spikes.

![d-term](2021-02-15_angle-dot.png)

This should be smoooother... but I think I can probably get something running besides. 

OK, it's up and ready to get kicked. I need an enable-pid-control flag and ideally some way to push the target around. 

Alright, here's day on PID then... still have some interface to build for PID remote set, also would like alpha settings on a_est and a_dot... but it does do *something* 

![video](2021-02-15_cl-step-pid-start.mp4) 

## 2021 02 16

Alright so I'm back with this, have PID remote-set done, now doing alpha (filtering) terms remote-set, then I'll finish with the position target update, to bump it around randomly for testing.

It occurs to me also that this particular case (motor on desk, nothing else) is extremly un-damped, meaning that the motor is going to be very unstable, irregardless of what I do. I think I might print out a rotary axis for testing, then. 

Yeah, tuning will be hard, have the subsystems now though. I am suspicious of the heavy filtering - especially as I think it might be shifting the d-term signal phase to be 180' or so out of phase, making for oscillations. In any case, tomorrow, a serious tuning / pid-mastery-attempt session. 

## 2021 02 17

So, got to design a little rotary drive last night, that should help establish a realistic baseline for 'the plant' so to speak. 

Currently:

![tn](2021-02-17_tuning-01.png)

```
P   -0.09
I   0
D   -0.0003

A   0.2
A*  0.05
```

This feels... OK. I need an I term, and an I limit... which I figure should be (?) ~ 0.2, to start. I can end up adding oscillation and is a PITA to tune IIRC. In a lot of ways, it's another 'p' term. 

![tn](2021-02-17_tuning-01.png)

I figured at the start that a relatively small iLimit would be the move, but a larger limit feels better to help small disturbances... i.e. it pins the torque to the max when there's a resting force on the rotor (as in machining / etc). 

My current winners are:

```
P       -0.09
I       -0.002
Ilim    0.8
D       -0.0003

A       0.2
A*      0.05

at 10kHz 
```

OK, I am satisfied with these for the time being, time now is to apply it to a real system. I have also a few things I want to know:

### Time Constant of the Exponential Filter?

I was suspecting earlier that my filters were too aggressive (alpha 0.01), causing effectively a signal phase delay that was driving oscillations. This was confirmed (psuedo-scientific / intuitive sense) when turning the filter values 'down' (to 0.2) caused oscillations to stop, and allowed me to tune my P values back up without bringing on more oscillation. 

So, what's the real maths here? Shouldn't be that hard to find on the internet. Yeah, from [the wiki](https://en.wikipedia.org/wiki/Exponential_smoothing) it looks like (if the sampling interval `delT` is fast relative the time constant)

`tc = delT / alpha`

So for an alpha around 0.01, with our sampling rate of 10kHz or 0.0001 the time constant was ~ 0.1 seconds, or 100ms, which is a whole lot in these terms - and the oscillations I observed seemed around 10Hz (maybe faster) so, yes, ok, we cannot just filter this into oblivion. 

However, we could sample the encoder more often (I think 50kHz was the hardware limit) and filter that, then run the PID loop on a pre-filtered value. I'm going to keep a list of 'likely improvements' here, that being one of them. 

### The Alpha Beta Filter?

This also seems primo, the [alpha beta filter](https://en.wikipedia.org/wiki/Alpha_beta_filter) is basically a 2d kalman filter, IIRC from conversations with Sam. This is basically what we do when we know that i.e. our previous speed estimate is likely to give us a good expected value for our new position measurement. 

Seems a bit tricky algorithmically because it is easy to become confused about time-steps / previous values / etc - and besides, we are using the position measurements to take derivatives that we then use to improve our position measurements, seems a bit cyclical. 

In any case, I am not going to go down this rabbit hole now, but it's there for interest. 

### Unrolling the Angles

So, then, next is having target set-points that are not directly related to the motor. I wonder if actually the way to do this is actually to add another layer that runs on top of the servo controller and, given a set-point with some 'RPU' (rotations per unit), calculates the rotations to get there, and keeps feeding a new target to the lower level hardware... nah, this won't work because eventually we will have some points that are at ~ 359.9 degs or so and it needs to intelligently servo from 0.1 degs to that, not around the other side.

So! There must be an established way to do this... 

- wrap around 360, test with 'targ 360' key 
- supervisor for offsets / etc ? 

Wondering if this is going to get tricky.

- derivative should also 'wrap' - otherwise there will be a spike between 0.0 and 360.0.

Competing approaches would be 

- wrap around 360 in the subsystem, supervisor points to angles around 360 
- or, count angle and *revolutions (integer, 32bit)* and track current-pos of these... translate between floating point *target* pos and *revs + angle* with units per step scalar 

I think the second manner is actually best, so I'm giving that a shot. First step I take is to translate from rotary measurements -> linear / unrolled-rotary position, wrapping around & incrementing a 'revolutions' count, then I calculate 'real position' with revs * angular_pos. 

Just taking a minute to figure out the particulars on this one... `a r i t h m e t i c` 

OK, here's my counting-the-revs code

```cpp
    // stash this,
    enc_reading = result;
    // stash old measurement, pull new from LUT 
    a_reading_last = a_reading;
    a_reading = lut[result * 2]; 
    // check for wraps around the circle,
    // i.e. if we went from 359 -> 1 degs, we have gone around once, 
    // if we went from 1 -> 359, we have decrimented one rev 
    if(a_reading - a_reading_last > 180.0F){
        revolutions --;
    } else if (a_reading_last - a_reading > 180.0F){
        revolutions ++;
    }

    // now we have a real position measurement:
    p_est_last = p_est;
    p_est = (float)revolutions * 360.0F + a_reading;
```

Now I can do the derivative, error, and integral all in terms of actual error on the long number line, no wrapping to worry about. Great: do it once, forget about it. 

I think I'll get that back up and running... and target a wider range of positions, should be all tuned in already, and then I'm kind of finished with this step... next would be to bring it into a real machine context, benchtop test a new VM with an accel controller feeding it positions. 

So, for 'units per revolution' (1) I cannot just throw in a -ve value here to reverse it, but I would like to be able to and (2) the tuning variables depend on this, since the loop now runs in the 'space' of the unrolled position, rather than in the 'space' of the wrapped / unwrapped angles. 

I think maybe then the move is to leave the controller in degrees-and-revolutions, kind of makes sense down there, and feed it target 'revs & degrees' positions based on this. Yep, this is the way. 

OK, all seems sound now. So tomorrow would be about setting this up in a 'real system' ... or maybe it's prepping that system: I have parts ready to make another clank, a larger one... though I should check against the reality of my available desk space. I might as well jump in on the X axis there...

- re-order the API / the internal code: lots of spaghetti in here at the moment
- match vm api, 
- build clank-stretch 
- put it on the bus, check if all commands can still op. through the bus: this is higher bandwidth, check for drops etc, maybe you have to fix those now
- configure to grab positions from the bus, you'll be mixing closed- and open-loop motors not irregularely. 

## 2021 02 25 

FWIW I made a demo of this today, and it looks pretty good:

![perf](2021-02-25_cl-step-perf.mp4) 