#include <Arduino.h>

#include "indicators.h"
#include "drivers/step_a4950.h"
#include "homing.h"
#include "osape/core/osap.h"
#include "osape/vertices/endpoint.h"
#include "osape_arduino/vp_arduinoSerial.h"
#include "osape_ucbus/vb_ucBusDrop.h"

// bare defaults: use vm / bus id to set on startup 
uint8_t axisPick = 0;
boolean invert = false;

// root

OSAP osap("stepper-drop");

// vport, vbus, 

VPort_ArduinoSerial vpUSBSer(&osap, "arduinoUSBSerial", &Serial);

VBus vb_ucBusDrop(
  &osap, "ucBusDrop", 
  &vb_ucBusDrop_loop,
  &vb_ucBusDrop_send,
  &vb_ucBusDrop_cts
);

// -------------------------------------------------------- AXIS PICK EP 

EP_ONDATA_RESPONSES onAxisPickData(uint8_t* data, uint16_t len){
  if(data[0] > 3){
    axisPick = 0;
  } else {
    axisPick = data[0];
  }
  return EP_ONDATA_ACCEPT;
}

Endpoint axisPickEp(&osap, "axisPick", onAxisPickData);

// -------------------------------------------------------- AXIS INVERSION EP

EP_ONDATA_RESPONSES onAxisInvertData(uint8_t* data, uint16_t len){
  data[0] ? invert = true : invert = false;
  return EP_ONDATA_ACCEPT;
}

Endpoint axisInvertEp(&osap, "axisInvert", onAxisInvertData);

// -------------------------------------------------------- MICROSTEP EP 

EP_ONDATA_RESPONSES onMicrostepData(uint8_t* data, uint16_t len){
  stepper_hw->setMicrostep(data[0]);
  return EP_ONDATA_ACCEPT;
}

Endpoint microstepEp(&osap, "microstep", onMicrostepData);

// -------------------------------------------------------- CSCALE DATA

EP_ONDATA_RESPONSES onCScaleData(uint8_t* data, uint16_t len){
  chunk_float32 cscalec = { .bytes = { data[0], data[1], data[2], data[3] } };
  if(cscalec.f > 1.0F){
    cscalec.f = 1.0F;
  } else if (cscalec.f < 0.0F){
    cscalec.f = 0.0F;
  }
  stepper_hw->setCurrent(cscalec.f);
  return EP_ONDATA_ACCEPT;
}

Endpoint cScaleEp(&osap, "CScale", onCScaleData);

// -------------------------------------------------------- HOME ROUTINE 

EP_ONDATA_RESPONSES onHomeData(uint8_t* data, uint16_t len){
  chunk_int32 rate = { .bytes = { data[0], data[1], data[2], data[3] } };
  chunk_uint32 offset = { .bytes = { data[4], data[5], data[6], data[7] } };
  // sign of rate is dir, 
  boolean hdir;
  uint32_t hrate;
  if(rate.i < 0) {
    hdir = false;
    hrate = -1 * rate.i;
  } else {
    hdir = true;
    hrate = rate.i;
  }
  writeHomeSettings(hdir, hrate, offset.u);
  startHomingRoutine();
  return EP_ONDATA_ACCEPT;
}

Endpoint homeEp(&osap, "Home", onHomeData);

// -------------------------------------------------------- HOME STATE 

Endpoint homeStateEp(&osap, "HomeState");

// -------------------------------------------------------- SETUP 

void setup() {
  ERRLIGHT_SETUP;
  CLKLIGHT_SETUP;
  //DEBUG1PIN_SETUP;
  homeSetup(&homeStateEp);
  // links... 
  vpUSBSer.begin();
  vb_ucBusDrop_setup(&vb_ucBusDrop, true, 0);
  // stepper init 
  stepper_hw->init(false, 0.0F);
}

// -------------------------------------------------------- LOOP 

void loop() {
  osap.loop();
  stepper_hw->dacRefresh();
  limitHit() ? ERRLIGHT_ON : ERRLIGHT_OFF;
} // end loop 

// -------------------------------------------------------- BUS INTERRUPT / STEP 

uint8_t stepCache[2];
volatile uint8_t cachePtr = 0;
volatile boolean stepValid = false;
volatile uint8_t stepMask = 0;

void ucBusDrop_onPacketARx(uint8_t* inBufferA, volatile uint16_t len){
  // refill step cache:
  stepCache[0] = inBufferA[0]; stepCache[1] = inBufferA[1];
  cachePtr = 0;
  stepValid = true;
}

void ucBusDrop_onRxISR(void){
  // if we're currently homing the motor, bail 
  // home statemachine 
  if(getHomeState() != HOMESTATE_NONE){
    runHomingRoutine();
    return;
  }
    // if we dont' have valid steps, bail 
  if(!stepValid) return;
  // extract our step mask
  stepMask = 0b00000011 & (stepCache[cachePtr] >> (axisPick * 2));
  // mask -> step api:
  if(invert){
    (stepMask & 0b00000001) ? stepper_hw->dir(true) : stepper_hw->dir(false); // dir bit,
  } else {
    (stepMask & 0b00000001) ? stepper_hw->dir(false) : stepper_hw->dir(true); // dir bit,
  }
  if (stepMask & 0b00000010) stepper_hw->step();                           // step bit
  // increment ptr -> next step mask, if over-wrap, steps no longer valid 
  cachePtr ++; if(cachePtr >= 2) stepValid = false;
}

